///////////////////////////////////////////////////////////////////////////////
// This class provides meta information about the program.

#include <sstream>
#include <string>
#include <vector>

#include <iostream>

class Debug
{
	public:

		/////////////////////////////////////////////////////////////////////////
		// It is recommended that the user uses the __FILE__ macro for the
		// file name, and the __PRETTY_FUNCTION__ macro for the function.
		// and the __LINE__ macro for the line number.
		Debug(const std::string& fileName, 
			  const std::string& functionName,
			  const int lineNumber);

		/////////////////////////////////////////////////////////////////////////
		// Adds details to the debug message. 
		void addDetails(const std::string& details);

		/////////////////////////////////////////////////////////////////////////
		// Gets the full debug message. The line number value will be cleared 
		// after this message gets called in order to avoid confusion when reading
		// subsequent message if the line is not set.
		std::string getMessage() const;

		/////////////////////////////////////////////////////////////////////////
		// Writes the debug message to the given file.
		void writeDebugMessage(const std::string& fileName) const;

		/////////////////////////////////////////////////////////////////////////
		// Adds the input object's data to the debug message. Multiple objects
		// can be added to the output message.
		template <class T>
		void addObjectInfoToMessage(const T& object,
									const std::string& objectName,
									const std::string& objectType,
									const std::string& objectValue = "") 
		{
			std::string infoMessage = "Object Name: " + objectName + "\n";
			infoMessage.append("Type Name: " + objectType + "\n");
			infoMessage.append("Address: " + std::to_string(&object) + "\n");

			if (!objectValue.empty())
			{
				infoMessage.append("Value: " + objectValue + "\n");
			}

			mObjectInfo.push_back(infoMessage);
		}

		/////////////////////////////////////////////////////////////////////////
		// Returns true if the two object address's that are input are equal 
		// and false if otherwise.
		template <class T>
		bool checkAddressEquality(const T& objectOne, const T& objectTwo) const
		{
			bool equal = false;

			if (&objectOne == &objectTwo)
			{
				equal = true;
			}

			return equal;
		}

	private:

		// The debug message details.
		std::string mDetails;

		// The current line number of the debug message.
		int mLineNumber;

		// The name of the file being debugged.
		std::string mFileName;

		// The name of the function being debugged.
		std::string mFunctionName;

		// Requested Information about specific objects.
		std::vector<std::string> mObjectInfo;
};
