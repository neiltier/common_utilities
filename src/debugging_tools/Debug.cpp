#include <fstream>

#include "Debug.hpp"

///////////////////////////////////////////////////////////////////////////////
Debug::Debug(const std::string& fileName, 
				 const std::string& functionName,
				 const int lineNumber) : 
												 mFileName(fileName),
												 mFunctionName(functionName),
												 mLineNumber(lineNumber)
{
}

///////////////////////////////////////////////////////////////////////////////
void Debug::addDetails(const std::string& details)
{
	mDetails = details;
}

///////////////////////////////////////////////////////////////////////////////
std::string Debug::getMessage() const
{

	std::string header("\n\n---------------------------------------------\n\n");
	const std::string date(__DATE__);
	const std::string time(__TIME__);

	std::string message = header + date + " " + time + "\n";
	message.append(mFileName + ": " + mFunctionName + "\n");

	if (mLineNumber > 0)
	{
		message.append("Line: " + std::to_string(mLineNumber) + "\n");
	}

	if (!mDetails.empty())
	{
		message.append("Details: " + mDetails + "\n\n");
	}

	if (!mObjectInfo.empty())
	{
		message.append("Object Information:\n");

		std::vector<std::string>::const_iterator itr;
		for(itr = mObjectInfo.begin(); itr != mObjectInfo.end(); ++itr)
		{
			message.append("\n" + *itr);
		}
	}

	return message;
}

///////////////////////////////////////////////////////////////////////////////
void Debug::writeDebugMessage(const std::string& fileName) const
{
	std::ofstream outfile;
	outfile.open(fileName.c_str(), std::ios_base::app);
	
	outfile << getMessage();
}

