///////////////////////////////////////////////////////////////////////////////
// This file contains simple examples on how the Debug class should be used.

#include <iostream>

#include "Debug.hpp"

static const std::string DEBUGFILE("debug.txt");

///////////////////////////////////////////////////////////////////////////////
void functionTwo()
{
	Debug debug(__FILE__, __PRETTY_FUNCTION__, __LINE__);
	debug.addDetails("In function two.");
	debug.writeDebugMessage(DEBUGFILE);
}

///////////////////////////////////////////////////////////////////////////////
void functionOne()
{
	Debug debug(__FILE__, __PRETTY_FUNCTION__, __LINE__);
	debug.addDetails("In function one.");
	debug.writeDebugMessage(DEBUGFILE);

	functionTwo();
}

///////////////////////////////////////////////////////////////////////////////
class Test
{

public:

	static void staticFunction() 
	{

		Debug debug(__FILE__, __PRETTY_FUNCTION__, __LINE__);
		debug.addDetails("In static function .");
		debug.writeDebugMessage(DEBUGFILE);
		
	}
};


int main()
{

	// Basic example.
	Debug debug(__FILE__, __PRETTY_FUNCTION__, __LINE__);
	debug.addDetails("Example one.");
	std::cout << debug.getMessage();
	debug.writeDebugMessage(DEBUGFILE);

	functionOne();

 	Test::staticFunction();

	Test testObjectOne;
	Test* testObjectTwo = &testObjectOne;
	double i = 1.99999;

	const std::string equalityMsg("Address's are equal: ");
	std::cout << equalityMsg 
				 << debug.checkAddressEquality<Test>(testObjectOne, *testObjectTwo)
				 << std::endl;	

	debug.addObjectInfoToMessage(testObjectOne, "testObjectOne", "Test");
	debug.addObjectInfoToMessage(*testObjectTwo, "testObjectTwo", "Test");
	debug.addObjectInfoToMessage(i, "i", "double", debug.toString(i));
	std::cout << debug.getMessage();

	double j = i;
	std::cout << equalityMsg
				 << debug.checkAddressEquality(i, j)
				 << std::endl;	

	debug.writeDebugMessage(DEBUGFILE);
	
	return 0;
}
