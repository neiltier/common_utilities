#ifndef RANDOM_HPP
#define RANDOM_HPP

#include <vector>

class Random
{
public:

	// ---------------------------------------------------------------------------
    // Returns a random integer within the range of the lower and upper limits
    // specified. An assertion failure occurrs if the upper limit is less than 
    // the lower limit.
    static int getRangedRandomInt(const int lower, const int upper);

	// ---------------------------------------------------------------------------
	// Generates a vector of random integer values of the input size and is
    // within the range of the upper and lower limits.
    // Assertion failures occur if the size is less than zero and the upper limit
    // is less than the lower limit.
	static std::vector<int> getRandomIntContainer(
            const int size,
            const int lower,
            const int upper);

private:

    friend class RandomTest;
};


#endif // RANDOM_HPP


