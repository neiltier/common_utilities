
#include "Random.hpp"

#include <algorithm>
#include <cassert>
#include <functional>
#include <random>


// ---------------------------------------------------------------------------
int Random::getRangedRandomInt(const int lower, const int upper)
{
    // Fail if lower is less than upper.
    assert(lower <= upper);

    // Create a random number between the lower and upper limits.
    std::mt19937 rng;
    rng.seed(std::random_device()());
    std::uniform_int_distribution<int> randomInt(lower, upper);

    return randomInt(rng);
}

// ---------------------------------------------------------------------------
// Generates a vector of random integer values of the input size. The size
// must be greater than -1;
std::vector<int> Random::getRandomIntContainer(
        const int size,
        const int lower,
        const int upper)
{
    assert(size > -1);
    assert(lower < upper);

    std::random_device randomDevice;
    std::mt19937 engine(randomDevice());
    std::uniform_int_distribution<int> distribution(lower, upper);

    auto generator = std::bind(distribution, engine);

    std::vector<int> randomValues(size);
    std::generate(std::begin(randomValues), std::end(randomValues), generator);

    return randomValues;
}


