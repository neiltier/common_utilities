#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <algorithm>
#include <map>
#include <exception>
#include <string>
#include <sstream>
#include <stdint.h>
#include <vector>

#include "ErrorManager.hpp"

template <class T>
class Matrix
{
public:

	// ---------------------------------------------------------------------------
	// Defines the location of Matrix row or column. 
	enum Position
	{
		COLUMN,
		ROW,
	};


	// ---------------------------------------------------------------------------
	// Creates an empty 1x1 matrix
	Matrix() : mMatrix(1, std::vector<T>(1))
	{
		initializeErrorCodes();
	}

	// ---------------------------------------------------------------------------
	Matrix(const Matrix<T>& matrix) = default;

	// ---------------------------------------------------------------------------
	Matrix<T>& operator=(Matrix<T> matrix)
	{
		swap(matrix);
		return *this;
	}

	// ---------------------------------------------------------------------------
	// Creates a matrix of the specified height x width where all of the values 
	// are equal to zero.
	Matrix(int height, int width)
	{
		initializeErrorCodes();

		if (height < 1)
		{
			std::vector<std::string> errorParams{ "height", std::to_string(height) };
			mErrors.push_back(mErrorCodes.getErrorMsg(INVALID_SIZE, errorParams));
			height = 1;
		}

		if (width < 1)
		{
			std::vector<std::string> errorParams{ "width", std::to_string(width) };
			mErrors.push_back(mErrorCodes.getErrorMsg(INVALID_SIZE, errorParams));
			width = 1;
		}

		mMatrix.assign(height, std::vector<T>(width));
	}

	// ---------------------------------------------------------------------------
	~Matrix() = default;
	Matrix(Matrix<T>&&) = default;

	// ---------------------------------------------------------------------------
	// Gets the matrix height.
	int getHeight() const
	{
		return mMatrix.size();
	}

	// ---------------------------------------------------------------------------
	// Gets the matrix width.
	int getWidth() const
	{
		int width = mMatrix[0].size();

		// Add 1 to the width since the vector holds 0 elements, 
		// but a matrix would be considered to be 1 element wide.
		if (mMatrix[0].empty())
		{
			width = 1;
		}

		return width;
	}

	// ---------------------------------------------------------------------------
	// Gets the row in the requested postion of the matrix. This function will 
	// NOT throw an exception if an invalid index is input, but it will report
	// an error.
	std::vector<T> getRow(const int rowIndex)
	{
		std::vector<T> row;

		if (validateRowIndex(rowIndex))
		{
			row = mMatrix[rowIndex];
		}
		else
		{
			mErrors.push_back(mErrorCodes.getErrorMsg(INVALID_ROW_ACCESS, std::to_string(rowIndex)));
		}

		return row;
	}

	// ---------------------------------------------------------------------------
	std::vector<T> getColumn(const int columnIndex)
	{
		std::vector<T> column;

		if (validateColumnIndex(columnIndex))
		{
			for (const std::vector<T>& element: mMatrix)
			{
				column.push_back(element[columnIndex]);
			}
		}
		else
		{
			mErrors.push_back(mErrorCodes.getErrorMsg(INVALID_COLUMN_ACCESS, std::to_string(columnIndex)));
		}

		return column;
	}

	// ---------------------------------------------------------------------------
	// Removes the specified column in the matrix by index. If theres is only one
	// column in the matrix then it is not erased since a matrix cannot be zero width.
	// If a column is able to be removed then true is returned and false otherwise.
	// If an invalid index is input then a error is logged and false is returned.
	bool removeColumn(const int columnIndex)
	{
		bool columnRemoved = false;

		if (validateColumnIndex(columnIndex))
		{
			if (getWidth() > 1)
			{
				for (std::vector<T>& row : mMatrix)
				{
					if (row.size() > 1)
					{
						row.erase(row.begin() + columnIndex);
					}
				}

				columnRemoved = true;
			}
		}
		else
		{
			mErrors.push_back(mErrorCodes.getErrorMsg(INVALID_COLUMN_ACCESS, std::to_string(columnIndex)));
		}

		return columnRemoved;
	}

	// ---------------------------------------------------------------------------
	// Removes the specified row in the matrix by index. If there is only one row
	// in the matrix then the row is not deleted since there cannot be zero rows in
	// the matrix.
	// If a row is able to be removed then true is returned and false otherwise.
	// If an invalid index is input then a error is logged and false is returned.
	bool removeRow(const int rowIndex)
	{
		bool rowRemoved = false;

		if (validateRowIndex(rowIndex))
		{
			if (getHeight() > 1)
			{
				mMatrix.erase(mMatrix.begin() + rowIndex);
				rowRemoved = true;
			}
		}
		else
		{
			mErrors.push_back(mErrorCodes.getErrorMsg(INVALID_ROW_ACCESS, std::to_string(rowIndex)));
		}

		return rowRemoved;
	}

	// ---------------------------------------------------------------------------
	// Adds a row to the bottom of the matrix. The width of the newly added row must
	// be the same width as the other row(s) in the matrix. Returns true if 
	// the row was added and false otherwise.
	bool appendRow(const std::vector<T>& newRow)
	{
		const bool validRow = validateNewRow(newRow);

		if (validRow)
		{
			mMatrix.push_back(newRow);
		}

		return validRow;
	}

	// ---------------------------------------------------------------------------
	// Modifies existing row within the matrix if the index already exists.
	// If the new row length is not valid then an INVALID_ROW_ACCESS error is logged.
	// Returns true if the rowIndex and the new row's length are valid and false otherwise.
	bool modifyRow(const int rowIndex, const std::vector<T>& newRow)
	{
		bool modified = false;

		// Validate the input index.
		if (validateRowIndex(rowIndex))
		{
			if (validateNewRow(newRow))
			{
				modified = true;
				mMatrix[rowIndex] = newRow;
			}
		}
		else
		{
			mErrors.push_back(mErrorCodes.getErrorMsg(INVALID_ROW_ACCESS, std::to_string(rowIndex)));
		}

		return modified;
	}

	// ---------------------------------------------------------------------------
	// Modifies existing row within the matrix if the index already exists. If the
	// index is equal to the matrix height (1 past the last valid index) then if 
	// the new row size is valid then it is appended to the matrix. If the new 
	// row length is not valid then an INVALID_ROW_ACCESS error is logged. Returns
	// true if the rowIndex and the new row's length are valid and false otherwise.
	bool addModifyRow(const int rowIndex, const std::vector<T>& newRow)
	{
		bool addedOrModified = false;

		bool validRowIndex = validateRowIndex(rowIndex);
		bool addNewRow = false;

		// If the the row index is invalid but the input index is only 1
		// past the valid index make the validRowIndex flag valid and set 
		// the addNewRow flag.
		if (!validRowIndex)
		{
			if (rowIndex == getHeight())
			{
				addNewRow = true;
				validRowIndex = true;
			}
		}

		// Validate the input index.
		if (validRowIndex)
		{
			if (validateNewRow(newRow))
			{
				addedOrModified = true;

				if (addNewRow)
				{
					mMatrix.push_back(newRow);
				}
				else
				{
					mMatrix[rowIndex] = newRow;
				}
			}
		}
		else
		{
			mErrors.push_back(mErrorCodes.getErrorMsg(INVALID_ROW_ACCESS, std::to_string(rowIndex)));
		}

		return addedOrModified;
	}


	// ---------------------------------------------------------------------------
	// Returns any outstanding errors in the container of error strings.
	std::vector<std::string> getErrors() const
	{
		return mErrors;
	}

	// ---------------------------------------------------------------------------
	// Clears any outstanding errors in the container of error strings.
	void clearErrors()
	{
		mErrors.clear();
	}

	// ---------------------------------------------------------------------------
	// Allows the user to modify the matrix element in a specific location.
	// If either of the input indexes are invalid then an IndexOutOfBoundsException
	// will be thrown. Otherwise, the desired element will be returned.
	T& at(const int rowIndex, const int columnIndex)
	{
		bool validIndexes = validateRowIndex(rowIndex);
		validIndexes &= validateColumnIndex(columnIndex);

		if (!validIndexes)
		{
			throw IndexOutOfBoundsException();
		}

		return mMatrix[rowIndex][columnIndex];
	}

	// ---------------------------------------------------------------------------
	// Const bracket operator to retrieve a row. Note that this operator does not
	// add an error message to mErrors. If the index is not 
	// in bounds the function will thow an IndexOutOfBoundsException.
	const std::vector<T>& operator[](const int index) const
	{
		// This must be above the return statement.
		if (!validateRowIndex(index))
		{
			throw IndexOutOfBoundsException();
		}

		return mMatrix[index];
	}

	// ---------------------------------------------------------------------------
	// Non-const bracket operator to retrieve a row. Note that this operator does not
	// add an error message to mErrors. If the index is not 
	// in bounds the function will thow an IndexOutOfBoundsException.
	const std::vector<T>& operator[](const int index) 
	{
		// This must be above the return statement.
		if (!validateRowIndex(index))
		{
			throw IndexOutOfBoundsException();
		}

		return mMatrix[index];
	}

	// ---------------------------------------------------------------------------
	Matrix<T> operator+(const Matrix<T>& matrix) const
	{
		if (validateEvenMatrices(matrix))
		{
			Matrix<T> results;
			results.mMatrix.clear();

			for(int i = 0; i < getHeight(); ++i)
			{
				results.mMatrix.push_back(std::move(addSubtractRow(true, mMatrix[i], matrix[i])));
			}

			return results;
		}
		else
		{
			throw UnevenMatricesException();
		}
	}

	// ---------------------------------------------------------------------------
	Matrix<T> operator-(const Matrix<T>& matrix) const
	{
		if (validateEvenMatrices(matrix))
		{
			Matrix<T> results;
			results.mMatrix.clear();

			for(int i = 0; i < getHeight(); ++i)
			{
				results.mMatrix.push_back(std::move(addSubtractRow(false, mMatrix[i], matrix[i])));
			}

			return results;
		}
		else
		{
			throw UnevenMatricesException();
		}
	}

	// ---------------------------------------------------------------------------
	bool operator==(const Matrix<T>& matrix) const
	{
		bool equal = (mErrors == matrix.mErrors);
		equal &= (mMatrix == matrix.mMatrix);
		equal &= (mErrorCodes == matrix.mErrorCodes);

		return equal;
	}

	// ---------------------------------------------------------------------------
	bool operator!=(const Matrix<T>& matrix) const
	{
		return !operator==(matrix);
	}

	// ---------------------------------------------------------------------------
	// Multiplies a matrix by a scalar value in the form of Matrix * scalar
	Matrix<T> operator*(const int scalar) 
	{
		Matrix<T> result;
		result.mMatrix.clear();

		for (const std::vector<T>& row: mMatrix)
		{
			std::vector<T> productRow;

			for (const T& element : row)
			{
				productRow.push_back(element * scalar);
			}

			result.mMatrix.push_back(std::move(productRow));
		}

		return result;
	}

	// ---------------------------------------------------------------------------
	Matrix<T> operator*(Matrix<T> matrix)
	{
		// If the lhs width doesn't equal the rhs height then the matrices can't
		// be multiplied so throw and exception.
		if (getWidth() != matrix.getHeight())
		{
			throw NonMultipliableMatricesException();
		}

		Matrix<T> result;
		result.mMatrix.clear();

		for (const std::vector<T> row : mMatrix)
		{
			std::vector<T> resultRow;

			for (int i = 0; i < matrix.getWidth(); ++i)
			{
				resultRow.push_back(getDotProduct(row, matrix.getColumn(i)));
			}

			result.mMatrix.push_back(std::move(resultRow));
		}

		return result;
	}

	// ---------------------------------------------------------------------------
	// TODO: Fix this function. It currently doesn't work and is not tested.
	int getDeterminant()
	{
		int determinant = 0;

		int width = getWidth();
		int height = getHeight();

		if (width == height)
		{
			if ((width == 2) && (height == 2))
			{
				determinant = getTwoByTwoDeterminant(*this);
			}
			else
			{
				const std::tuple<Position, int> rowOrColumnLocation =
					getColumnOrRowWithMostZeros();

				const Position position = std::get<0>(rowOrColumnLocation);
				const int index = std::get<1>(rowOrColumnLocation);
			}
		}

		return determinant;
	}

	// TODO: Move to private
	std::vector<Matrix<T>> getMinors(
		const Matrix<T>& matrix, 
		std::tuple<Position, int> positionData) const
	{
		std::vector<Matrix<T>> minors;

		const int position = std::get<1>(positionData);

		if (std::get<0>(positionData) == ROW)
		{
			for (int i = 0; i < matrix.getWidth(); ++i)
			{
				Matrix<T> duplicate(matrix);

				duplicate.removeRow(position);
				duplicate.removeColumn(i);

				minors.push_back(duplicate);
			}
		}

		return minors;
	}

	
private:

	// Make Test class a friend.
	friend class MatrixTest;

	// ---------------------------------------------------------------------------
	// Adds or subtracts two matrix rows. If the addition flag is false then 
	// the rows are subtracted and if true the rows are added. The result of the 
	// matrix row addition/subtraction is returned. NOTE: This function should be
	// used only if the row lengths are the same length.
	std::vector<T> addSubtractRow(
		const bool addition,
		const std::vector<T>& x,
		const std::vector<T>& y) const
	{
		std::vector<T> z;

		for(int i = 0; i < x.size(); ++i)
		{ 
			T result;

			if (addition)
			{
				result = x[i] + y[i];
			}
			else
			{
				result = x[i] - y[i];
			}

			z.push_back(result);
		}

		return z;
	}

	// ---------------------------------------------------------------------------
	// Swaps the current object with the input object.
	void swap(Matrix<T>& rhs)
	{
		std::swap(mMatrix, rhs.mMatrix);
		std::swap(mErrors, rhs.mErrors);
		std::swap(mErrorCodes, rhs.mErrorCodes);
	}

	// ---------------------------------------------------------------------------
	// Validates a matrix row index. If the index is within bounds then true 
	// is returned and false otherwise.
	bool validateRowIndex(const int index) const
	{
		return (index > -1) && (index < getHeight());
	}

	// ---------------------------------------------------------------------------
	// Validates a matrix column index. If the index is within bounds then true 
	// is returned and false otherwise.
	bool validateColumnIndex(const int index) const
	{
		return (index > -1) && (index < getWidth());
	}

	// ---------------------------------------------------------------------------
	// Returns true if the new row is equal to the width of the matrix and returns
	// false if it is not as well as appends an INVALID_NEW_ROW error.
	bool validateNewRow(const std::vector<T>& newRow)
	{
		const bool validWidth = (newRow.size() == getWidth());

		if (!validWidth)
		{
			const std::vector<std::string> errors { std::to_string(newRow.size()), std::to_string(getWidth()) };
			mErrors.push_back(mErrorCodes.getErrorMsg(INVALID_NEW_ROW, errors));
		}

		return validWidth;
	}

	// ---------------------------------------------------------------------------
	// Validates that two matrices are even in width and height.
	bool validateEvenMatrices(const Matrix<T>& matrix) const
	{
		const bool equalHeight(getHeight() == matrix.getHeight());
		const bool equalWidth(getWidth() == matrix.getWidth());

		return (equalHeight && equalWidth);
	}

	// ---------------------------------------------------------------------------
	// Performs a row * column dot product calculation and returns the product. 
	// The value will return 0 if the row size != column size.
	T getDotProduct(const std::vector<T>& row, const std::vector<T>& column) const
	{
		T product = 0;

		if (row.size() == column.size())
		{
			int i = 0;
			for (const T& rowValue : row)
			{
				product += (rowValue * column[i]);
				i += 1;
			}
		}

		return product;
	}

	// ---------------------------------------------------------------------------
	// Returns the determinant of a two by two matrix. If the matrix is not a two
	// by two then an exception is thrown.
	T getTwoByTwoDeterminant(const Matrix<T>& matrix) const
	{
		const bool twoByTwoMatrix = (matrix.getHeight() == 2) && (matrix.getWidth() == 2);

		if (!twoByTwoMatrix)
		{
			throw UnevenMatricesException();
		}

		return (matrix[0][0] * matrix[1][1]) - (matrix[0][1] * matrix[1][0]);
	}

	// ---------------------------------------------------------------------------
	// This function assists in finding the matrix determinant by finding the
	// row or column value with the most zeros. If no rows or values have zeros
	// then row 0 will be returned by default. If a row and a column have equal
	// amounts of zeros then the row value will be returned.
	std::tuple<Position, int> getColumnOrRowWithMostZeros()
	{
		// Set the initial position to be row zero.
		std::tuple<Position, int> position(ROW, 0);

		int max = 0;

		// TODO: optimize this algorithm.

		// Iterate the rows
		for (int i = 0; i < getHeight(); ++i)
		{
			const int numberOfZeros = std::count(mMatrix[i].begin(), mMatrix[i].end(), 0);

			if (numberOfZeros > max)
			{
				max = numberOfZeros;
				position = std::make_tuple(ROW, i);
			}
		}

		// Iterate the columns.
		for (int i = 0; i < getWidth(); ++i)
		{
			const std::vector<T> column = std::move(getColumn(i));
			const int numberOfZeros = std::count(column.begin(), column.end(), 0);

			if (numberOfZeros > max)
			{
				max = numberOfZeros;
				position = std::make_tuple(COLUMN, i);
			}
		}

		return position;
	}

	// ---------------------------------------------------------------------------
	// Return either a posive or negative sign depending on the starting index
	// of the determinant calculation.
	int getDeterminantCalculationPattern(const int startingIndex) const
	{
		int sign = 1;

		if (startingIndex % 2 != 0)
		{
			sign = -1;
		}

		return sign;
	}


	std::vector<std::vector<T>> mMatrix;

	// =========================================================================== 
	// ERROR SECTION
	void initializeErrorCodes()
	{
		std::stringstream ss;
		ss << "Invalid Matrix " << mErrorCodes.STR << " of " << mErrorCodes.STR <<
			". Matrix size will be set to 1x1.";
		mErrorCodes.addError(INVALID_SIZE, ss.str());

		ss = std::stringstream();
		ss << "Invalid matrix row: " << mErrorCodes.STR;
		mErrorCodes.addError(INVALID_ROW_ACCESS, ss.str());

		ss = std::stringstream();
		ss << "Error: The newly added row is " << mErrorCodes.STR <<
			" elements and the matrix width is " << mErrorCodes.STR << " elements.";
		mErrorCodes.addError(INVALID_NEW_ROW, ss.str());

		ss = std::stringstream();
		ss << "Invalid matrix column: " << mErrorCodes.STR;
		mErrorCodes.addError(INVALID_COLUMN_ACCESS, ss.str());
	}

	// ---------------------------------------------------------------------------
	class IndexOutOfBoundsException : public std::exception{};

	// ---------------------------------------------------------------------------
	class UnevenMatricesException : public std::exception{};

	// ---------------------------------------------------------------------------
	class NonMultipliableMatricesException : public std::exception{};

	// ---------------------------------------------------------------------------
	enum ErrorCodes
	{
		INVALID_SIZE,
		INVALID_ROW_ACCESS,
		INVALID_COLUMN_ACCESS,
		INVALID_NEW_ROW,
	};

	// All errors created by this class.
	ErrorManager<ErrorCodes> mErrorCodes;

	// Errors that will be output to the client.
	std::vector<std::string> mErrors;

	// End Error Section
	// =========================================================================== 
};

	// ---------------------------------------------------------------------------
	// Multiplies a matrix by a scalar value in the form of scalar * Matrix
	template <class T>
	Matrix<T> operator*(const int scalar, Matrix<T> matrix) 
	{
		return matrix * scalar;
	}

#endif
