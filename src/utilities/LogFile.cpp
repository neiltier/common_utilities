#include "LogFile.hpp"

#include <cstdio>
#include <sstream>

#include "Utilities.hpp"

bool LogFile::mUnitTesting = false;

const std::string LogFile::mFileSeperator = "---------------------------------------------------";  

// ------------------------------------------------------------------
LogFile::LogFile(const std::string& file)
{
    initializeErrorCodes();

    // Remove the log file if it already exists.
    if (Utilities::pathExists(file))
    {
        bool removedFile = false;

        // If NOT unit testing then remove the file.
        if (!mUnitTesting)
        {
            removedFile = (remove(file.c_str()) == 0);
        }

        // If the file can't be removed then report an error.
        if (!removedFile || mUnitTesting)
        {
            mErrorContainer.push_back(mErrorCodes.getErrorMsg(UNABLE_TO_REMOVE_EXISTING_FILE, { file }));

            // Don't return when unit testing in order to produce the file-open error.
            if (!mUnitTesting)
            {
                return;
            }
        }
    }

    // Create a new file and open the file stream
    mFileStream.open(file, std::ofstream::out);

    // Report an error if the file can't be opened or there is an issue.
    if (!mFileStream.is_open() || !mFileStream.good())
    {
        mErrorContainer.push_back(mErrorCodes.getErrorMsg(UNABLE_TO_OPEN_FILE, { file }));

        return;
    }
}

// ------------------------------------------------------------------
LogFile::~LogFile()
{
    mFileStream.close();
}

// ------------------------------------------------------------------
void LogFile::initializeErrorCodes()
{
    std::stringstream ss;
    ss << "The file: '" << mErrorCodes.STR << "' already exists and cannot be removed.";
    mErrorCodes.addError(UNABLE_TO_REMOVE_EXISTING_FILE, ss.str());

    ss = std::stringstream();
    ss << "The file: '" << mErrorCodes.STR << "' cannot be opened.";
    mErrorCodes.addError(UNABLE_TO_OPEN_FILE, ss.str());
}

// ------------------------------------------------------------------
std::string LogFile::getErrors() const
{
    std::string errorStr;
    for (const std::string& error : mErrorContainer)
    {
        errorStr += error + "\n";
    }

    return errorStr;
}

// ------------------------------------------------------------------
void LogFile::write(const std::string& logString)
{
    mFileStream << std::endl << mFileSeperator << std::endl;
    mFileStream << Utilities::getCurrentDateAndTime() << std::endl;
    mFileStream << logString << std::endl;
}
