#ifndef UTILITIES_HPP
#define UTILITIES_HPP

#include <string>
#include <vector>

// This class defines utilities that can be used throughout the code base.
// All data in this class is intended to be static.
class Utilities
{
public:

    // Returns true if the input file or directory exists and false otherwise.
    static bool pathExists(const std::string& path);

    // Returns the current date and time in a year-month-day-time format 
    static std::string getCurrentDateAndTime();

    // Returns true if the passed-in file was created and false otherwise.
    // If the file already exists then false will be returned.
    static bool createFile(const std::string& file);

    // Returns true if the input file was successfully read to a string and false otherwise.
    // Parameter fileString will output the file contents if the read was successful.
    static bool readFileToString(const std::string& absoluteFilePath, std::string& fileString);

    // Returns true if the source file was successfully copied to the destination.
    // This function copies directories and files.
    static bool copyData(const std::string& source, const std::string& destination);

    // Receives a vector of strings and returns the values into a string where each
    // vector's string element is delimited by the delimeter parameter.
    // If parameter excludeEndDelimeter is set then the last delimeter is excluded
    // at the end of the string.
    static std::string flattenStringVector(const std::vector<std::string>& stringVector,
                                           const std::string& delimeter,
                                           const bool excludeEndDelimeter = true);

    // Returns a string where the passed-in character is added to the beginning and
    // to the end of the passed-in string. If the encapsulation character is null then
    // the original string will be returned.
    static std::string encapsulateString(const std::string& originalString,
                                         const char encapsulationChar);

    // Returns a string where the passed-in character is stripped from the beginning and
    // the end of the passed-in string. If the passed-in string is less than two characters
    // or the character passed-in doesn't exist at the ends of the strings then the original
    // string will be returned.
    static std::string deencapsulateString(const std::string& originalString,
                                           const char deencapsulationChar);

private:

    friend class UtilitiesTest;
};

#endif // UTILITIES_HPP
