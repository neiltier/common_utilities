#include <cstdio>
#include <chrono>
#include <ctime>
#include <fstream>
#include <iomanip>
#include <sys/stat.h>
#include <sstream>

#include "Utilities.hpp"

// ------------------------------------------------------------------
bool Utilities::pathExists(const std::string& path)
{
    struct stat buffer;
    return (stat(path.c_str(), &buffer) == 0);
}

// ------------------------------------------------------------------
std::string Utilities::getCurrentDateAndTime()
{
    auto now = std::chrono::system_clock::now();
    auto in_time_t = std::chrono::system_clock::to_time_t(now);

    std::stringstream ss;
    ss << std::put_time(std::localtime(&in_time_t), "%x %X");
    return ss.str();
}

// ------------------------------------------------------------------
bool Utilities::createFile(const std::string& file)
{
    bool ok = false;

    if (!pathExists(file))
    {
        std::ofstream newFile { file };
        newFile.close();

        ok = newFile.good() && pathExists(file);
    }

    return ok;
}

// ------------------------------------------------------------------
bool Utilities::readFileToString(const std::string& absoluteFilePath, std::string& fileString)
{
    bool ok = false;

    // Open the file.
    std::ifstream fileStream(absoluteFilePath);

    // If the file is open then write it to the string.
    if (fileStream.is_open() && fileStream.good())
    {
        std::stringstream ss;
        ss << fileStream.rdbuf();

        fileString = ss.str();

        ok = true;
    }

    return ok;
}

// ------------------------------------------------------------------
bool Utilities::copyData(const std::string& source, const std::string& destination)
{
    const std::string cmd = "cp -r " + source + " " + destination + " 2> /dev/null";
    return (system(cmd.c_str()) == 0);
}

// ------------------------------------------------------------------
std::string Utilities::flattenStringVector(const std::vector<std::string>& stringVector,
                                           const std::string& delimeter,
                                           const bool excludeEndDelimeter)
{
    std::stringstream ss;

    std::vector<std::string>::const_iterator itr;

    for(itr = std::begin(stringVector); itr != std::end(stringVector); ++itr)
    {
        const bool isLastElement = (itr == std::end(stringVector) - 1);

        if (isLastElement && excludeEndDelimeter)
        {
            ss << *itr;
        }
        else
        {
            ss << *itr << delimeter;
        }
    }

    return ss.str();
}

// ------------------------------------------------------------------
std::string Utilities::encapsulateString(const std::string& originalString,
                                         const char encapsulationChar)
{
    std::string output = encapsulationChar + originalString + encapsulationChar;

    // If the character is null then return the original string.
    if (encapsulationChar == 0)
    {
        output = originalString;
    }

    return output;
}

// ------------------------------------------------------------------
std::string Utilities::deencapsulateString(const std::string& originalString,
                                           const char deencapsulationChar)
{
    std::string output = originalString;

    bool errors = (originalString.size() < 2);
    errors |= (deencapsulationChar == 0);

    // If the string has a valid length then check verify the ends.
    if (!errors)
    {
        std::string::const_iterator frontItr = std::cbegin(output);

        errors |= (*frontItr != deencapsulationChar);
        errors |= (*(std::cend(originalString) - 1) != deencapsulationChar);

        // If there are no errors then strip the string.
        if (!errors)
        {
            output.erase(frontItr);
            output.pop_back();
        }
    }

    return output;
}
