#ifndef ERRORS_HPP
#define ERRORS_HPP

#include <cassert>
#include <map>
#include <string>
#include <vector>


template <class T>
class ErrorManager
{
public:

	// ---------------------------------------------------------------------------
	void addError(const T& errorCode, const std::string& errorMsg)
	{
		assert(mErrors.find(errorCode) == mErrors.end());
		mErrors[errorCode] = errorMsg;
	}

	// ---------------------------------------------------------------------------
	bool operator==(const ErrorManager<T>& errors) const
	{
		return (mErrors == errors.mErrors);
	}

	// ---------------------------------------------------------------------------
	bool operator!=(const ErrorManager<T>& errors) const
	{
		return !(operator==(errors));
	}

	// ---------------------------------------------------------------------------
	ErrorManager<T>& operator=(ErrorManager<T> rhs)
	{
		swap(rhs);
		return *this;
	}

	// ---------------------------------------------------------------------------
	// Get the error message associated with provided error code.
	std::string getErrorMsg(const T& errorCode) const
	{
		assert(mErrors.find(errorCode) != mErrors.end());
		return mErrors.at(errorCode);
	}

	// ---------------------------------------------------------------------------
	// Gets an error message filled in with the supplied errorMsgParameters.
	// NOTE: The number of elements in the input container must be the same number of
	// parameters fields in the error message. The first element of the vector 
	// will go into the first fill-in parameter of the errorMsg,
	// the second will go into the second, etc... 
	std::string getErrorMsg(
		const T& errorCode,
		const std::vector<std::string>& errorMsgParameters) const
	{
		assert(mErrors.find(errorCode) != mErrors.end());

		return replaceErrorMsgParameters(mErrors.at(errorCode), errorMsgParameters);
	}

	// ---------------------------------------------------------------------------
	// Gets an error message filled with the supplied parameter.
	std::string getErrorMsg(
		const T& errorCode, 
		const std::string& errorMsgParameter) const
	{
		assert(mErrors.find(errorCode) != mErrors.end());

		// Input the one parameter into a container.
		std::vector<std::string> parameter {errorMsgParameter};

		return replaceErrorMsgParameters(mErrors.at(errorCode), parameter);
	}

	const std::string STR = "{}";

private:

	// Make the test class a friend.
	friend class ErrorManagerTest;

	// ---------------------------------------------------------------------------
	// Replaces the parameter fields in the provided string with the provided 
	// error message parameters.
	std::string replaceErrorMsgParameters(
		const std::string& errorMsg,
		const std::vector<std::string>& errorMsgParameters) const
	{
		assert(!errorMsgParameters.empty());

		std::string errorMessage = errorMsg;

		size_t npos = errorMessage.find(STR, 0);

		int count = 0;
		while (npos != std::string::npos)
		{
			const std::string newString(errorMsgParameters[count]);

			errorMessage.replace(npos, STR.length(), newString);
			npos += newString.length();
			npos = errorMessage.find(STR, npos + 1);
			++count;
		}

		return errorMessage;
	}

	// ---------------------------------------------------------------------------
	void swap(ErrorManager<T>& rhs)
	{
		std::swap(mErrors, rhs.mErrors);
	}


	std::map<T, std::string> mErrors;
};

#endif
