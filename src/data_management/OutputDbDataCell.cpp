#include "OutputDbDataCell.hpp"

using DataManagement::OutputDbDataCell;

// ------------------------------------------------------------------
OutputDbDataCell::OutputDbDataCell(const std::string& columnName, const std::any& value) :
    mColumnName(columnName),
    mValue(value)
{
}

// ------------------------------------------------------------------
std::string OutputDbDataCell::getColumnName() const
{
    return mColumnName;
}

// ------------------------------------------------------------------
std::any OutputDbDataCell::getColumnValue() const
{
    return mValue;
}
