#ifndef INPUT_DB_DATA_CELL_HPP
#define INPUT_DB_DATA_CELL_HPP

#include <string>
#include <vector>

// Declare the test class in the global namespace.
class DbDataTest;

namespace DataManagement
{

// This class is used to translate object data to a string
// form so that is able to be processed into the
// database. This class represents the column and value data for one
// database table cell.
class InputDbDataCell
{
public:

    // Constructor.
    // Parameter columnName is the data table's column name where the data will be added to.
    // Parameter value is the data value that will be added to the column.
    InputDbDataCell(const std::string& columnName, const std::string& value);

    // Copy constructor.
    InputDbDataCell(const InputDbDataCell& rhs) = default;

    // Move constructor.
    InputDbDataCell(InputDbDataCell&& rhs) = default;

    // Assignment operator.
    InputDbDataCell& operator=(const InputDbDataCell& rhs) = default;

    // Move assignment operator.
    InputDbDataCell& operator=(InputDbDataCell&& rhs) = default;

    // Returns the column name string.
    std::string getColumnName() const;

    // Returns the data value as a string.
    std::string getColumnValue() const;

private:

    // Make the test class a friend.
    friend class ::DbDataTest;

    // The data column name where the data value will be added.
    std::string mColumnName;

    // The data value that will be added to the column.
    std::string mValue;
};

// Type definition for a container of InputDbDataCells.
typedef std::vector<DataManagement::InputDbDataCell> InputDbDataCells_t;


// Type definition for a container of InputDbDataCells_t.
typedef std::vector<InputDbDataCells_t> InputDbData_t;

} // namespace DataManagement

#endif // INPUT_DB_DATA_CELL_HPP
