#include "InputDbDataCell.hpp"
#include "Utilities.hpp"

using DataManagement::InputDbDataCell;

// ------------------------------------------------------------------
InputDbDataCell::InputDbDataCell(const std::string& columnName, const std::string& value) :
    mColumnName(columnName)
{
    // Add quotes to all values.
    mValue = Utilities::encapsulateString(value, '\'');
}

// ------------------------------------------------------------------
std::string InputDbDataCell::getColumnName() const
{
    return mColumnName;
}

// ------------------------------------------------------------------
std::string InputDbDataCell::getColumnValue() const
{
    return mValue;
}
