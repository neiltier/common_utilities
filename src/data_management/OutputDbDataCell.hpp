#ifndef OUTPUT_DB_DATA_CELL_HPP
#define OUTPUT_DB_DATA_CELL_HPP

#include <any>
#include <string>
#include <vector>

// Declare the test class in the global namespace.
class DbDataTest;

namespace DataManagement
{

// This class represents the column and value data for one
// database table value output from the DB.
class OutputDbDataCell
{
public:

    // Constructor.
    // Parameter columnName is the data table's column name.
    // Parameter value is the data value.
    OutputDbDataCell(const std::string& columnName, const std::any& value);

    // Copy constructor.
    OutputDbDataCell(const OutputDbDataCell& rhs) = default;

    // Move constructor.
    OutputDbDataCell(OutputDbDataCell&& rhs) = default;

    // Assignment operator.
    OutputDbDataCell& operator=(const OutputDbDataCell& rhs) = default;

    // Move assignment operator.
    OutputDbDataCell& operator=(OutputDbDataCell&& rhs) = default;

    // Returns the column name string.
    std::string getColumnName() const;

    // Returns the data value as any type.
    std::any getColumnValue() const;

private:

    // Make the test class a friend.
    friend class ::DbDataTest;

    // The data column name.
    std::string mColumnName;

    // The data value.
    std::any mValue;
};

// Type definition for a container of OutputDbDataCells.
typedef std::vector<DataManagement::OutputDbDataCell> OutputDbDataCells_t;


// Type definition for a container of OutputDbDataCells_t.
typedef std::vector<OutputDbDataCells_t> OutputDbData_t;

} // namespace DataManagement

#endif // OUTPUT_DB_DATA_CELL_HPP
