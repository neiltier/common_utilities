#include "SqliteDbManager.hpp"

#include <sqlite3.h>
#include <sstream>

#include "Utilities.hpp"

using DataManagement::SqliteDbManager;

// ------------------------------------------------------------------
SqliteDbManager::SqliteDbManager(const std::string& sqlFileAbsolutePath, LogFile& logFile) :
    mDb(nullptr),
    mLogFile(logFile),
    mConnected(false)
{
    initializeErrorCodes();

    // Verify that a DB file exists.
    if (!Utilities::pathExists(sqlFileAbsolutePath))
    {
        mLogFile.write(mErrorCodes.getErrorMsg(NO_DB_FILE, sqlFileAbsolutePath));
        return;
    }

    // Open the database.
    if (sqlite3_open(sqlFileAbsolutePath.c_str(), &mDb) != SQLITE_OK)
    {
        mLogFile.write(mErrorCodes.getErrorMsg(UNABLE_TO_CONNECT, sqlite3_errmsg(mDb)));
        return;
    }

    mConnected = true;
}

// ------------------------------------------------------------------
SqliteDbManager::~SqliteDbManager()
{
    sqlite3_close_v2(mDb);
}

// ------------------------------------------------------------------
bool SqliteDbManager::isConnected() const
{
    return mConnected;
}

// ------------------------------------------------------------------
bool SqliteDbManager::executeStatement(const std::string& statement)
{
    bool ok = true;

    char* errMsg = nullptr;

    if (sqlite3_exec(mDb, statement.c_str(), nullptr, nullptr, &errMsg) != SQLITE_OK)
    {
        ok = false;

        mLogFile.write(mErrorCodes.getErrorMsg(STATEMENT_ERROR, { statement, errMsg }));

        sqlite3_free(errMsg);
    }

    return ok;
}

// ------------------------------------------------------------------
void SqliteDbManager::initializeErrorCodes()
{
    std::stringstream ss;
    ss << "The file: '" << mErrorCodes.STR << "' does not exist. Cannot open database.";
    mErrorCodes.addError(NO_DB_FILE, ss.str());

    ss = std::stringstream();
    ss << "Unable to connect to database." << std::endl;
    ss << "SQLITE Message: " << mErrorCodes.STR;
    mErrorCodes.addError(UNABLE_TO_CONNECT, ss.str());

    ss = std::stringstream();
    ss << "Error executing the following SQL statement: " << std::endl;
    ss << mErrorCodes.STR << std::endl;
    ss << "SQLITE Message: " << mErrorCodes.STR;
    mErrorCodes.addError(STATEMENT_ERROR, ss.str());

    ss = std::stringstream();
    ss << "Error executing the following SQL query: " << std::endl;
    ss << mErrorCodes.STR << std::endl;
    ss << "SQLITE Message: " << mErrorCodes.STR;
    mErrorCodes.addError(QUERY_ERROR, ss.str());

    ss = std::stringstream();
    ss << "Error executing the following SQL query: " << mErrorCodes.STR << std::endl;
    ss << "Invalid output row size" << mErrorCodes.STR << std::endl;
    mErrorCodes.addError(INVALID_ROW_SIZE, ss.str());
}

// ------------------------------------------------------------------
bool SqliteDbManager::insertData(const std::string& tableName, const DataManagement::InputDbData_t& dataContainer)
{
    std::stringstream statement;

    // The delimiter separating the columns and values in the statement.
    const std::string dataDelimiter = ", ";

    statement << "INSERT INTO " << tableName << " (";

    DataManagement::InputDbData_t::const_iterator itr;
    for(itr = std::begin(dataContainer); itr != std::end(dataContainer); ++itr)
    {
        // If it is the first row then get the column names.
        const bool isFirstRow = (itr == std::begin(dataContainer));
        if (isFirstRow)
        {
            statement << Utilities::flattenStringVector(getColumnNames(*itr), dataDelimiter) << ") ";
            statement << "VALUES ";
        }

        statement << "(" << Utilities::flattenStringVector(getValues(*itr), dataDelimiter) << ")";

        const bool isEndRow = (itr == std::end(dataContainer) - 1);
        if (!isEndRow)
        {
            statement << ", " << std::endl;
        }
    }

    return executeStatement(statement.str());
}

// ------------------------------------------------------------------
bool SqliteDbManager::updateData(const std::string& tableName,
                                 const DataManagement::InputDbData_t& newData,
                                 const DataManagement::InputDbData_t& whereClauseData)
{
    bool success = true;

    assert(newData.size() == whereClauseData.size());

    for(int i = 0; i < newData.size(); ++i)
    {
        if (!updateDataItem(tableName, newData.at(i), whereClauseData.at(i)))
        {
            success = false;
            break;
        }
    }

    return success;
}

// ------------------------------------------------------------------
bool SqliteDbManager::updateDataItem(const std::string& tableName,
                                     const DataManagement::InputDbDataCells_t& newData,
                                     const DataManagement::InputDbDataCells_t& whereClauseData)
{
    std::stringstream statement;

    statement << "UPDATE " << tableName << " SET ";

    const std::vector<std::string> newDataColumns = getColumnNames(newData);
    const std::vector<std::string> newDataValues = getValues(newData);

    for(int i = 0; i < newData.size(); ++i)
    {
        statement << newDataColumns.at(i) << " = " << newDataValues.at(i);

        // If there are more columns in the data container then append a comma
        if (i != newData.size() - 1)
        {
            statement << ", ";
        }
    }

    statement << " WHERE ";

    const std::vector<std::string> whereDataColumns = getColumnNames(whereClauseData);
    const std::vector<std::string> whereDataValues = getValues(whereClauseData);

    for(int i = 0; i < whereClauseData.size(); ++i)
    {

        statement << whereDataColumns.at(i) << " = " << whereDataValues.at(i);

        if (i != whereClauseData.size() - 1)
        {
            statement << " AND ";
        }
    }

    return executeStatement(statement.str());
}

// ------------------------------------------------------------------
std::vector<std::string> SqliteDbManager::getColumnNames(const DataManagement::InputDbDataCells_t& cells)
{
    std::vector<std::string> names;

    for(const DataManagement::InputDbDataCell& cell : cells)
    {
        names.push_back(cell.getColumnName());
    }

    return names;
}

// ------------------------------------------------------------------
std::vector<std::string> SqliteDbManager::getValues(const DataManagement::InputDbDataCells_t& cells)
{
    std::vector<std::string> values;

    for(const DataManagement::InputDbDataCell& cell : cells)
    {
        values.push_back(cell.getColumnValue());
    }

    return values;
}

// ------------------------------------------------------------------
bool SqliteDbManager::retrieveData(const std::string& query, const int rowSize, OutputDbData_t& queriedData)
{
    bool success = true;
    queriedData.clear();

    sqlite3_stmt* stmt = nullptr;

    if (sqlite3_prepare_v2(mDb, query.c_str(), -1, &stmt, NULL) != SQLITE_OK)
    {
        mLogFile.write(mErrorCodes.getErrorMsg(QUERY_ERROR, { query, sqlite3_errmsg(mDb)} ));
        success = false;
    }

    int returnCode = SQLITE_OK;
    while((returnCode = sqlite3_step(stmt)) == SQLITE_ROW)
    {
        DataManagement::OutputDbDataCells_t rowResults;
        try
        {
            for(int i = 0; i < rowSize; ++i)
            {
                const std::string columnName = sqlite3_column_name(stmt, i);

                // If the return result is null then make an empty std::any type.
                std::any value;

                switch(sqlite3_column_type(stmt, i))
                {
                    case SQLITE_INTEGER:
                    {
                        value = sqlite3_column_int(stmt, i);
                        break;
                    }
                    case SQLITE_FLOAT:
                    {
                        value = sqlite3_column_double(stmt, i);
                        break;
                    }
                    case SQLITE_TEXT:
                    {
                        value = std::string(reinterpret_cast<const char*>(sqlite3_column_text(stmt, i)));
                        break;
                    }
                    case SQLITE_BLOB:
                    {
                        value = sqlite3_column_blob(stmt, i);
                        break;
                    }
                    default:
                    {
                        break;
                    }
                };
                rowResults.push_back(DataManagement::OutputDbDataCell(columnName, value));
            }
        }
        catch(...)
        {
            success = false;
            mLogFile.write(mErrorCodes.getErrorMsg(INVALID_ROW_SIZE, { query, std::to_string(rowSize) }));
            break;
        }

        queriedData.push_back(rowResults);
    }

    if (returnCode != SQLITE_DONE)
    {
        // Disaster.
        mLogFile.write(mErrorCodes.getErrorMsg(QUERY_ERROR, { query, sqlite3_errmsg(mDb)} ));
        success = false;
    }

    sqlite3_finalize(stmt);

    return success;
}
