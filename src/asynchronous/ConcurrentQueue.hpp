#ifndef CONCURRENT_QUEUE_HPP
#define CONCURRENT_QUEUE_HPP

/// @file
/// @brief This file declares the ConcurrentQueue class.

#include <condition_variable>
#include <memory>
#include <mutex>

// Forward declare the test class.
class ConcurrentQueueTest;

namespace async
{

/// @brief This class provides a queue that uses locks to share data between multiple threads.
/// It is a generic templated class where each element in the queue is an instance of type T.
template <class T>
class ConcurrentQueue
{
    // Add the test class as a friend.
    friend class ::ConcurrentQueueTest;

    /// @brief Node in the queue that holds the data for an element in the container
    /// as well as points to the address of the next available node.
    struct Node
    {
        /// @brief The node's stored data.
        std::shared_ptr<T> mData;

        /// @brief Pointer to the node that is next in the queue.
        std::unique_ptr<Node> mNext;
    };

    /// @brief Locks the tail mutex then returns the tail node.
    //
    /// @returns Node pointing to the tail of the queue.
    Node* getTail()
    {
        std::lock_guard<std::mutex> tailLock(mTailMutex);

        return mTail;
    }

    /// @brief Locks the head mutex then waits for the head node to not equal the
    /// tail node. Once the waiting is finished the lock is returned.
    //
    /// @returns Pointer to the head node lock.
    std::unique_lock<std::mutex> waitForData()
    {
        std::unique_lock<std::mutex> headLock(mHeadMutex);
        mDataCondition.wait(headLock, [&] { return mHead.get() != getTail(); });

        return std::move(headLock);
    }

    /// @brief Retrieves the head of queue and returns it as well as replaces the head node with
    /// the next node in the queue.
    //
    /// @returns Pointer to former head node of the queue that was popped.
    std::unique_ptr<Node> popHead()
    {
        std::unique_ptr<Node> oldHead = std::move(mHead);
        mHead = std::move(oldHead->mNext);

        return oldHead;
    }

    /// @brief Locks the head node then retrieves the element that is next in the queue. If there are 
    /// no elements available in the queue then a non-initialized node pointer is returned. Otherwise,
    /// the head is popped and its value is returned as a pointer.
    //
    /// @returns Pointer to former head node of the queue that was popped. If there is no data in
    /// the queue then a non-initialized node pointer is returned.
    std::unique_ptr<Node> tryPopHead()
    {
        std::lock_guard<std::mutex> headLock(mHeadMutex);

        if (mHead.get() == getTail())
        {
            return std::unique_ptr<Node>();
        }

        return popHead();
    }

    /// @brief Locks the head node then retrieves the element that is next in the queue. If there are 
    /// no elements available in the queue then a non-initialized node pointer is returned. Otherwise,
    /// the head is popped and its value is returned as a pointer.
    //
    /// @param[out] value The value of the former head node that was popped. If the queue has no data
    /// then the value is not updated.
    //
    /// @returns Pointer to former head node of the queue that was popped. If there is no data in
    /// the queue then a non-initialized node pointer is returned.
    std::unique_ptr<Node> tryPopHead(T& value)
    {
        std::lock_guard<std::mutex> headLock(mHeadMutex);

        if (mHead.get() == getTail())
        {
            return std::unique_ptr<Node>();
        }

        value = std::move(*mHead->mData);

        return popHead();
    }

    /// @brief Blocks execution until there is data to pop from the queue, replaces the head node with
    /// the previous node then returns the popped head node.
    //
    /// @returns Pointer to former head node of the queue that was popped.
    std::unique_ptr<Node> waitPopHead()
    {
        std::unique_lock<std::mutex> headLock(waitForData());
        
        return popHead();
    }

    /// @brief Blocks execution until there is data to pop from the queue, replaces the head node with the
    /// the previous node, updates the passed-in value with the old head node data, and returns the pointer
    /// to the old head node.
    //
    /// @param[out] value The value of the former head node that was popped.
    //
    /// @returns Pointer to former head node of the queue that was popped.
    std::unique_ptr<Node> waitPopHead(T& value)
    {
        std::unique_lock<std::mutex> headLock(waitForData());
        value = std::move(*mHead->mData);

        return popHead();
    }

    /// @brief Makes a copy of the head node's data and returns a pointer of it.
    //
    /// @returns A shared pointer containing a copy of the data held by the head node. If no data
    /// exists then a non-initialized pointer is returned.
    std::shared_ptr<T> getHeadData()
    {
        if (mHead->mData)
        {
            return std::make_shared<T>(*mHead->mData);
        }

        return std::shared_ptr<T>();
    }

    /// @brief Locks mutex and blocks execution until there is data available
    /// in the queue then returns the head node's data.
    //
    /// @returns A shared pointer containing a copy of the data held by the head node.
    std::shared_ptr<T> waitPeekHead()
    {
        std::unique_lock<std::mutex> headLock(waitForData());
        return getHeadData();
    }

    /// @brief Locks mutex then tries to retrieve the data at the head of the queue. 
    //
    /// @returns A shared pointer containing a copy of the data held by the head node. If no data
    /// exists then a non-initialized pointer is returned.
    std::shared_ptr<T> tryPeekHead()
    {
        std::lock_guard<std::mutex> headLock(mHeadMutex);
        return getHeadData();
    }

    /// @brief Condition variable that synchronizes that data entering and leaving the queue.
    std::condition_variable mDataCondition;

    /// @brief Mutex lock for the head node of the queue.
    std::mutex mHeadMutex;

    /// @brief Pointer to the queue's head node.
    std::unique_ptr<Node> mHead;

    /// @brief Mutex lock for the tail node of the queue.
    std::mutex mTailMutex;

    /// @brief Pointer to the queue's tail node.
    Node* mTail;

public:

    /// @brief Initialized a queue of size 0 where the head and the tail are equal.
    ConcurrentQueue() :
        mHead(new Node),
        mTail(mHead.get())
    {
    }

    /// @brief Copy constructor. Deleted to avoid being used with object.
    ConcurrentQueue(const ConcurrentQueue& rhs) = delete;

    /// @brief Copyable assignment operator. Deleted to avoid being used with object.
    ConcurrentQueue& operator=(const ConcurrentQueue& rhs) = delete;

    /// @brief Determines if the queue contains data.
    //
    /// @returns Returns true if the queue has 0 elements and false otherwise.
    bool empty()
    {
        std::lock_guard<std::mutex> headLock(mHeadMutex);
        return (mHead.get() == getTail());
    }

    /// @brief adds the passed in value to the back of the queue.
    //
    /// @param[in] value The value to be inserted into the queue.
    void push(T value)
    {
        // Create the new nodes as smart pointers outside of the mutex.
        std::shared_ptr<T> newData(std::make_shared<T>(std::move(value)));
        std::unique_ptr<Node> newNode(new Node);

        // Update the node. Since mHead and mTail initially point to the same memory, mHead 
        // initially sets the mHead->mNext pointer to point to mTail. After the first node
        {
            // Lock the mutex on the tail of the data.
            std::lock_guard<std::mutex> tailLock(mTailMutex);

            // Update the the pointer data.
            mTail->mData = newData;

            // Create the new tail
            Node* const newTail = newNode.get();

            // Set the new node pointer to mTail's next value.
            mTail->mNext = std::move(newNode);

            // Point mTail to mTail->mNext
            mTail = newTail;
        }

        // Notify listeners of variable that they can stop waiting.
        mDataCondition.notify_one();
    }

    /// @brief Non-blocking function that attempts to retrieve a copy of the head node's data. If
    /// the data is unable to be obtained then a non-intialized pointer is returned.
    //
    /// @returns A pointer to a copy of the queue's head node data. A non-initialized pointer is
    /// returned if data was not able to be obtained.
    std::shared_ptr<T> tryPeek()
    {
        return tryPeekHead();
    }

    /// @brief Non-blocking function that attempts to retrieve a copy of the head node's data. If
    /// the data is unable to be obtained then false is returned and the passed-in value is not updated.
    //
    /// @param[out] value The value returned from the head of the queue. If data was not able to be obtained
    /// then the value is not updated.
    //
    /// @returns True if the queue's head node was retrieved, false otherwise.
    bool tryPeek(T& value)
    {
        std::shared_ptr<T> data = tryPeekHead();

        if (data)
        {
            value = std::move(*data);
        }

        return data.get();
    }

    /// @brief Non-blocking function that attempts to retrieve and remove the next element in the queue.
    /// If no element exists then a non-initialized pointer is returned. 
    //
    /// @returns A shared pointer containing a copy of the data held by the head node. If no data
    /// exists then a non-initialized pointer is returned.
    std::shared_ptr<T> tryPop()
    {
        std::unique_ptr<Node> oldHead = tryPopHead();

        return oldHead ? oldHead->mData : std::shared_ptr<T>();
    }

    /// @brief Non-blocking function that attempts to retrieve and remove the next element in the queue.
    /// If no element exists then false is returned and the parameter is not updated.
    //
    /// @param[out] value The value returned from the head of the queue. If data was not able to be obtained
    /// then the value is not updated.
    //
    /// @returns True if the queue's head node was popped, false otherwise.
    bool tryPop(T& value)
    {
        std::unique_ptr<Node> const oldHead = tryPopHead(value);

        return oldHead.get();
    }

    /// @brief Blocks execution while waiting for data to be copied from the head of the queue.
    /// Once data is available it is returned.
    //
    /// @returns A pointer to a copy of the queue's head node data.
    std::shared_ptr<T> waitAndPeek()
    {
        return waitPeekHead();
    }

    /// @brief Blocks execution while waiting for data to be copied from the head of the queue.
    /// Once data is available the value parameter is updated.
    //
    /// @param[out] value The value from the head node of the queue.
    void waitAndPeek(T& value)
    {
        std::shared_ptr<T> data = waitPeekHead();

        if (data)
        {
            value = std::move(*data);
        }
    }

    /// @brief Blocks execution while waiting for data to be popped from the queue. Once data
    /// is available a pointer to the data is returned.
    //
    /// @returns shared_ptr encapsulating the data removed from the queue.
    std::shared_ptr<T> waitAndPop()
    {
        std::unique_ptr<Node> const oldHead = waitPopHead();

        return oldHead->mData;
    }

    /// @brief Blocks execution while waiting for data to be popped from the queue. Once data
    /// is available the data is updated in the value parameter.
    //
    /// @param[out] value The value returned from the head of the queue.
    void waitAndPop(T& value)
    {
        std::unique_ptr<Node> const oldHead = waitPopHead(value);
    }
};

} // namespace async

#endif /// CONCURRENT_QUEUE_HPP

