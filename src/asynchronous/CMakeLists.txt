
add_library(asynchronous
    AbstractStoppableThread.cpp
)

target_include_directories(asynchronous PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
target_link_libraries(asynchronous pthread)

