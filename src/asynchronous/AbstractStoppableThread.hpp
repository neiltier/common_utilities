#ifndef ABSTRACT_STOPPABLE_THREAD_HPP
#define ABSTRACT_STOPPABLE_THREAD_HPP

/// @file
/// @brief This file declares the AbstractStoppableThread class.

#include <future>

namespace async
{

/// @brief This class encapsulates a promise and future object and
/// provides and API to set an exit signal for a thread.
class AbstractStoppableThread
{
public:

    AbstractStoppableThread(const AbstractStoppableThread&) = delete;
    AbstractStoppableThread& operator=(const AbstractStoppableThread&) = delete;

    /// @brief Default constructor.
    AbstractStoppableThread();

    /// @brief Move Constructor.
    //
    /// @param [in/out] obj Object whose data is being moved to the current object.
    AbstractStoppableThread(AbstractStoppableThread&& obj);

    /// @brief Move assignment operator.
    //
    /// @param [in/out] obj Object whose data is being move to the current object.
    //
    /// @returns A new AbstractStoppableThread object with the data moved from the assignee object.
    AbstractStoppableThread& operator=(AbstractStoppableThread&& obj);

    /// @brief Destructs the object and calls any children object destructors.
    virtual ~AbstractStoppableThread() = default;

    /// @brief Pure virtual function needed to be run be threads that inherit this class.
    virtual void run() = 0;

    /// @brief Requests the thread to stop by setting a value in the exit signal promise object.
    void stop();

protected:

    /// @brief Check if thread is requested to stop running.
    //
    /// @returns True if the thread is requested to stop and false if it was not.
    bool stopRequested();

    /// @brief Overloaded parenthesis operator thread function to be executed by the thread.
    void operator()();

private:
        
        /// @brief Promise object that defines the thread's exit signal.
        std::promise<void> mPromiseExitSignal;

        /// @brief Future object that receives the the thread's exit signal.
        std::future<void> mFutureExitSignal;

};

} // namespace async

#endif /// ABSTRACT_STOPPABLE_THREAD_HPP

