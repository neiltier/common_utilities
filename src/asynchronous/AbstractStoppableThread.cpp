// This file defines the AbstractStoppableThread class.

#include "AbstractStoppableThread.hpp"

#include <chrono>

using async::AbstractStoppableThread;

// ------------------------------------------------------------------
AbstractStoppableThread::AbstractStoppableThread() :
    mFutureExitSignal(mPromiseExitSignal.get_future())
{
}

// ------------------------------------------------------------------
AbstractStoppableThread::AbstractStoppableThread(AbstractStoppableThread&& obj) :
    mPromiseExitSignal(std::move(obj.mPromiseExitSignal)),
    mFutureExitSignal(std::move(obj.mFutureExitSignal))
{
}

// ------------------------------------------------------------------
AbstractStoppableThread& AbstractStoppableThread::operator=(AbstractStoppableThread&& obj)
{
    mPromiseExitSignal = std::move(obj.mPromiseExitSignal);
    mFutureExitSignal = std::move(obj.mFutureExitSignal);

    return *this;
}

// ------------------------------------------------------------------
void AbstractStoppableThread::operator()()
{
    run();
}

// ------------------------------------------------------------------
bool AbstractStoppableThread::stopRequested()
{
    // Returns true if there is a future object available.
    return (mFutureExitSignal.wait_for(std::chrono::milliseconds(0)) != std::future_status::timeout);
}

// ------------------------------------------------------------------
void AbstractStoppableThread::stop()
{
    mPromiseExitSignal.set_value();
}
