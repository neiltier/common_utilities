#include "TcpSocketClient.hpp"

#include "SocketException.hpp"

using communication::TcpSocketClient;

// ---------------------------------------------------------------------------
TcpSocketClient::TcpSocketClient(sockaddr_in serverAddress)
{
    // Validate the server address.
    validateAddress(serverAddress);

    // Create the socket.
    const int protocolFlag = 0;
    defineSocket(AF_INET, SOCK_STREAM, protocolFlag);

    // Connect this client to the server.
    if (connect(mSocketDescriptor, (sockaddr*)&serverAddress, sizeof(serverAddress)) == SOCKET_ERROR)
    {
        throw SocketException("Unable to connect to the server from the client", errno);
    }
}

// ---------------------------------------------------------------------------
void TcpSocketClient::receiveData(void* buffer, const size_t bufferSize)
{
    AbstractSocket::receiveData(mSocketDescriptor, buffer, bufferSize); 
}
