#ifndef ABSTRACT_MSG_HPP
#define ABSTRACT_MSG_HPP

/// @file
/// @brief This file declares the AbstractMsg interface class as well
/// as the StopProcessing message.

namespace communication
{

/// @brief This class represents a message that is processed through a message
/// processing interface.
class AbstractMsg
{
public:

    /// @brief Reserved message IDs used for operation.
    enum InfoMsg 
    {
        UNINITIALIZED,
        STOP_PROCESSING
    };

    /// @brief Constructs a default message ID that is set to a
    /// default unitialized value.
    AbstractMsg() :
        mId(UNINITIALIZED) {}

    /// @brief Gets the message ID.
    //
    /// @returns A message ID.
    uint32_t getId() const { return mId; }

protected:

    /// @brief The message's ID.
    uint32_t mId;
};

// ------------------------------------------------------------------
// ------------------------------------------------------------------
/// @brief This class defines the Stop processing message which
/// is used to stop processing on any IMsgService implementation.
class StopProcessingMsg : public AbstractMsg
{

public:

    /// @brief Constructor that sets the message ID.
    StopProcessingMsg(){ mId = STOP_PROCESSING; }
};

} // namespace communication

#endif // ABSTRACT_MSG_HPP


