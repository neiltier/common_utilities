/////////////////////////////////////////////////////////////////////
//
// This file implements the Abstract Socket class.
//
/////////////////////////////////////////////////////////////////////

#include "AbstractSocket.hpp"

#include <arpa/inet.h>
#include <errno.h>
#include <sys/socket.h>
#include <unistd.h>

#include "SocketException.hpp"

using communication::AbstractSocket;

const int AbstractSocket::SOCKET_ERROR = -1;
const int AbstractSocket::UNDEFINED_SOCKET_DESCRIPTOR = -1;

// ------------------------------------------------------------------
AbstractSocket::AbstractSocket() :
    mSocketDescriptor(UNDEFINED_SOCKET_DESCRIPTOR)
{
}

// ------------------------------------------------------------------
AbstractSocket::~AbstractSocket()
{
    closeSocket();
}

// ------------------------------------------------------------------
void AbstractSocket::closeSocket()
{
    if (getSocketDescriptor() != UNDEFINED_SOCKET_DESCRIPTOR)
    {
        if (close(getSocketDescriptor()) == SOCKET_ERROR)
        {
            throw SocketException("Unable to close the socket", errno);
        }

        mSocketDescriptor = UNDEFINED_SOCKET_DESCRIPTOR;
    }
}

// ------------------------------------------------------------------
int AbstractSocket::getSocketDescriptor() const
{
    return mSocketDescriptor;
}

// ------------------------------------------------------------------
void AbstractSocket::defineSocket(const int domain, const int type, const int protocol)
{
    const int descriptor = socket(domain, type, protocol);

    if (descriptor == SOCKET_ERROR)
    {
        throw communication::SocketException("Unable to define socket", errno);
    }

    mSocketDescriptor = descriptor;
}

// ------------------------------------------------------------------
void AbstractSocket::bindSocket(const sockaddr_in& address)
{
    const socklen_t addressLength = sizeof(address);

    validateAddress(address);

    if (bind(mSocketDescriptor, reinterpret_cast<const sockaddr*>(&address), addressLength) == SOCKET_ERROR)
    {
        throw communication::SocketException("Unable to bind socket", errno);
    }
}

// ------------------------------------------------------------------
void AbstractSocket::receiveData(const int socketDescriptor, void* buffer, const size_t bufferSize)
{
    // Throws exception if any of the parameters are invalid.
    validateTransmissionData(socketDescriptor, buffer, bufferSize);

    const int bytesRead = read(socketDescriptor, buffer, bufferSize);

    if (bytesRead == 0 || bytesRead == SOCKET_ERROR)
    {
        throw communication::SocketException("Error when reading message", errno);
    }
}

// ------------------------------------------------------------------
void AbstractSocket::sendData(const int socketDescriptor, const void* buffer, const ssize_t bufferSize)
{
    // Throws exception if any of the parameters are invalid.
    validateTransmissionData(socketDescriptor, buffer, bufferSize);

    int error_code;
    socklen_t error_code_size = sizeof(error_code);
    int i = getsockopt(socketDescriptor, SOL_SOCKET, SO_ERROR, &error_code, &error_code_size);

    const ssize_t bytesSent = write(socketDescriptor, buffer, bufferSize);

    // Throw an exception if there is an error returned from the write command.
    // or all of the data didn't send.
    if (bytesSent != bufferSize || bytesSent == -1)
    {
        throw communication::SocketException("Error when sending data", errno);
    }
}

// ------------------------------------------------------------------
void AbstractSocket::validateAddress(const sockaddr_in& address) const
{
    char buffer[INET_ADDRSTRLEN];
    const socklen_t addressLength = sizeof(address);

    bool valid = (address.sin_family == AF_INET);
    valid &= (NULL != inet_ntop(address.sin_family, &address.sin_addr, buffer, addressLength));

    if (!valid)
    {
        throw communication::SocketException("Invalid sockaddr_in address", errno);
    }
}

// ------------------------------------------------------------------
bool AbstractSocket::validateTransmissionData(
        const int socketDescriptor,
        const void* buffer,
        const ssize_t bufferSize) const
{
    bool success = buffer && (socketDescriptor > UNDEFINED_SOCKET_DESCRIPTOR) && (bufferSize > 0);

    if (!success)
    {
        const std::string error = "Error with the " + std::string(__func__) + " parameters.";
        throw communication::SocketException(error);
    }

    return success;
}
