#ifndef SOCKET_EXCEPTION_HPP
#define SOCKET_EXCEPTION_HPP

/// @file
/// @brief This file declares the Socket Exception class. This exception
/// is thrown by the socket classes used by the communication module.

#include <exception>
#include <string>

namespace communication
{

/// @brief Defines an exception class used for indicating errors related to
/// the socket classes used by the communication module.
class SocketException : public std::exception
{

public:

    /// @brief Constructs an exception using the passed-in data.
    //
    /// @param [in] message The message that will be output by the overloaded 
    /// what() call.
    //
    /// @param [in] errnum The error value that is intended to be the C++
    /// errno value that is added to the output message. By default this value
    /// is set to 0 which equates to a SUCCESSFUL errno. 
    SocketException(const std::string& message, const int errnum = 0) throw();

    /// @brief Overrides the what() function to output the exception message.
    //
    /// @returns The exception message.
    const char* what() const throw();

private:

    /// @brief The exception message.
    std::string mMessage;
};

} // namespace communication

#endif // SOCKET_EXCEPTION_HPP
