/////////////////////////////////////////////////////////////////////
//
// This file implements the Socket Exception class.
//
/////////////////////////////////////////////////////////////////////

#include "SocketException.hpp"

#include <string.h>

using communication::SocketException;

// ------------------------------------------------------------------
SocketException::SocketException(const std::string& message, const int errnum) throw() :
       mMessage(message)
{
    if (errnum != 0)
    {
        mMessage += " - System Error Code: "  + std::string(strerror(errnum));
    }
}

// ------------------------------------------------------------------
const char* SocketException::what() const throw()
{
    return mMessage.c_str();
}
