/////////////////////////////////////////////////////////////////////
//
// This file implements the AsyncMsgService class.
//
/////////////////////////////////////////////////////////////////////

#include "AsyncMsgService.hpp"

using communication::AbstractMsg;
using communication::AsyncMsgService;

// ------------------------------------------------------------------
AsyncMsgService::AsyncMsgService(std::function<void(std::shared_ptr<AbstractMsg> msg)>& reactor)
{
    mThread = std::make_unique<std::thread>(
                [&](std::function<void(std::shared_ptr<AbstractMsg> msg)> reactor)
    {
        std::shared_ptr<AbstractMsg> msg;

        while(true)
        {
            if (mPromiseAndFuture.ready())
            {
                msg = mPromiseAndFuture.getAndReset();

                if (msg->getId() == AbstractMsg::STOP_PROCESSING)
                {
                    break;
                }

                reactor(msg);
            }
        }
    }, reactor);
}

// ------------------------------------------------------------------
AsyncMsgService::~AsyncMsgService()
{
    std::shared_ptr<AbstractMsg> msg(new communication::StopProcessingMsg);

    processMessage(msg);

    if (mThread->joinable())
    {
        mThread->join();
    }
}

// ------------------------------------------------------------------
void AsyncMsgService::processMessage(std::shared_ptr<AbstractMsg> msg)
{
    bool success = false;

    while(!success)
    {
        success = mPromiseAndFuture.set(msg);
    }
}


