#ifndef ASYNC_MSG_SERVICE_HPP
#define ASYNC_MSG_SERVICE_HPP

/// @file
/// @brief This file declares the Asynchronous message service.
// I'd like to point out that this code is a "re-work" of the github
// code that I found in the cpp-thread-communication master repo.
// Thank you sir.

#include <functional>
#include <future>
#include <thread>

#include "AbstractMsg.hpp"
#include "IMsgService.hpp"

namespace communication
{

/// @brief Implements the IMsgInterface interface in a way so that messages are processed
/// between threads using a promise/future communication method.
class AsyncMsgService : public communication::IMsgService
{
public:


    /// @brief Constructor.
    //
    /// @param[in/out] reactor Reacts to the messages that are processed.
    AsyncMsgService(std::function<void(std::shared_ptr<AbstractMsg> msg)>& reactor);

    /// @brief Destructor. Stops and waits for the managed thread to exit.
    ~AsyncMsgService();

    /// @brief Sends a message to the thread that this instance manages.
    //
    /// @param[in] msg The message that will be processed by the managed thread.
    void processMessage(std::shared_ptr<AbstractMsg> msg);

private:

    /// @brief Thread-safe wrapper for an std::promise/future pair that is
    /// used with the passed-in template type.
    template <class T>
    class PromiseAndFuture
    {
    public:

        /// @brief Constructor.
        PromiseAndFuture()
        {
            reset();
        }

        /// @brief Resets the values of the promised and future values.
        void reset()
        {
            mPromise.reset(nullptr);
            mFuture.reset(nullptr);

            mPromise = std::make_unique<std::promise<T>>();
            mFuture = std::make_unique<std::future<T>>(mPromise->get_future());
        }

        /// @brief Non-blocking function that returns whether or not managed future object
        /// has a valid value.
        //
        /// @returns True if the future object has a valid value and false otherwise.
        bool ready()
        {
            const std::future_status status = mFuture->wait_for(std::chrono::milliseconds(0));

            return (status == std::future_status::ready);
        }

        /// @brief Blocking function that gets the future object's values
        /// and then resets the future value.
        //
        /// @returns The template object that is passed into this class.
        T getAndReset()
        {
            std::lock_guard<std::mutex> lockGuard(mMutex);
            T returnVal = mFuture->get();

            // Reset the promise/future values since the exchance was fulfilled.
            reset();

            return returnVal;
        }

        /// @brief Sets the promise object to a value. This makes the future be
        /// in the ready status and contain a value.
        //
        /// @param[in] value The value that will be set in the future object.
        //
        /// @returns False if the future object is already in a ready status and
        /// true otherwise.
        bool set(const T& value)
        {
            std::lock_guard<std::mutex> lockGuard(mMutex);

            bool success = false;

            if (!ready())
            {
                mPromise->set_value(value);
                success = true;
            }

            return success;
        }

    private:

        /// @brief Pointer to the promise object.
        std::unique_ptr<std::promise<T>> mPromise;

        /// @brief Pointer to the future object.
        std::unique_ptr<std::future<T>> mFuture;

        /// @brief Mutex lock for the thread.
        std::mutex mMutex;
    };

    /// @brief Thread that is running in the background and is managed by this class.
    std::unique_ptr<std::thread> mThread;

    /// @brief The instance of the Promise and future pair used for passing messages.
    PromiseAndFuture<std::shared_ptr<AbstractMsg>> mPromiseAndFuture;
};

} // namespace communication

#endif // ASYNC_MSG_SERVICE_HPP
