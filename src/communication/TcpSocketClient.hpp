#ifndef TCP_SOCKET_CLIENT_HPP
#define TCP_SOCKET_CLIENT_HPP

/// @file
/// @brief This file declares the TCP Socket Client class.

#include <sys/types.h>

#include "AbstractSocket.hpp"

class TcpSocketClientTest;

namespace communication
{

/// @brief Implements a socket client that connects to a server 
/// using the TCP protocol.
class TcpSocketClient : public AbstractSocket
{

public:

    /// @brief Constructor. Receives the socket server address and
    /// connects to it.
    //
    /// @throws SocketException.
    TcpSocketClient(sockaddr_in serverAddress);

    void receiveData(void* buffer, const size_t bufferSize);

private:

    // Make the test class a friend.
    friend class ::TcpSocketClientTest;
};

} // namespace communication

#endif // TCP_SOCKET_CLIENT_HPP
