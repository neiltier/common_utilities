
#ifndef IMSG_SERVICE_HPP
#define IMSG_SERVICE_HPP

#include <memory>

/// @file
/// @brief This file declares IMsgService interface.

namespace communication
{

class AbstractMsg;

/// @brief Defines an interface that processes abstract messages.
class IMsgService
{

public:

    /// @brief Processes the passed in message.
    //
    /// @param[in] msg The abstract message that is processed.
    virtual void processMessage(std::shared_ptr<AbstractMsg> msg) = 0;

    /// @brief Virtual destructor that sends stop signame to the managed thread,
    /// and can either wait or detach for the thread to exit.
    virtual ~IMsgService() {}
};

} // namespace communication

#endif // IMSG_SERVICE_HPP
