#ifndef ABSTRACT_SOCKET_HPP
#define ABSTRACT_SOCKET_HPP

/// @file
/// @brief This file declares the Abstract Socket class.

#include <netinet/in.h>

class TcpSocketServerTest;
class AbstractSocketTest;

namespace communication
{

/// @brief This class is inherited as a common socket class.
class AbstractSocket
{
public:

    /// @brief Constructor. Initialiazes the socket descriptor to be undefined.
    AbstractSocket();

    /// @brief Destructor. Closes the socket.
    //
    /// @throws SocketException
    virtual ~AbstractSocket();

    /// @brief Gets the socket descriptor.
    //
    /// @returns The socket descriptor value.
    int getSocketDescriptor() const;

    /// @brief Closes the open socket for the sockets file descriptor. If the
    /// socket is closed successfully then the descriptor is set back to the
    /// undefined value.
    // Throws an exception if the socket can't be closed.
    //
    /// @throws SocketException
    virtual void closeSocket();

    /// @brief Reads data that was sent to the passed-in socket descriptor and updates
    /// the passed-in buffer.
    /// Throws an exception when the descriptor is undefined, the buffer is null, the size
    /// of the buffer is less than 1, or there is a socket reading issue.
    //
    /// @param [in] socketDescriptor The ID for the socket where data is read from.
    //
    /// @param [in/out] buffer The buffer that is populated with data read from the socket.
    //
    /// @param [in] bufferSize The size of the passed-in buffer.
    //
    /// @throws SocketException
    void receiveData(const int socketDescriptor, void* buffer, const size_t bufferSize);

    /// @brief writes the passed-in data to the passed-in socket descriptor.
    /// Throws an exception when the descriptor is undefined, the buffer is null, the size
    /// of the buffer is less than 1, or there is a socket writing issue.
    //
    /// @param [in] socketDescriptor The ID for the socket where data is written to.
    //
    /// @param [in/out] buffer The buffer that is populated with data read from the socket.
    //
    /// @param [in] bufferSize The size of the passed-in buffer.
    //
    /// @throws SocketException
    void sendData(const int socketDescriptor, const void* buffer, const ssize_t bufferSize);

protected:

    // Make the test class a friend.
    friend class ::TcpSocketServerTest;
    friend class ::AbstractSocketTest;

    /// @brief Defines the socket descriptor using the passed communication data.
    /// Throws an exception when an error occurs.
    //
    /// @param [in] domain Specifies the communication protocol used for communcation.
    //
    /// @param [in] type Specifies the communcation semantics for the socket.
    //
    /// @param [in] protocol Specifies a specific protocol for the socket. If the socket only supports
    /// one protocol then 0 is an acceptable parameter.
    //
    /// @throws SocketException 
    void defineSocket(const int domain, const int type, const int protocol);

    /// @brief Binds the passed-in socket address to an already defined socket.
    //
    /// @param [in] address The address that the socket is bound to.
    //
    /// @throws SocketException
    void bindSocket(const sockaddr_in& address);

    /// @brief Validates a socket address.
    //
    /// @param [in] address The address being validated.
    //
    /// @throws SocketException
    void validateAddress(const sockaddr_in& address) const;

    /// @brief Validates that the passed-in data is valid for transmission across a socket.
    /// Throws an exception when the descriptor is undefined, the buffer is null, the size
    /// of the buffer is less than 1.
    //
    /// @param [in] socketDescriptor The ID for the socket where data is written to.
    //
    /// @param [in/out] buffer The buffer that is populated with data read from the socket.
    //
    /// @param [in] bufferSize The size of the passed-in buffer.
    //
    /// @returns true if the passed-in data is valid and false otherwise.
    //
    /// @throws SocketException
    bool validateTransmissionData(const int socketDescriptor, const void* buffer, const ssize_t bufferSize) const;


    /// @brief The descriptor assigned to the socket that inherits this class.
    int mSocketDescriptor;

    /// @brief Error code that typically appears when the C socket library calls
    /// fail.
    static const int SOCKET_ERROR;

    /// @brief The value of a socket descriptor before it is defined. 
    static const int UNDEFINED_SOCKET_DESCRIPTOR;
};

} // namespace communication

#endif // ABSTRACT_SOCKET_HPP

