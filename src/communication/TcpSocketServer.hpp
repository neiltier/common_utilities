#ifndef TCP_SOCKET_SERVER_HPP
#define TCP_SOCKET_SERVER_HPP

/// @file
/// @brief This file declares the TCP Socket Server class.

#include <chrono>
#include <vector>

#include "AbstractSocket.hpp"

class TcpSocketServerTest;
class TcpSocketServerImpl;

namespace communication
{

/// @brief Implements a socket server that manages clients using the TCP protocol.
class TcpSocketServer : public AbstractSocket
{

public:

    /// @brief Type definition of a container that holds socket descriptors.
    typedef std::vector<int> DescriptorContainer_t;

    /// @brief Defines a new socket for the server, sets the socket options, and
    /// binds the socket to the address.
    //  An exception is thrown when the following things occur: The address is invalid,
    //  the socket options are not able to be set, or the socket can't be bound.
    //
    /// @param [in] address The socket address of the server.
    //
    /// @throws SocketException
    TcpSocketServer(sockaddr_in address);

    // TODO
    void listenForAndAcceptConnections(const std::chrono::seconds& acceptanceDuration);

    /// @brief Returns the client descriptors that are connected to the server.
    //
    /// @returns A vector of the server's connected client socket descriptors.
    DescriptorContainer_t getClientDescriptors() const;

    void closeClientSocket(const int clientDescriptor);

    void closeAllClients();

private:

    // Make the test class a friend.
    friend class ::TcpSocketServerTest;
    friend class ::TcpSocketServerImpl;

    /// @brief Commands the socket to listen for new connections and sets
    /// the mListening flag to true.
    //
    /// @throws SocketException
    void listenForConnections();

    /// @brief Sets the socket options.
    //
    /// @throws SocketException
    void setSocketOptions();

    /// @brief The set of file descriptors that are readable.
    fd_set mManagedDesriptors;

    /// @brief Container holding the socket descriptors of all of the connected
    /// socket descriptors.
    DescriptorContainer_t mClientSocketDescriptors;

    /// @brief address of the server socket.
    sockaddr_in mServerAddress;

    bool mListening;

};

} // namespace communication

#endif // TCP_SOCKET_SERVER_HPP

