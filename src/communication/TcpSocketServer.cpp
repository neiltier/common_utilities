
#include "TcpSocketServer.hpp"

#include <algorithm>
#include <unistd.h>
#include <sys/select.h>

#include "SocketException.hpp"

using communication::TcpSocketServer;

// ---------------------------------------------------------------------------
TcpSocketServer::TcpSocketServer(sockaddr_in address) :
    AbstractSocket(),
    mListening(false)
{
    // Throw an exception if the passed in address is not valid
    validateAddress(address);

    mServerAddress = address;

    // Create the socket.
    const int protocolFlag = 0;
    defineSocket(AF_INET, SOCK_STREAM, protocolFlag);

    // Set the socket options.
    // NOTE: This is not tested due to exception above.
    setSocketOptions();

    // Bind the socket to the address.
    bindSocket(address);

    // Initialize the managed descriptors to have only the server's descriptor
    // in it.
    FD_ZERO(&mManagedDesriptors);
    FD_SET(mSocketDescriptor, &mManagedDesriptors);
}

// ---------------------------------------------------------------------------
void TcpSocketServer::setSocketOptions()
{
    const int level = SOL_SOCKET;
    const int options = SO_REUSEADDR | SO_REUSEPORT;
    const int optionValue = 1;
    if (setsockopt(mSocketDescriptor, level, options, &optionValue, sizeof(optionValue)) == SOCKET_ERROR)
    {
        throw SocketException("Unable to set socket options", errno);
    }
}

// ---------------------------------------------------------------------------
void TcpSocketServer::listenForConnections()
{
    if (!mListening)
    {
        const int connectionBacklog = 3;
        if (listen(mSocketDescriptor, connectionBacklog) == SOCKET_ERROR)
        {
            mListening = false;
            throw SocketException("Error listening on socket", errno);
        }

        mListening = true;
    }
}

#include <iostream>
// ---------------------------------------------------------------------------
void TcpSocketServer::listenForAndAcceptConnections(
        const std::chrono::seconds& acceptanceDuration)
{
    if (acceptanceDuration < std::chrono::seconds(1))
    {
        throw SocketException("Time duration is less than 1");
    }

    listenForConnections();

    const std::chrono::time_point<std::chrono::steady_clock> endTime =
            std::chrono::steady_clock::now() + acceptanceDuration;

    while(std::chrono::steady_clock::now() < endTime)
    {
        fd_set tempClientDescriptorSet = mManagedDesriptors;

        std::cerr << "Waiting on select...\n";
        // Documentation says to consider timeout to be uninitialized after select so leave in loop.
        timeval selectTimeout = { acceptanceDuration.count(), 0};

        const int selectReturnValue = select(mSocketDescriptor + 1, &tempClientDescriptorSet, nullptr, nullptr, &selectTimeout);

        if (selectReturnValue == SOCKET_ERROR)
        {
            throw SocketException("Unable to accept new connections - select() failed", errno);
        }

        if (FD_ISSET(mSocketDescriptor, &tempClientDescriptorSet))
        {
            for(int i = 0; i < selectReturnValue; ++i)
            {
                const int newClientDescriptor = accept(mSocketDescriptor, nullptr, nullptr);

                // Throw an exception if an error is returned from the accept call and
                // that error is not an EWOULDBLOCK error. EWOULDBLOCK is an acceptable
                // error to receive since it denotes that all of the sockets have been
                // accepted.
                if (newClientDescriptor == SOCKET_ERROR && errno != EWOULDBLOCK)
                {
                    throw SocketException("Unable to accept new connections", errno);
                }

                // Add the client to the container of client descriptors.
                mClientSocketDescriptors.push_back(newClientDescriptor);

                std::cerr << "New incoming connection: " << newClientDescriptor << std::endl;
                FD_SET(newClientDescriptor, &mManagedDesriptors);
            }
        }
    }

    // No exceptions were thrown and all connections were made so return true.
    std::cerr << "TRUE RETURNED\n\n";
}

// ---------------------------------------------------------------------------
TcpSocketServer::DescriptorContainer_t TcpSocketServer::getClientDescriptors() const
{
    return mClientSocketDescriptors;
}

// ---------------------------------------------------------------------------
void TcpSocketServer::closeClientSocket(const int clientDescriptor)
{
    const std::string error = "Unable to close client descriptor " + std::to_string(clientDescriptor);

    DescriptorContainer_t::const_iterator itr =
            std::find(std::begin(mClientSocketDescriptors), std::end(mClientSocketDescriptors), clientDescriptor);

    if (itr == std::end(mClientSocketDescriptors))
    {
        throw SocketException(error + " because it does not exist.");
    }

    if (close(clientDescriptor) == SOCKET_ERROR)
    {
        throw SocketException(error, errno);
    }

    // Remove the descriptor from the container of managed clients.
    mClientSocketDescriptors.erase(itr);
}

// ---------------------------------------------------------------------------
void TcpSocketServer::closeAllClients()
{
    while(std::cbegin(getClientDescriptors()) != std::cend(getClientDescriptors()))
    {
        closeClientSocket(*std::cbegin(getClientDescriptors()));
    }
}
