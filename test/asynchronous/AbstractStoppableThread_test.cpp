#include "AbstractStoppableThread.hpp"

#include <chrono>
#include <thread>

#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/ui/text/TestRunner.h>

using namespace CppUnit;

using async::AbstractStoppableThread;

class AbstractStoppableThreadTest : public TestFixture
{
public:

	// ---------------------------------------------------------------------------
	void setUp()
	{
	}

	// ---------------------------------------------------------------------------
	void tearDown()
	{
	}

	// ---------------------------------------------------------------------------
	void testStop()
	{
        // CASE: Test that calling stop stops the thread.

        //Creating a thread to execute our task
        std::thread testThread([&]()
        {
            mTask.run();
        });

        // Verify that the thread is joinable since it is still running.
        CPPUNIT_ASSERT(testThread.joinable());
        std::this_thread::sleep_for(std::chrono::seconds(5));

        // Stop the thread.
        mTask.stop();

        // Verify that the thread ended successfully and is joinable.
        testThread.join();
        CPPUNIT_ASSERT(!testThread.joinable());
	}

private:

    // Class that defines a task that inherits an AbstractStoppableThread used for testing.
    class Task : public AbstractStoppableThread
    {
    public:

        // Overloads the run() function that runs in the thread.
        void run()
        {
            // Check if thread is requested to stop.
            while (!stopRequested())
            {
                std::this_thread::sleep_for(std::chrono::milliseconds(100));
            }
        }
    };

    // Task object used for testing.
    Task mTask;

	// ------------------------------------------------------------------------
	CPPUNIT_TEST_SUITE(AbstractStoppableThreadTest);
	CPPUNIT_TEST(testStop);
	CPPUNIT_TEST_SUITE_END();
};

CPPUNIT_TEST_SUITE_REGISTRATION(AbstractStoppableThreadTest);


int main(int argc, char* argv[])
{
	// Get the top level suite from the registry
	CppUnit::Test *suite = CppUnit::TestFactoryRegistry::getRegistry().makeTest(); 
	// Adds the test to the list of test to run
	CppUnit::TextUi::TestRunner runner;
	runner.addTest(suite);

	// Change the default outputter to a compiler error format outputter
	runner.setOutputter(new CppUnit::CompilerOutputter(&runner.result(),
		std::cerr));
	// Run the tests.
	bool wasSucessful = runner.run();

	// Return error code 1 if the one of test failed.
	return wasSucessful ? 0 : 1;
}
