
#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/ui/text/TestRunner.h>

#include "ConcurrentQueue.hpp"

#include <chrono>
#include <numeric>
#include <thread>


using async::ConcurrentQueue;

// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
class ConcurrentQueueTest : public CppUnit::TestFixture
{
public:

   // ---------------------------------------------------------------------------
   void setUp()
   {
       mQueue = std::make_shared<ConcurrentQueue<int>>();

       // Verify queue is empty.
       verifyEmpty(__LINE__, true);
   }

   // ---------------------------------------------------------------------------
   void tearDown()
   {
       mQueue.reset();
   }

   // ---------------------------------------------------------------------------
   void testPush()
   {
       // Verify queue is empty.
       bool isEmpty = true;
       verifyEmpty(__LINE__, isEmpty);
       auto previousTail = mQueue->mTail;

       // Add a value.
       int inputValue = 10090;
       mQueue->push(inputValue);

       // Verify that the tail is empty.
       CPPUNIT_ASSERT(mQueue->mTail->mNext == nullptr);
       CPPUNIT_ASSERT(mQueue->mTail->mData == nullptr);

       // Verify that the head has the updated value.
       CPPUNIT_ASSERT_EQUAL(inputValue, *mQueue->mHead->mData);

       // Verify that the head is pointing to the tail after the first push.
       CPPUNIT_ASSERT_EQUAL(mQueue->mTail, mQueue->mHead->mNext.get());
       previousTail = mQueue->mTail;

       // Add another element.
       inputValue = 55;
       mQueue->push(inputValue);

       isEmpty = false;

       // Verify that the tail is empty and changed.
       CPPUNIT_ASSERT(previousTail != mQueue->mTail);
       CPPUNIT_ASSERT(mQueue->mTail->mNext == nullptr);
       CPPUNIT_ASSERT(mQueue->mTail->mData == nullptr);

       // Verify that the head's next node has the updated value.
       CPPUNIT_ASSERT_EQUAL(inputValue, *mQueue->mHead->mNext->mData);

       // Verify that the head's newest mNext value is pointing to the previous tail.
       CPPUNIT_ASSERT_EQUAL(previousTail, mQueue->mHead->mNext.get());
       previousTail = mQueue->mTail;

       // Add another element.
       inputValue = 8888;
       mQueue->push(inputValue);

       // Verify that the tail is empty and changed.
       CPPUNIT_ASSERT(previousTail != mQueue->mTail);
       CPPUNIT_ASSERT(mQueue->mTail->mNext == nullptr);
       CPPUNIT_ASSERT(mQueue->mTail->mData == nullptr);

       // Verify that the head's newest node has the updated value.
       CPPUNIT_ASSERT_EQUAL(inputValue, *mQueue->mHead->mNext->mNext->mData);

       // Verify that the head's newest mNext value is pointing to the previous tail.
       CPPUNIT_ASSERT_EQUAL(previousTail, mQueue->mHead->mNext->mNext.get());
   }

   // ---------------------------------------------------------------------------
   void testEmpty()
   {
       // CASE: Verify that empty returns true when the queue is empty.
       verifyEmpty(__LINE__, true);
       CPPUNIT_ASSERT(mQueue->empty());

       // CASE: Verify that empty returns true when the queue is empty.
       mQueue->push(9999999);

       verifyEmpty(__LINE__, false);
       CPPUNIT_ASSERT(!mQueue->empty());
   }

   // ---------------------------------------------------------------------------
   void testWaitAndPopPtr()
   {
       const std::vector<int> addedValues = { 10, 55 };

       // CASE: verify that the function blocks until data is able to be
       // added to the queue.
       verifyWaitOperation(__LINE__, POP_PTR, addedValues);


       // CASE: verify that when data is in the queue no wait occurs.
       CPPUNIT_ASSERT_EQUAL(*(std::cbegin(addedValues) + 1), *mQueue->waitAndPop());
   }

   // ---------------------------------------------------------------------------
   void testWaitAndPopValue()
   {
       const std::vector<int> addedValues = { 1392032, 9 };

       // CASE: verify that the function blocks until data is able to be
       // added to the queue.
       verifyWaitOperation(__LINE__, POP_VALUE, addedValues);


       // CASE: verify that when data is in the queue no wait occurs.
       
       int actualValue;
       mQueue->waitAndPop(actualValue);
       CPPUNIT_ASSERT_EQUAL(*(std::cbegin(addedValues) + 1), actualValue);
   }

   // ---------------------------------------------------------------------------
   void testTryPopPtr()
   {
       // CASE: Verify that an empty pointer is returned if no data is in the queue.

       // Verify the queue is empty.
       verifyEmpty(__LINE__, true);

       CPPUNIT_ASSERT(!mQueue->tryPop());

       // CASE: Verify that data is returned when it is in the queue.
       const int expectedValue = 58;
       mQueue->push(expectedValue);

       CPPUNIT_ASSERT_EQUAL(expectedValue, *mQueue->tryPop());

       // Verify queue is empty after.
       verifyEmpty(__LINE__, true);
   }

   // ---------------------------------------------------------------------------
   void testTryPopValue()
   {
       // CASE: Verify that an empty pointer is returned if no data is in the queue.

       // Verify the queue is empty.
       verifyEmpty(__LINE__, true);

       int expectedValue = 2;
       int actualValue = expectedValue;
       CPPUNIT_ASSERT(!mQueue->tryPop(actualValue));
       CPPUNIT_ASSERT_EQUAL(expectedValue, actualValue);


       // CASE: Verify that data is returned when it is in the queue.
       expectedValue = 58;
       mQueue->push(expectedValue);

       CPPUNIT_ASSERT(mQueue->tryPop(actualValue));
       CPPUNIT_ASSERT_EQUAL(expectedValue, actualValue) ;

       // Verify queue is empty after.
       verifyEmpty(__LINE__, true);
   }

   // ---------------------------------------------------------------------------
   void testTryPeekPtr()
   {
       // CASE: Verify that an empty pointer is returned if no data is in the queue.

       // Verify the queue is empty.
       verifyEmpty(__LINE__, true);

       CPPUNIT_ASSERT(!mQueue->tryPeek());


       // CASE: Verify that data is returned when it is in the queue.
       const int expectedValue = 58;
       mQueue->push(expectedValue);

       CPPUNIT_ASSERT_EQUAL(expectedValue, *mQueue->tryPeek());

       // Verify queue is not empty after.
       verifyEmpty(__LINE__, false);
   }

   // ---------------------------------------------------------------------------
   void testTryPeekValue()
   {
       // CASE: Verify that an empty pointer is returned if no data is in the queue.

       // Verify the queue is empty.
       verifyEmpty(__LINE__, true);

       int expectedValue = 2;
       int actualValue = expectedValue;
       CPPUNIT_ASSERT(!mQueue->tryPeek(actualValue));
       CPPUNIT_ASSERT_EQUAL(expectedValue, actualValue);


       // CASE: Verify that data is returned when it is in the queue.
       expectedValue = 58;
       mQueue->push(expectedValue);

       CPPUNIT_ASSERT(mQueue->tryPeek(actualValue));
       CPPUNIT_ASSERT_EQUAL(expectedValue, actualValue) ;

       // Verify queue is empty after.
       verifyEmpty(__LINE__, false);
   }

   // ---------------------------------------------------------------------------
   void testWaitAndPeekPtr()
   {
       const std::vector<int> addedValues = { 10, 55 };

       // CASE: verify that the function blocks until data is able to be
       // added to the queue.
       verifyWaitOperation(__LINE__, PEEK_PTR, addedValues);


       // CASE: verify that when data is in the queue no wait occurs.
       CPPUNIT_ASSERT_EQUAL(*std::cbegin(addedValues) , *mQueue->waitAndPeek());
   }

   // ---------------------------------------------------------------------------
   void testWaitAndPeekValue()
   {
       const std::vector<int> addedValues = { 1392032, 9203, 3290};

       // CASE: verify that the function blocks until data is able to be
       // added to the queue.

       verifyWaitOperation(__LINE__, PEEK_VALUE, addedValues);


       // CASE: verify that when data is in the queue no wait occurs.
       
       int actualValue;
       mQueue->waitAndPeek(actualValue);
       CPPUNIT_ASSERT_EQUAL(*std::cbegin(addedValues), actualValue);
   }

   // ---------------------------------------------------------------------------
   void testThreadIntegration()
   {
       verifyEmpty(__LINE__, true);


       std::vector<int> thread1Values(100);
       std::iota(thread1Values.begin(), thread1Values.end(), 0);

       using namespace std::chrono;
       const auto expectedWaitTime = seconds(1);

       // Add data to queue to after the expected wait time in a different thread.
       std::thread thread1(&ConcurrentQueueTest::addToQueue, this, mQueue, thread1Values, expectedWaitTime);

       // Add more data to queue to after the expected wait time in another different thread.
       std::vector<int> thread2Values(100);
       std::iota(thread2Values.begin(), thread2Values.end(), 100);
       std::thread thread2(&ConcurrentQueueTest::addToQueue, this, mQueue, thread2Values, expectedWaitTime);

       // Add a thread that peeks at the data in order to test that functionality.
       std::vector<int> peekResults;
       const size_t expectedPeekResultSize = 50;
       std::thread thread3([&]
               { 
                    std::this_thread::sleep_for(expectedWaitTime);
                    for(size_t i = 0; i < expectedPeekResultSize; ++i)
                    {
                        int value;
                        mQueue->waitAndPeek(value);
                        peekResults.push_back(value);
                    }
               });

       // Wait for data to be added to the queue.
       const size_t expectedPopResultsSize = 50;
       std::vector<int> popResults;
       int value;
       for(size_t i = 0; i < expectedPopResultsSize; ++i)
       {
           mQueue->waitAndPop(value);
           popResults.push_back(value);
       }

       // Once data was added join the thread.
       thread1.join();
       thread2.join();
       thread3.join();

       std::vector<int> totalElements;
       while(mQueue->tryPop(value))
       {
           totalElements.push_back(value);
       }

       // Verify that waitAndPeek obtained the expected amount of elements.
       CPPUNIT_ASSERT_EQUAL(expectedPeekResultSize, peekResults.size());

       // Verify that waitAndPop obtained the expected amount of elements.
       CPPUNIT_ASSERT_EQUAL(expectedPopResultsSize, popResults.size());


       // Verify that the correct amount of elements are still in the queue.
       CPPUNIT_ASSERT_EQUAL((thread1Values.size() + thread2Values.size()) - popResults.size(), totalElements.size());
   }

private:

   // Enumeration containing types of queue operations.
   enum QueueAccess
   {
       PEEK_VALUE,
       PEEK_PTR,
       POP_VALUE,
       POP_PTR
   };

   // ---------------------------------------------------------------------------
   // Sets the thread to sleep of the passed-in sleep time, then iterates the passed-in values and pushes them to the passed-in queue pointer.
   void addToQueue(const std::shared_ptr<ConcurrentQueue<int>> queue, const std::vector<int>& values, const std::chrono::seconds& sleepTime)
   {
       std::this_thread::sleep_for(sleepTime);

       for(auto i : values)
       {
           queue->push(i);
       }
   }

   // ---------------------------------------------------------------------------
   // Verifies that the blocking call from the queue which is specified by the accessType is able to successfully
   // block execution while there is no data in the queue and is able to continue execution when data is added
   // to the queue by another thread. When the function finishes blocking, the time that it took before continuing
   // is verified. The data returned by the call is also verified to match the data passed-in through the inputValues
   // parameter.
   void verifyWaitOperation(const int line, const QueueAccess accessType, const std::vector<int>& inputValues)
   {
       const std::string lineStr = "Called from line: " + std::to_string(line);

       CPPUNIT_ASSERT_MESSAGE(lineStr, !inputValues.empty());

       verifyEmpty(line, true);

       using namespace std::chrono;
       const auto expectedWaitTime = seconds(1);

       // Start timing how long the wait is.
       steady_clock::time_point t1 = steady_clock::now();

       // Add data to queue in a different thread and then wait the expected time before pushing the data.
       std::thread t(&ConcurrentQueueTest::addToQueue, this, mQueue, inputValues, expectedWaitTime);

       // Determine which wating call to make then wait for data to be added to the queue.
       int actualValue;

       switch(accessType)
       {
           case POP_PTR:
           {
               actualValue = *mQueue->waitAndPop();
               break;
           }
           case POP_VALUE:
           {
               mQueue->waitAndPop(actualValue);
               break;
           }
           case PEEK_PTR:
           {
               actualValue = *mQueue->waitAndPeek();
               break;
           }
           case PEEK_VALUE:
           {
               mQueue->waitAndPeek(actualValue);
               break;
           }
       }

       // Once the functions stop blocking then join the thread.
       t.join();

       // Stop timing.
       steady_clock::time_point t2 = steady_clock::now();


       // Verify that the time that it took to retrieve the data was slightly more than 
       // the passed-in sleep time of the thread.
       const duration<double> timeSpan = duration_cast<duration<double>>(t2 - t1);
       const duration<double> delta = duration<double>(milliseconds(10));
       CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE(lineStr, expectedWaitTime.count(), timeSpan.count(), delta.count());

       // Verify the returned data was the first value added to the queue.
       CPPUNIT_ASSERT_EQUAL_MESSAGE(lineStr, *std::cbegin(inputValues), actualValue);
   }

   // ---------------------------------------------------------------------------
   // Determines if the queue is empty when isEmpty is true and checks if it isn't
   // when isEmpty is false.
   void verifyEmpty(const int line, const bool isEmpty)
   {
       const std::string lineStr = "Called from line: " + std::to_string(line);

       // Verify the head and tail have allocated pointers.
       CPPUNIT_ASSERT_MESSAGE(lineStr, mQueue->mHead.get());
       CPPUNIT_ASSERT_MESSAGE(lineStr, mQueue->mTail);

       if (isEmpty)
       {
           // Verify that the head and the tail are the same pointer initially.
           CPPUNIT_ASSERT_MESSAGE(lineStr, mQueue->mHead.get() == mQueue->mTail);
       }
       else
       {
           CPPUNIT_ASSERT_MESSAGE(lineStr, mQueue->mHead.get() != mQueue->mTail);
       }
   }

   // ---------------------------------------------------------------------------
   CPPUNIT_TEST_SUITE(ConcurrentQueueTest);
   CPPUNIT_TEST(testPush);
   CPPUNIT_TEST(testEmpty);
   CPPUNIT_TEST(testWaitAndPopPtr);
   CPPUNIT_TEST(testWaitAndPopValue);
   CPPUNIT_TEST(testTryPopPtr);
   CPPUNIT_TEST(testTryPopValue);
   CPPUNIT_TEST(testTryPeekPtr);
   CPPUNIT_TEST(testTryPeekValue);
   CPPUNIT_TEST(testWaitAndPeekPtr);
   CPPUNIT_TEST(testWaitAndPeekValue);
   CPPUNIT_TEST(testThreadIntegration);
   CPPUNIT_TEST_SUITE_END();

   // The class being tested.
   std::shared_ptr<ConcurrentQueue<int>> mQueue;
   
};

CPPUNIT_TEST_SUITE_REGISTRATION(ConcurrentQueueTest);


// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
int main(int argc, char* argv[])
{
   // Get the top level suite from the registry
   CppUnit::Test *suite = CppUnit::TestFactoryRegistry::getRegistry().makeTest();

   // Adds the test to the list of test to run
   CppUnit::TextUi::TestRunner runner;
   runner.addTest(suite);
   // Change the default outputter to a compiler error format outputter
   runner.setOutputter(new CppUnit::CompilerOutputter(&runner.result(), std::cerr));

   // Run the tests.
   const bool wasSucessful = runner.run();

   // Return error code 1 if the one of test failed.
   return wasSucessful ? 0 : 1;
}
