#include "SocketTestHelper.hpp"
#include "TcpSocketServer.hpp"

#include <thread>
#include <chrono>
#include <iostream>

int main()
{
    communication::TcpSocketServer server(SocketTestHelper::getDefaultAddress());
    std::cout << "SERVER Descriptor: " << server.getSocketDescriptor() << std::endl;

    server.listenForAndAcceptConnections(std::chrono::seconds(10));


    int desc = server.getClientDescriptors().at(0);

    std::cout << "RECEIVING\n";
    char buffer[3] = {0};
    server.receiveData(desc, buffer, 3); 

//    std::this_thread::sleep_for(std::chrono::seconds(20));

    std::cout << "BUFFER: " << buffer;




    return 0;
}
