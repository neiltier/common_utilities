#ifndef SOCKET_TEST_HELPER_HPP
#define SOCKET_TEST_HELPER_HPP

// This file declares the SocketTestHelper class.


#include <netinet/in.h>
#include <string>

#include <memory>


class TcpSocketServerImpl;

// This class is used as a helper by all of the socket tests.
class SocketTestHelper
{
public:

    // Returns a default valid socket address used for socket testing.
    static sockaddr_in getDefaultAddress();

    // Returns an invalid socket address.
    static sockaddr_in getInvalidAddress();

    // Verifies that the passed-in string is a substring of the passed-in exception string.
    static void verifyExceptionString(const std::string& exceptionStr,
                                      const std::string& expectedPartialStr,
                                      const int sourceLine);

    // Function that is passed into a thread that runs a TcpSocketServer that
    // is controlled by the passed-in TcpSocketServer class.
    static void serverReactor(std::shared_ptr<TcpSocketServerImpl> serverImpl);

    static void serverReactor2(std::shared_ptr<TcpSocketServerImpl> serverImpl);
};

#endif // SOCKET_TEST_HELPER_HPP
