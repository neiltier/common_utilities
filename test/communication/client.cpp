#include "SocketTestHelper.hpp"

#include "TcpSocketClient.hpp"

#include <string>
#include <iostream>
#include <thread>
#include <chrono>
#include <string.h>

int main(int argc, char* argv[])
{
    communication::TcpSocketClient client(SocketTestHelper::getDefaultAddress());

   std::string descriptorStr;
   std::cout << "Input the server descriptor: ";
   std::cin >> descriptorStr;

    std::string::size_type sz;
    int descriptor = std::stoi(descriptorStr, &sz);

    std::this_thread::sleep_for(std::chrono::seconds(10));
    client.sendData(descriptor, "hi!", 3);

    return 0;
}


