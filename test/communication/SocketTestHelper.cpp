// This file defines the SocketTestHelper class.

#include "SocketTestHelper.hpp"

#include <tuple>
#include <string.h>

#include <cppunit/extensions/HelperMacros.h>

// ---------------------------------------------------------------------------
sockaddr_in SocketTestHelper::getDefaultAddress()
{
    return { AF_INET, htons(8080), htonl(uint32_t(INADDR_ANY)) };
}

// ---------------------------------------------------------------------------
sockaddr_in SocketTestHelper::getInvalidAddress()
{
    sockaddr_in address = SocketTestHelper::getDefaultAddress();
    address.sin_family = AF_INET + 1;

    return address;
}

// ---------------------------------------------------------------------------
void SocketTestHelper::verifyExceptionString(
        const std::string& exceptionStr,
        const std::string& expectedPartialStr,
        const int sourceLine)
{
        std::string msg = "Called from line: " + std::to_string(sourceLine);
        msg += " - Actual exception: " + exceptionStr;

        CPPUNIT_ASSERT_MESSAGE(msg, exceptionStr.find(expectedPartialStr) != std::string::npos);
}
