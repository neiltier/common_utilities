
#include "TcpServerTestThread.hpp"

#include <chrono>

#include "SocketTestHelper.hpp"

// ---------------------------------------------------------------------------
TcpServerTestThread::TcpServerTestThread() :
        AbstractStoppableThread(),
        mConnecting(true),
        mConnectionListeningTime(std::chrono::seconds(4)),
        mWaitingForMessage(false)
{
}

// ---------------------------------------------------------------------------
#include <iostream>
void TcpServerTestThread::run()
{
    mServer = std::make_unique<communication::TcpSocketServer>(SocketTestHelper::getDefaultAddress());

    // loop until a stop is requested by an external source.
    while(!stopRequested())
    {
        if (isConnecting())
        {
            // std::lock_guard<std::mutex> lock(mMutex); This causes segfault??
            mServer->listenForAndAcceptConnections(getConnectionListeningTime());

            // Stop listening for connections after the call finishes.
            mConnecting = false;
        }

        if (!mExpectedMessages.empty())
        {
            for(auto msg : mExpectedMessages)
            {
                const int clientDescriptor = std::get<0>(msg);
                const int msgSize = std::get<1>(msg);

                char* buffer = new char[msgSize];
                mWaitingForMessage = true;
                std::cerr << "Client Descriptor" << clientDescriptor << std::endl;
                std::cerr << "SERVER descriptor inside of thread: " << mServer->getSocketDescriptor() << std::endl;
                std::cerr << "receiving data\n";
                //mServer->receiveData(clientDescriptor, buffer, msgSize);
                std::cerr << "PAST RECEIVING DATA!!!!!!!!!!!!!!!!!!\n\n";
                mWaitingForMessage = false;

                data.push_back(buffer);
            }

            // Clear the messages when finished receiving them.
            mExpectedMessages.clear();
        }

        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    }
}

// ---------------------------------------------------------------------------
std::chrono::seconds TcpServerTestThread::getConnectionListeningTime() const
{
//    std::lock_guard<std::mutex> lock(mMutex);
    return mConnectionListeningTime;
}

// ---------------------------------------------------------------------------
bool TcpServerTestThread::isConnecting() const
{
    std::lock_guard<std::mutex> lock(mMutex);
    return mConnecting;
}

// ---------------------------------------------------------------------------
communication::TcpSocketServer::DescriptorContainer_t TcpServerTestThread::getConnectedClients() const
{
//    std::lock_guard<std::mutex> lock(mMutex);
    return mServer->getClientDescriptors();
}

// ---------------------------------------------------------------------------
void TcpServerTestThread::closeClient(const int descriptor)
{
 //   std::lock_guard<std::mutex> lock(mMutex);
    mServer->closeClientSocket(descriptor);
}

// ---------------------------------------------------------------------------
void TcpServerTestThread::closeAllClients()
{
//    std::lock_guard<std::mutex> lock(mMutex);
    mServer->closeAllClients();
}

// ---------------------------------------------------------------------------
void TcpServerTestThread::setExpectedClientMsg(const int clientDescriptor, const int msgSize)
{
    //std::lock_guard<std::mutex> lock(mMutex);
    mExpectedMessages.push_back(std::make_tuple(clientDescriptor, msgSize));
    std::cerr << "Message set!";
}

// ---------------------------------------------------------------------------
int TcpServerTestThread::getServerDescriptor() const
{
//    std::lock_guard<std::mutex> lock(mMutex);
    return mServer->getSocketDescriptor();
}
