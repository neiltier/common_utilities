
#include "AsyncMsgService.hpp"
#include "AbstractMsg.hpp"

#include <iostream>

#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/ui/text/TestRunner.h>

using namespace CppUnit;

// TODO: This test is not a real test. I used it to see if the implementation of the AsyncMsgService
// class actually worked. In the future, I may just scrap this entire class but for now I want to
// keep this test in case more experimentation is needed.

// ---------------------------------------------------------------------------
// These are the "reactor" functions that are passed into the thread.
void serviceFuncOne(std::shared_ptr<communication::AbstractMsg> msg)
{
    std::cout << "ServiceFuncOne - THE MESSAGE ID IS: " << msg->getId() << std::endl;
}

// ---------------------------------------------------------------------------
void serviceFuncTwo(std::shared_ptr<communication::AbstractMsg> msg)
{
    std::cout << "ServiceFuncTwo - THE MESSAGE ID IS: " << msg->getId() << std::endl;
}

// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
class AsyncMsgServiceTest: public TestFixture
{
public:

	// ---------------------------------------------------------------------------
	void setUp()
    {
        std::function<void(std::shared_ptr<communication::AbstractMsg>)> fptrOne = serviceFuncOne;
        mMsgServiceOne = new communication::AsyncMsgService(fptrOne);

        std::function<void(std::shared_ptr<communication::AbstractMsg>)> fptrTwo= serviceFuncTwo;
        mMsgServiceTwo = new communication::AsyncMsgService(fptrTwo);
    }

	// ---------------------------------------------------------------------------
	void tearDown()
	{
        delete mMsgServiceOne;
        delete mMsgServiceTwo;
    }

    // ---------------------------------------------------------------------------
    void testJunk()
    {
    }

private:

    communication::IMsgService* mMsgServiceOne = nullptr;
    communication::IMsgService* mMsgServiceTwo = nullptr;

	// ------------------------------------------------------------------------
    CPPUNIT_TEST_SUITE(AsyncMsgServiceTest);
    CPPUNIT_TEST(testJunk);
    CPPUNIT_TEST_SUITE_END();
};

CPPUNIT_TEST_SUITE_REGISTRATION(AsyncMsgServiceTest);


// ------------------------------------------------------------------------
// ------------------------------------------------------------------------
int main(int argc, char* argv[])
{
	// Get the top level suite from the registry
	CppUnit::Test *suite = CppUnit::TestFactoryRegistry::getRegistry().makeTest(); 
	// Adds the test to the list of test to run
	CppUnit::TextUi::TestRunner runner;
	runner.addTest(suite);

	// Change the default outputter to a compiler error format outputter
	runner.setOutputter(new CppUnit::CompilerOutputter(&runner.result(),
		std::cerr));
	// Run the tests.
	bool wasSucessful = runner.run();

	// Return error code 1 if the one of test failed.
	return wasSucessful ? 0 : 1;
}
