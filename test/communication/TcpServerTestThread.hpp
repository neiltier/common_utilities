#ifndef TCP_SERVER_TEST_THREAD
#define TCP_SERVER_TEST_THREAD

#include "AbstractStoppableThread.hpp"

#include <mutex>

#include "TcpSocketServer.hpp"

// This class defines a thread object that runs a TCP server socket
// used for testing the TCP Sockets.
class TcpServerTestThread : public async::AbstractStoppableThread
{
public:

    // Constructor.
    TcpServerTestThread();

    // Overloaded run function that runs in the thread.
    void run();

    // Returns the time span allowed for listening for new
    // client connections from the server.
    std::chrono::seconds getConnectionListeningTime() const;

    // Returns true if the server is listening for connections, false otherwise.
    bool isConnecting() const;

    // Returns container of descriptors of the clients that are connected.
    communication::TcpSocketServer::DescriptorContainer_t getConnectedClients() const;

    // Closes the passed-in client socket.
    void closeClient(const int descriptor);

    // Closes all the open sockets
    void closeAllClients();

    // Commands the server to listen for a message from the
    // passed-in descriptor with the passed-in message size.
    void setExpectedClientMsg(const int clientDescriptor, const int msgSize);

    // Returns the server's socket descriptor.
    int getServerDescriptor() const;

    // temp remove later.
    std::vector<char*> data;

    // temp move back to private later.
    std::vector<std::tuple<int, int>> mExpectedMessages;

    bool mWaitingForMessage;
private:

    // A TCP socket server.
    std::shared_ptr<communication::TcpSocketServer> mServer;

    // Mutex used to lock the data.
    mutable std::mutex mMutex;

    // Time span passed-in to the listenForAndAcceptConnections call.
    std::chrono::seconds mConnectionListeningTime;

    // Flag indicating if the server is accepting connections.
    bool mConnecting;

    // Container holding the expected message data coming from a client.
    // Value 1 in the tuple is the client descriptor. Value 2 is the
    // expected size of the message.
    //std::vector<std::tuple<int, int>> mExpectedMessages;

};

#endif // TCP_SERVER_TEST_THREAD
