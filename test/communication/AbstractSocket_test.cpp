// This class tests the functionality of the AbstractSocket class.

#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/ui/text/TestRunner.h>

#include "AbstractSocket.hpp"
#include "SocketException.hpp"
#include "SocketTestHelper.hpp"

// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
class AbstractSocketTest: public CppUnit::TestFixture
{
public:

	// ---------------------------------------------------------------------------
	void setUp()
    {
    }

	// ---------------------------------------------------------------------------
	void tearDown()
	{
    }

    // ---------------------------------------------------------------------------
    void testDefineSocket()
    {
        communication::AbstractSocket socket;
        CPPUNIT_ASSERT_EQUAL(communication::AbstractSocket::UNDEFINED_SOCKET_DESCRIPTOR, socket.getSocketDescriptor());

        // CASE: Verify that a Socket exception is thrown and that the descriptor is not
        // defined.
        CPPUNIT_ASSERT_THROW(socket.defineSocket(AF_FILE, SOCK_STREAM, -100), communication::SocketException);
        CPPUNIT_ASSERT_EQUAL(communication::AbstractSocket::UNDEFINED_SOCKET_DESCRIPTOR, socket.getSocketDescriptor());


        // CASE: Verify that when valid values are passed-in that the socket descriptor is defined.
        socket.defineSocket(AF_FILE, SOCK_STREAM, 0);
        CPPUNIT_ASSERT(communication::AbstractSocket::UNDEFINED_SOCKET_DESCRIPTOR != socket.getSocketDescriptor());
    }

    // ---------------------------------------------------------------------------
    void testValidateAddress()
    {
        communication::AbstractSocket socket;

        // CASE: Test that  a valid address returns true.
        bool exceptionThrown = false;
        try
        {
            socket.validateAddress(SocketTestHelper::getDefaultAddress());
        }
        catch(const communication::SocketException& ex)
        {
            exceptionThrown = true;
        }

        CPPUNIT_ASSERT(!exceptionThrown);

        // CASE: Test that an address is considered invalid if the sin_family is not AF_INET.
        exceptionThrown = false;
        try
        {
            socket.validateAddress(SocketTestHelper::getInvalidAddress());
        }
        catch(const communication::SocketException& ex)
        {
            exceptionThrown = true;
        }

        CPPUNIT_ASSERT(exceptionThrown);
    }

    // ---------------------------------------------------------------------------
    void testBindSocket()
    {
        communication::AbstractSocket socket;

        // CASE: Test that an exception is thrown if an invalid socket address is passed-in.
        bool exceptionThrown = false;
        try
        {
            socket.bindSocket(SocketTestHelper::getInvalidAddress());
        }
        catch(communication::SocketException ex)
        {
            exceptionThrown = true;
            SocketTestHelper::verifyExceptionString(ex.what(), "Invalid sockaddr_in address", __LINE__);
        }

        CPPUNIT_ASSERT_MESSAGE("An exception was expected to be thrown and it was not.", exceptionThrown);


        // CASE: Verify that an exception is thrown if the socket doesn't bind properly.
        exceptionThrown = false;
        try
        {
            socket.bindSocket(SocketTestHelper::getDefaultAddress());
        }
        catch(communication::SocketException ex)
        {
            exceptionThrown = true;
            SocketTestHelper::verifyExceptionString(ex.what(), "Unable to bind socket", __LINE__);
        }

        CPPUNIT_ASSERT_MESSAGE("An exception was expected to be thrown and it was not.", exceptionThrown);


        // CASE: Verify that no exceptions are thrown when a socket is bound successfully.
        socket.defineSocket(AF_INET, SOCK_STREAM, 0);
        CPPUNIT_ASSERT(communication::AbstractSocket::UNDEFINED_SOCKET_DESCRIPTOR != socket.getSocketDescriptor());

        exceptionThrown = false;
        try
        {
            socket.bindSocket(SocketTestHelper::getDefaultAddress());
        }
        catch(const communication::SocketException& ex)
        {
            exceptionThrown = true;
        }

        CPPUNIT_ASSERT_MESSAGE("An exception was expected to not be thrown and it was.", !exceptionThrown);
    }

    // ---------------------------------------------------------------------------
    void testSendData()
    {
        communication::AbstractSocket socket;

        socket.defineSocket(AF_INET, SOCK_STREAM, 0);
        CPPUNIT_ASSERT(communication::AbstractSocket::UNDEFINED_SOCKET_DESCRIPTOR != socket.getSocketDescriptor());

        const int validSocketDescriptor = socket.getSocketDescriptor();
        const int validBufferSize = 256;
        char validBuffer[validBufferSize] = "Hello world!";

        // CASE: Verify that an exception is thrown when one of the parameters is not valid.
        CPPUNIT_ASSERT_THROW(socket.sendData(communication::AbstractSocket::UNDEFINED_SOCKET_DESCRIPTOR, nullptr, 0),
                             communication::SocketException);


        // CASE: Verify that when there is an error sending that an exception is thrown.
        bool exceptionThrown = false;
        try
        {
            socket.sendData(validSocketDescriptor + 1, validBuffer, validBufferSize);
        }
        catch(communication::SocketException ex)
        {
            exceptionThrown = true;
            SocketTestHelper::verifyExceptionString(ex.what(), "Error when sending data", __LINE__);
        }

        CPPUNIT_ASSERT_MESSAGE("An exception was expected to be thrown and it was not.", exceptionThrown);

        // TODO: - finish with a successful case. I need to connect a server to a client in order to do this.
    }

    // ---------------------------------------------------------------------------
    void testValidateTransmissionData()
    {
        communication::AbstractSocket socket;

        socket.defineSocket(AF_INET, SOCK_STREAM, 0);
        CPPUNIT_ASSERT(communication::AbstractSocket::UNDEFINED_SOCKET_DESCRIPTOR != socket.getSocketDescriptor());

        const int validSocketDescriptor = socket.getSocketDescriptor();
        const int validBufferSize = 256;
        char validBuffer[validBufferSize] = {0};

        // CASE: Verify that true is returned when all valid parameters are input.
        CPPUNIT_ASSERT(socket.validateTransmissionData(validSocketDescriptor,validBuffer, validBufferSize));

        // CASE: Verify false is returned and exception thrown when the socket descriptor is invalid.
        verifyInvalidTransmissionData(communication::AbstractSocket::UNDEFINED_SOCKET_DESCRIPTOR, validBuffer, validBufferSize, __LINE__);

        // CASE: Verify false is returned and exception thrown when the buffer is invalid.
        verifyInvalidTransmissionData(validSocketDescriptor, nullptr, validBufferSize, __LINE__);

        // CASE: Verify false is returned and exception thrown when the buffer size is less than 1.
        verifyInvalidTransmissionData(validSocketDescriptor, validBuffer, 0, __LINE__);
    }

    // ---------------------------------------------------------------------------
    void testCloseSocket()
    {
        communication::AbstractSocket socket;

        // CASE: Verify that close will not be called if the socket descriptor is not defined.
        CPPUNIT_ASSERT_NO_THROW(socket.closeSocket());


        // CASE: Verify that an exception is thrown if the socket cannot be closed and the
        // descriptor is set back to be undefined.

        // Change the socket descriptor to not be undefined so that close() is called.
        socket.mSocketDescriptor = -2;
        CPPUNIT_ASSERT_THROW(socket.closeSocket(), communication::SocketException);

        // Reset the descriptor back to undefined so that the destructor doesn't throw an exception.
        socket.mSocketDescriptor = communication::AbstractSocket::UNDEFINED_SOCKET_DESCRIPTOR;


        // CASE: Verify that an open socket is closed successfully.

        // define a socket.
        socket.defineSocket(AF_FILE, SOCK_STREAM, 0);
        CPPUNIT_ASSERT(communication::AbstractSocket::UNDEFINED_SOCKET_DESCRIPTOR != socket.getSocketDescriptor());

        // Call close.
        CPPUNIT_ASSERT_NO_THROW(socket.closeSocket());

        // verify that the descriptor was reset.
        CPPUNIT_ASSERT_EQUAL(communication::AbstractSocket::UNDEFINED_SOCKET_DESCRIPTOR, socket.getSocketDescriptor());
    }

private:

    // ---------------------------------------------------------------------------
    // Verifies that if the socketDescriptor, buffer, or bufferSize are invalid the
    // validateTransmissionData function will return false and an exception will be
    // thrown.
    void verifyInvalidTransmissionData(
        const int socketDescriptor,
        const void* buffer,
        const ssize_t bufferSize,
        const int sourceLine)
    {
        const std::string lineStr = "Called from line: " + std::to_string(sourceLine);

        communication::AbstractSocket socket;

        bool exceptionThrown = false;

        try
        {
            CPPUNIT_ASSERT_MESSAGE(lineStr, !socket.validateTransmissionData(socketDescriptor, buffer, bufferSize));
        }
        catch(communication::SocketException ex)
        {
            exceptionThrown = true;
            SocketTestHelper::verifyExceptionString(ex.what(), "Error with the validateTransmissionData parameters", sourceLine);
        }

        const std::string msg = lineStr + " An exception was expected to be thrown and it was not.";
        CPPUNIT_ASSERT_MESSAGE(msg, exceptionThrown);
    }

	// ------------------------------------------------------------------------
    CPPUNIT_TEST_SUITE(AbstractSocketTest);
    CPPUNIT_TEST(testDefineSocket);
    CPPUNIT_TEST(testValidateAddress);
    CPPUNIT_TEST(testBindSocket);
    CPPUNIT_TEST(testSendData);
    CPPUNIT_TEST(testValidateTransmissionData);
    CPPUNIT_TEST(testCloseSocket);
    CPPUNIT_TEST_SUITE_END();
};

CPPUNIT_TEST_SUITE_REGISTRATION(AbstractSocketTest);



// ------------------------------------------------------------------------
// ------------------------------------------------------------------------
int main(int argc, char* argv[])
{
	// Get the top level suite from the registry
	CppUnit::Test *suite = CppUnit::TestFactoryRegistry::getRegistry().makeTest(); 
	// Adds the test to the list of test to run
	CppUnit::TextUi::TestRunner runner;
	runner.addTest(suite);

	// Change the default outputter to a compiler error format outputter
	runner.setOutputter(new CppUnit::CompilerOutputter(&runner.result(),
		std::cerr));
	// Run the tests.
	bool wasSucessful = runner.run();

	// Return error code 1 if the one of test failed.
	return wasSucessful ? 0 : 1;
}



