// This file tests the functionality of the TcpSocketServer class.

#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/ui/text/TestRunner.h>

#include <thread>

#include "SocketException.hpp"
#include "TcpSocketClient.hpp"
#include "SocketTestHelper.hpp"

// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
class TcpSocketClientTest: public CppUnit::TestFixture
{
public:

	// ---------------------------------------------------------------------------
	void setUp()
    {
    }

	// ---------------------------------------------------------------------------
	void tearDown()
	{
    }

    // ---------------------------------------------------------------------------
    /*
    void testConstructor()
    {
        // CASE: Verify that an exception is thrown when an invalid server address
        // is passed in.
        bool exceptionThrown = false;
        try
        {
            mClient = std::make_unique<communication::TcpSocketClient>(SocketTestHelper::getInvalidAddress());
        }
        catch(const communication::SocketException& ex)
        {
            exceptionThrown = true;
            SocketTestHelper::verifyExceptionString(ex.what(), "Invalid sockaddr_in address", __LINE__);
        }

        CPPUNIT_ASSERT_MESSAGE("An exception was expected to be thrown and it was not.", exceptionThrown);

        mClient.reset();

        // NOTE: There is no way to test the defineSocket exception since the only way to generate an execption
        // for it would be to pass in an invalid address and that is guarded by the validateAddress call.

        // CASE: Verify that an exception is thrown when the client can't connect to the server.
        exceptionThrown = false;
        try
        {
            mClient = std::make_unique<communication::TcpSocketClient>(SocketTestHelper::getDefaultAddress());
        }
        catch(const communication::SocketException& ex)
        {
            exceptionThrown = true;
            SocketTestHelper::verifyExceptionString(ex.what(), "Unable to connect to the server from the client", __LINE__);
        }

        CPPUNIT_ASSERT_MESSAGE("An exception was expected to be thrown and it was not.", exceptionThrown);

        mClient.reset();


        // CASE: Verify that when a server is actively accepting connections a client can connect
        // without throwing an exception.

        // Launch a server implementation in a separate thread.
        communication::TcpSocketServer server(SocketTestHelper::getDefaultAddress());
        TcpSocketServerImpl serverImpl(server);
        std::thread serverThread(&SocketTestHelper::serverReactor, std::ref(serverImpl));

        // Sleep for while to ensure that the server is fully setup before the clients
        // begin connecting.
        std::this_thread::sleep_for(std::chrono::milliseconds(15));

        try
        {
            // Connect several clients in order to test range of usablity.
            for(int i = 0; i < 10; ++i)
            {
                communication::TcpSocketClient client(SocketTestHelper::getDefaultAddress());
            }
        }
        catch (const communication::SocketException& ex)
        {
            CPPUNIT_FAIL("Failed to connect client to server - The following exception was thrown: " + std::string(ex.what()));
        }

        serverThread.join();
    }
    */

private:

    // The TCP Socket client class being tested.
    std::unique_ptr<communication::TcpSocketClient> mClient;

	// ------------------------------------------------------------------------
    CPPUNIT_TEST_SUITE(TcpSocketClientTest);
    //CPPUNIT_TEST(testConstructor);
    CPPUNIT_TEST_SUITE_END();
};

CPPUNIT_TEST_SUITE_REGISTRATION(TcpSocketClientTest);


// ------------------------------------------------------------------------
// ------------------------------------------------------------------------
int main(int argc, char* argv[])
{
	// Get the top level suite from the registry
	CppUnit::Test *suite = CppUnit::TestFactoryRegistry::getRegistry().makeTest(); 
	// Adds the test to the list of test to run
	CppUnit::TextUi::TestRunner runner;
	runner.addTest(suite);

	// Change the default outputter to a compiler error format outputter
	runner.setOutputter(new CppUnit::CompilerOutputter(&runner.result(),
		std::cerr));
	// Run the tests.
	bool wasSucessful = runner.run();

	// Return error code 1 if the one of test failed.
	return wasSucessful ? 0 : 1;
}
