// This file tests the functionality of the TcpSocketServer class.

#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/ui/text/TestRunner.h>

#include <thread>

#include "SocketException.hpp"
#include "TcpServerTestThread.hpp"
#include "TcpSocketClient.hpp"
#include "TcpSocketServer.hpp"
#include "SocketTestHelper.hpp"

#include <string.h>


// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
class TcpSocketServerTest: public CppUnit::TestFixture
{
public:

	// ---------------------------------------------------------------------------
	void setUp()
    {
        mServer = std::make_unique<communication::TcpSocketServer>(SocketTestHelper::getDefaultAddress());
    }

	// ---------------------------------------------------------------------------
	void tearDown()
	{
        mServer.reset();
    }

    // ---------------------------------------------------------------------------
    void testConstructor()
    {
        // CASE: Test that the passed-in data is not valid.
        bool exceptionThrown = false;
        try
        {
            communication::TcpSocketServer server(SocketTestHelper::getInvalidAddress());
        }
        catch(const communication::SocketException& ex)
        {
            exceptionThrown = true;
            SocketTestHelper::verifyExceptionString(ex.what(), "Invalid sockaddr_in address", __LINE__);
        }

        CPPUNIT_ASSERT_MESSAGE("An exception was expected to be thrown and it was not.", exceptionThrown);


        // CASE: Test that the passed-in data is valid.
        const sockaddr_in expectedAddress = SocketTestHelper::getDefaultAddress();
        communication::TcpSocketServer server(expectedAddress);

        // Verify the passed-in address is equal to mServerAddress.
        CPPUNIT_ASSERT_EQUAL(expectedAddress.sin_addr.s_addr, server.mServerAddress.sin_addr.s_addr);
        CPPUNIT_ASSERT_EQUAL(expectedAddress.sin_family, server.mServerAddress.sin_family);
        CPPUNIT_ASSERT_EQUAL(expectedAddress.sin_port, server.mServerAddress.sin_port);

        // Verify the Socket descriptor is set.
        CPPUNIT_ASSERT(server.getSocketDescriptor() != communication::TcpSocketServer::UNDEFINED_SOCKET_DESCRIPTOR);

        // Verify that the server's file descriptor is set in the file descriptor set.
        CPPUNIT_ASSERT(FD_ISSET(server.getSocketDescriptor(), &server.mManagedDesriptors));
    }

    // ---------------------------------------------------------------------------
    void testSetSocketOptions()
    {
        // CASE: Verify that no exception is thrown when a valid socket descriptor is set.
        CPPUNIT_ASSERT_NO_THROW(mServer->setSocketOptions());

        // CASE: Verify that an exception is thrown when an invalid socket descriptor is set.
        bool exceptionThrown = false;
        try
        {
            mServer->mSocketDescriptor = communication::TcpSocketServer::UNDEFINED_SOCKET_DESCRIPTOR;

            mServer->setSocketOptions();
        }
        catch(const communication::SocketException& ex)
        {
            exceptionThrown = true;
            SocketTestHelper::verifyExceptionString(ex.what(), "Unable to set socket options", __LINE__);
        }

        CPPUNIT_ASSERT_MESSAGE("An exception was expected to be thrown and it was not.", exceptionThrown);
    }

   // ---------------------------------------------------------------------------
   void testListenForConnections()
   {
       // CASE: Verify that if the listen call is successful then no exceptions are thrown
       // and the mListening flag is set.

       CPPUNIT_ASSERT(!mServer->mListening);
       CPPUNIT_ASSERT_NO_THROW(mServer->listenForConnections());
       CPPUNIT_ASSERT(mServer->mListening);
       mServer.reset();


       // CASE: Verify that if the socket is already listening that listen isn't called
       // again and the flag remains set to listen.
       mServer = std::make_unique<communication::TcpSocketServer>(SocketTestHelper::getDefaultAddress());
       mServer->mListening = true;

       // Set the descriptor to something invalid so that if listen were to be called it would
       // throw an exception.
       mServer->mSocketDescriptor = communication::TcpSocketServer::UNDEFINED_SOCKET_DESCRIPTOR;

       // Verify no exception is thrown since the code that would throw an exception isn't executed.
       CPPUNIT_ASSERT_NO_THROW(mServer->listenForConnections());

       // Verify that the server is still listening.
       CPPUNIT_ASSERT(mServer->mListening);
       mServer.reset();


       // CASE: Verify that if an exception is thrown that the listening flag is still set to false.
       mServer = std::make_unique<communication::TcpSocketServer>(SocketTestHelper::getDefaultAddress());

       // Set the descriptor to something invalid so that when listen is be called an
       // exception is thrown.
       mServer->mSocketDescriptor = communication::TcpSocketServer::UNDEFINED_SOCKET_DESCRIPTOR;
       CPPUNIT_ASSERT(!mServer->mListening);
       CPPUNIT_ASSERT_THROW(mServer->listenForConnections(), communication::SocketException);
       CPPUNIT_ASSERT(!mServer->mListening);
   }


    // ---------------------------------------------------------------------------
    void testListenForAndAcceptConnections()
    {
        // CASE: Verify that an exception is thrown when a time duration less than 1 is passed in.
        verifyListeningStatus(mServer->getSocketDescriptor(), std::vector<int>(), false, __LINE__);

        bool exceptionThrown = false;
        try
        {
            mServer->listenForAndAcceptConnections(std::chrono::seconds(0));
        }
        catch(const communication::SocketException& ex)
        {
            exceptionThrown = true;
            SocketTestHelper::verifyExceptionString(ex.what(), "Time duration is less than 1", __LINE__);
        }

        CPPUNIT_ASSERT_MESSAGE("An exception was expected to be thrown and it was not.", exceptionThrown);

        verifyListeningStatus(mServer->getSocketDescriptor(), std::vector<int>(), false, __LINE__);

        mServer.reset();


        // CASE: Verify that an exception is thrown when listening for the socket when
        // the descriptor is not valid.
        mServer = std::make_unique<communication::TcpSocketServer>(SocketTestHelper::getDefaultAddress());
        verifyListeningStatus(mServer->getSocketDescriptor(), std::vector<int>(), false, __LINE__);

        const int originalDescriptor = mServer->getSocketDescriptor();
        mServer->mSocketDescriptor = communication::TcpSocketServer::UNDEFINED_SOCKET_DESCRIPTOR;

        exceptionThrown = false;
        try
        {
            mServer->listenForAndAcceptConnections(std::chrono::seconds(2));
        }
        catch(communication::SocketException ex)
        {
            exceptionThrown = true;
            SocketTestHelper::verifyExceptionString(ex.what(), "Error listening on socket", __LINE__);
        }

        CPPUNIT_ASSERT_MESSAGE("An exception was expected to be thrown and it was not.", exceptionThrown);

        verifyListeningStatus(originalDescriptor, std::vector<int>(), false, __LINE__);

        mServer.reset();


        // CASE: Verify that the function times out after the passed-in time.
        mServer = std::make_unique<communication::TcpSocketServer>(SocketTestHelper::getDefaultAddress());
        verifyListeningStatus(mServer->getSocketDescriptor(), std::vector<int>(), false, __LINE__);

        const std::chrono::seconds duration(3);

        // Start a timer before the function is called.
        const std::chrono::high_resolution_clock::time_point before = std::chrono::high_resolution_clock::now();

        mServer->listenForAndAcceptConnections(std::chrono::seconds(duration));

        // Stop timing.
        const std::chrono::high_resolution_clock::time_point after = std::chrono::high_resolution_clock::now();

        // Calculate the time span that the function took.
        const std::chrono::duration<double> timeSpan =
                std::chrono::duration_cast<std::chrono::duration<double>>(after - before);

        // Convert the duration to milliseconds then add a tolerance to the duration to compare to the time-span that
        // it took for the function to complete. The tolerance is added to take into account the extra processing time
        // past the function.
        std::chrono::milliseconds durationMillisecs = std::chrono::duration_cast<std::chrono::milliseconds>(duration);
        durationMillisecs += std::chrono::milliseconds(25);

        // Convert the duration with tolerance to a duration object.
        const std::chrono::duration<double> durationWithTolerance = durationMillisecs;

        // Verify that the time-span that it took for the function to execute is less than the
        // duration with the tolerance.
        CPPUNIT_ASSERT(timeSpan < durationWithTolerance);

        // Verify that only the listening member was updated.
        verifyListeningStatus(mServer->getSocketDescriptor(), std::vector<int>(), true, __LINE__);
        mServer.reset();

        // CASE: Verify that the server can successfully connect to multiple clients.
        TcpServerTestThread serverTask;
        CPPUNIT_ASSERT(serverTask.isConnecting());
        CPPUNIT_ASSERT(serverTask.getConnectedClients().empty());

        // Start the thread with the server running in it.
        std::thread serverThread([&]() { serverTask.run(); });

        // Sleep for a period before connecting so that the thread has a chance to fully complete its
        // calls before invoking the clients.
        std::this_thread::sleep_for(std::chrono::milliseconds(15));

        // Invoke the clients.
        std::vector<std::unique_ptr<communication::TcpSocketClient>> clients;

        const size_t numberOfClients = 10;
        for(size_t i = 0; i < numberOfClients; ++i)
        {
            try
            {
                clients.push_back(std::make_unique<communication::TcpSocketClient>(SocketTestHelper::getDefaultAddress()));
            }
            catch (const communication::SocketException& ex)
            {
                CPPUNIT_FAIL("Failed to create client - The following exception was thrown: " + std::string(ex.what()));
            }
        }

        // Wait for the server to finish connecting with the clients.
        while(serverTask.isConnecting())
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }

        // Retrieve the information of the server before calling join. If an assertion fails then
        // a core dump will happen instead of the assertion failure message.
        const bool isConnecting = serverTask.isConnecting();
        const size_t numberOfConnectedClients = serverTask.getConnectedClients().size();

        // Stop the thread.
        serverTask.stop();
        serverThread.join();

        // Verify the data.
        CPPUNIT_ASSERT(!isConnecting);
        CPPUNIT_ASSERT_EQUAL(numberOfClients, numberOfConnectedClients);
    }

    // ---------------------------------------------------------------------------
    void testGetClientDescriptors()
    {
        // CASE: Test that an empty container is returned when no clients are connected.
        CPPUNIT_ASSERT(mServer->getClientDescriptors().empty());


        // CASE: Test that if there are connected clients that the function returns those
        // clients in the container.

        communication::TcpSocketServer::DescriptorContainer_t expectedClients = { 1, 2, 3 };

        // Set the mClientSocketDescriptors vector in the class to simulate connecting to clients.
        mServer->mClientSocketDescriptors = expectedClients;

        // Verify that the function returns the same vector back.
        CPPUNIT_ASSERT(expectedClients == mServer->getClientDescriptors());
    }

    // ------------------------------------------------------------------------
    void testSendData()
    {
        TcpServerTestThread serverTask;

        // Start the thread with the server running in it.
        std::thread serverThread([&]() { serverTask.run(); });

        // Sleep for a period before connecting so that the thread has a chance to fully complete its
        // calls before invoking the clients.
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));

        // Invoke the clients.
        std::vector<std::unique_ptr<communication::TcpSocketClient>> clients;

        const size_t numberOfClients = 1;
        for(size_t i = 0; i < numberOfClients; ++i)
        {
            try
            {
                clients.push_back(std::make_unique<communication::TcpSocketClient>(SocketTestHelper::getDefaultAddress()));
            }
            catch (const communication::SocketException& ex)
            {
                CPPUNIT_FAIL("Failed to create client - The following exception was thrown: " + std::string(ex.what()));
            }
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));

        std::this_thread::sleep_for(std::chrono::seconds(3));

        // Retrieve the information of the server before calling join. If an assertion fails then
        // a core dump will happen instead of the assertion failure message.
        const bool isConnecting = serverTask.isConnecting();

        const int serverDescriptor = serverTask.getServerDescriptor();

        std::cerr << "SERVER descriptor outside of thread: " << serverDescriptor << std::endl;

        serverTask.setExpectedClientMsg(clients.at(0)->getSocketDescriptor(), 3);

        // 3/7/2019 -  was never able to get this test to work when trying to
        // communicate between a client in the main thread and server in other thread working properly.
        // It would always throw a signal pipe error when I would wrote data to the server from the client.


        std::this_thread::sleep_for(std::chrono::seconds(10));

        clients.at(0)->sendData(serverDescriptor, "hi!", 3);


        serverTask.stop();
        serverThread.join();

        CPPUNIT_ASSERT(!serverTask.data.empty());
        std::cout << serverTask.data[0];
    }

    // ------------------------------------------------------------------------
    void testCloseClientSocket()
    {
        int clientDescriptor = 10;

        const std::string commonException = "Unable to close client descriptor ";

        // CASE: verify that an exception is thrown when the passed-in descriptor doesn't exist
        // as one of the stored clients.

        bool exceptionThrown = false;
        try
        {
            mServer->closeClientSocket(clientDescriptor);
        }
        catch(const communication::SocketException& ex)
        {
            std::string exception = commonException + std::to_string(clientDescriptor);
            exception += " because it does not exist.";

            exceptionThrown = true;
            SocketTestHelper::verifyExceptionString(ex.what(), exception, __LINE__);
        }

        CPPUNIT_ASSERT_MESSAGE("An exception was expected to be thrown and it was not.", exceptionThrown);


        // CASE: Verify that an exception is thrown if an existing client descriptor can't be closed.

        // Add an invalid socket to the container of client descriptors.
        clientDescriptor = communication::TcpSocketServer::UNDEFINED_SOCKET_DESCRIPTOR;

        mServer->mClientSocketDescriptors = { clientDescriptor };

        exceptionThrown = false;
        try
        {
            mServer->closeClientSocket(clientDescriptor);
        }
        catch(const communication::SocketException& ex)
        {
            const std::string exception = commonException + std::to_string(clientDescriptor);

            exceptionThrown = true;
            SocketTestHelper::verifyExceptionString(ex.what(), exception, __LINE__);
        }

        CPPUNIT_ASSERT_MESSAGE("An exception was expected to be thrown and it was not.", exceptionThrown);
        mServer.reset();


        // CASE: Successfully close open client descriptors.
        TcpServerTestThread serverTask;
        CPPUNIT_ASSERT(serverTask.isConnecting());
        CPPUNIT_ASSERT(serverTask.getConnectedClients().empty());

        // Start the thread with the server running in it.
        std::thread serverThread([&]() { serverTask.run(); });

        // Sleep for a period before connecting so that the thread has a chance to fully complete its
        // calls before invoking the clients.
        std::this_thread::sleep_for(std::chrono::milliseconds(15));

        // Invoke the clients.
        std::vector<std::unique_ptr<communication::TcpSocketClient>> clients;

        const size_t numberOfClients = 2;
        for(size_t i = 0; i < numberOfClients; ++i)
        {
            try
            {
                clients.push_back(std::make_unique<communication::TcpSocketClient>(SocketTestHelper::getDefaultAddress()));
            }
            catch (const communication::SocketException& ex)
            {
                CPPUNIT_FAIL("Failed to create client - The following exception was thrown: " + std::string(ex.what()));
            }
        }

        // Wait for the server to finish connecting with the clients.
        while(serverTask.isConnecting())
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }

        // Retrieve the information of the server before calling join. If an assertion fails then
        // a core dump will happen instead of the assertion failure message.
        const bool isConnecting = serverTask.isConnecting();

        // Get the connected clients.
        const communication::TcpSocketServer::DescriptorContainer_t totalClients = serverTask.getConnectedClients();

        // Close the first client.
        const int descriptorOne = *std::cbegin(totalClients);
        serverTask.closeClient(descriptorOne);

        // Get the clients again.
        const communication::TcpSocketServer::DescriptorContainer_t clientsMinusOne = serverTask.getConnectedClients();

        serverTask.stop();
        serverThread.join();

        CPPUNIT_ASSERT(!isConnecting);

        // Verify that the correct number of clients were connected originally.
        CPPUNIT_ASSERT_EQUAL(numberOfClients, totalClients.size());

        // Verify that the client's descriptor that was closed isn't in the now clients container.
        CPPUNIT_ASSERT_EQUAL(size_t(1), clientsMinusOne.size());
        CPPUNIT_ASSERT_EQUAL(*(std::cend(totalClients) - 1), *std::cbegin(clientsMinusOne));
    }

    // ------------------------------------------------------------------------
    void testCloseAllClients()
    {
        // CASE: Call when no clients are open.
        CPPUNIT_ASSERT_NO_THROW(mServer->closeAllClients());

        // CASE: Successfully close open client descriptors.
        TcpServerTestThread serverTask;
        CPPUNIT_ASSERT(serverTask.isConnecting());
        CPPUNIT_ASSERT(serverTask.getConnectedClients().empty());

        // Start the thread with the server running in it.
        std::thread serverThread([&]() { serverTask.run(); });

        // Sleep for a period before connecting so that the thread has a chance to fully complete its
        // calls before invoking the clients.
        std::this_thread::sleep_for(std::chrono::milliseconds(15));

        // Invoke the clients.
        std::vector<std::unique_ptr<communication::TcpSocketClient>> clients;

        const size_t numberOfClients = 5;
        for(size_t i = 0; i < numberOfClients; ++i)
        {
            try
            {
                clients.push_back(std::make_unique<communication::TcpSocketClient>(SocketTestHelper::getDefaultAddress()));
            }
            catch (const communication::SocketException& ex)
            {
                CPPUNIT_FAIL("Failed to create client - The following exception was thrown: " + std::string(ex.what()));
            }
        }

        // Wait for the server to finish connecting with the clients.
        while(serverTask.isConnecting())
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }

        // Retrieve the information of the server before calling join. If an assertion fails then
        // a core dump will happen instead of the assertion failure message.
        const bool isConnecting = serverTask.isConnecting();
        const size_t openClients = serverTask.getConnectedClients().size();

        // Close all the clients
        serverTask.closeAllClients();

        const size_t numberOfOpenClients = serverTask.getConnectedClients().size();

        serverTask.stop();
        serverThread.join();

        CPPUNIT_ASSERT(!isConnecting);

        // Verify that the correct number of clients were connected originally.
        CPPUNIT_ASSERT_EQUAL(numberOfClients, openClients);

        // Verify that no sockets are opened after they are closed.
        CPPUNIT_ASSERT_EQUAL(size_t(0), numberOfOpenClients);
    }

private:

    // ---------------------------------------------------------------------------
    // Verifies the state of the class members after listenForAndAcceptConnections()
    // is called.
    void verifyListeningStatus(const int expectedMaxSocketDescriptor,
                               const std::vector<int>& expectedClientDescriptors,
                               const bool listening,
                               const int line)
    {

        const std::string lineStr = "Called from line: " + std::to_string(line);

        CPPUNIT_ASSERT_MESSAGE(lineStr, expectedClientDescriptors == mServer->getClientDescriptors());

        CPPUNIT_ASSERT_MESSAGE(lineStr, listening == mServer->mListening);
    }

    // The TCP Socket server class being tested.
    std::unique_ptr<communication::TcpSocketServer> mServer;

	// ------------------------------------------------------------------------
    CPPUNIT_TEST_SUITE(TcpSocketServerTest);
    CPPUNIT_TEST(testConstructor);
    CPPUNIT_TEST(testSetSocketOptions);
    CPPUNIT_TEST(testListenForConnections);
    CPPUNIT_TEST(testListenForAndAcceptConnections);
    CPPUNIT_TEST(testGetClientDescriptors);
    CPPUNIT_TEST(testSendData);
    CPPUNIT_TEST(testCloseClientSocket);
    CPPUNIT_TEST(testCloseAllClients);
    CPPUNIT_TEST_SUITE_END();
};

CPPUNIT_TEST_SUITE_REGISTRATION(TcpSocketServerTest);


// ------------------------------------------------------------------------
// ------------------------------------------------------------------------
int main(int argc, char* argv[])
{
	// Get the top level suite from the registry
	CppUnit::Test *suite = CppUnit::TestFactoryRegistry::getRegistry().makeTest(); 
	// Adds the test to the list of test to run
	CppUnit::TextUi::TestRunner runner;
	runner.addTest(suite);

	// Change the default outputter to a compiler error format outputter
	runner.setOutputter(new CppUnit::CompilerOutputter(&runner.result(),
		std::cerr));
	// Run the tests.
	bool wasSucessful = runner.run();

	// Return error code 1 if the one of test failed.
	return wasSucessful ? 0 : 1;
}
