
#include "Random.hpp"

#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/ui/text/TestRunner.h>

using namespace CppUnit;

class RandomTest : public TestFixture
{
public:

	// ------------------------------------------------------------------------
    void setUp()
    {
        mLowerLimit = -1382236160;
        mUpperLimit = 1382236160;
    }

	// ------------------------------------------------------------------------
    void testGetRangedRandomInt()
    {
		// CASE: Test that the output random number is within the range allotted.
        CPPUNIT_ASSERT(mLowerLimit <= Random::getRangedRandomInt(mLowerLimit, mUpperLimit));
        CPPUNIT_ASSERT(mUpperLimit >= Random::getRangedRandomInt(mLowerLimit, mUpperLimit));
    }

	// ------------------------------------------------------------------------
    void testGetRandomIntContainer()
    {
        const size_t expectedSize(200000);

        const std::vector<int> container = 
            std::move(Random::getRandomIntContainer(expectedSize, mLowerLimit, mUpperLimit)); 

		// CASE: Test that the container is the correct size;
        CPPUNIT_ASSERT_EQUAL(expectedSize, container.size());

		// CASE: Test that the output random number elements are within the range allotted.
		for(const int element : container)
        {
            CPPUNIT_ASSERT(mLowerLimit <= element);
            CPPUNIT_ASSERT(mUpperLimit >= element);
        }
    }

private:
    
    // The lower limit for the random functions.
    int mLowerLimit;

    // The upper limit for the random functions.
    int mUpperLimit;

	// ------------------------------------------------------------------------
	CPPUNIT_TEST_SUITE(RandomTest);
	CPPUNIT_TEST(testGetRangedRandomInt);
	CPPUNIT_TEST(testGetRandomIntContainer);
	CPPUNIT_TEST_SUITE_END();
};

CPPUNIT_TEST_SUITE_REGISTRATION(RandomTest);


int main(int argc, char* argv[])
{
	// Get the top level suite from the registry
	CppUnit::Test *suite = CppUnit::TestFactoryRegistry::getRegistry().makeTest();

	// Adds the test to the list of test to run
	CppUnit::TextUi::TestRunner runner;
	runner.addTest(suite);

	// Change the default outputter to a compiler error format outputter
	runner.setOutputter(new CppUnit::CompilerOutputter(&runner.result(),
		std::cerr));
	// Run the tests.
	bool wasSucessful = runner.run();

	// Return error code 1 if the one of test failed.
	return wasSucessful ? 0 : 1;
}

