
#include "Random.hpp"
#include "Matrix.hpp"

#include <algorithm>
#include <iostream>
#include <random>

#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/ui/text/TestRunner.h>

using namespace CppUnit;

class MatrixTest : public TestFixture
{
public:

	// ---------------------------------------------------------------------------
	void testGetHeight()
	{
		// Create a valid random height value.
		const int height(Random::getRangedRandomInt(1, 1200));
		const int width(1201);

		// Create a matrix object with the random height.
		Matrix<int> matrix(height, width);

		// Verify the matrix height is correct.
		CPPUNIT_ASSERT_EQUAL(height, matrix.getHeight());
		CPPUNIT_ASSERT(matrix.getErrors().empty());

		// Append a row to the matrix.
		const std::vector<int> row(width);
		matrix.appendRow(row);

		// Verify the matrix height is correct.
		CPPUNIT_ASSERT_EQUAL(height + 1, matrix.getHeight());
		CPPUNIT_ASSERT(matrix.getErrors().empty());

	}

	// ---------------------------------------------------------------------------
	void testGetWidth()
	{
		// Create a valid random width value.
		const int height(200);
		const int width(Random::getRangedRandomInt(2, 1200));

		// Create a matrix object with the random width.
		const Matrix<int> matrix(height, width);

		// Verify the matrix width is correct.
		CPPUNIT_ASSERT_EQUAL(width, matrix.getWidth());
		CPPUNIT_ASSERT(matrix.getErrors().empty());

		// Create a 1x1 matrix and test check that the width
		const Matrix<double> emptyMatrix;
		CPPUNIT_ASSERT_EQUAL(1, emptyMatrix.getWidth());
		CPPUNIT_ASSERT(matrix.getErrors().empty());
	}

	// ---------------------------------------------------------------------------
	void testAppendRow()
	{
		// CASE: test adding a row to a matrix and verify that 
		// the contents are correct.
		Matrix<std::string> matrix;
		CPPUNIT_ASSERT(matrix.mMatrix[0][0] == std::string());

		const std::string rowElement("Element: [1][0]");
		std::vector<std::string> row { rowElement };
		CPPUNIT_ASSERT(matrix.appendRow(row));
		CPPUNIT_ASSERT(matrix.getErrors().empty());
		CPPUNIT_ASSERT_EQUAL(rowElement, matrix.mMatrix[1][0]);

		// CASE: test adding a row of a different width will return false and 
		// produce an error.
		row.push_back("new element");
		CPPUNIT_ASSERT(!matrix.appendRow(row));
		CPPUNIT_ASSERT_EQUAL(size_t(1), matrix.getErrors().size());
	}

	// ---------------------------------------------------------------------------
	void testModifyRow()
	{
		const int width = 5;
		Matrix<int> matrix(2, width);

		const std::vector<int> validRow { 1, 2, 3, 4, 5 };
		const int modifiedRowIndex = 1;

		// CASE: test that when a valid row is modified that true is returned and
		// there are no errors.

		CPPUNIT_ASSERT(matrix.modifyRow(modifiedRowIndex, validRow));
		CPPUNIT_ASSERT(validRow == matrix[modifiedRowIndex]);
		CPPUNIT_ASSERT(matrix.getErrors().empty());


		// Create a new matrix to test the failure cases and save the row
		// intended to be modified as a backup for verification.
		Matrix<int> errorMatrix(2, width);
		const std::vector<int> originalRow = errorMatrix[modifiedRowIndex];

		// CASE: test that when an invalid index is input that false is returned and
		// an error is produced.
		CPPUNIT_ASSERT(!errorMatrix.modifyRow(-1, validRow));
		CPPUNIT_ASSERT_EQUAL(size_t(1), errorMatrix.getErrors().size());
		CPPUNIT_ASSERT(originalRow == errorMatrix[modifiedRowIndex]);

		// CASE: test that when a row of invalid size is input that false is returned and
		// an error is produced.
		errorMatrix.clearErrors();
		const std::vector<int> invalidRow(width + 1);

		CPPUNIT_ASSERT(!errorMatrix.modifyRow(modifiedRowIndex, invalidRow));
		CPPUNIT_ASSERT_EQUAL(size_t(1), errorMatrix.getErrors().size());
		CPPUNIT_ASSERT(originalRow == errorMatrix[modifiedRowIndex]);
	}

	// ---------------------------------------------------------------------------
	void testAddModifyRow()
	{
		const int width = 5;
		Matrix<int> matrix(2, width);

		const std::vector<int> validRow { 1, 2, 3, 4, 5 };
		const int modifiedRowIndex = 1;

		// CASE: test that when a valid row is modified that true is returned and
		// there are no errors.
		CPPUNIT_ASSERT(matrix.addModifyRow(modifiedRowIndex, validRow));
		CPPUNIT_ASSERT(validRow == matrix[modifiedRowIndex]);
		CPPUNIT_ASSERT(matrix.getErrors().empty());

		// CASE: test that when the index is the max valid index + 1 that the function
		// returns true and a new row is added.
		const int height = matrix.getHeight();
		CPPUNIT_ASSERT(matrix.addModifyRow(height, validRow));
		CPPUNIT_ASSERT(validRow == matrix[height]);
		CPPUNIT_ASSERT(matrix.getErrors().empty());

		// Create a new matrix to test the failure cases and save the row
		// intended to be modified as a backup for verification.
		Matrix<int> errorMatrix(2, width);
		const std::vector<int> originalRow = errorMatrix[modifiedRowIndex];

		// CASE: test that when an invalid index is input that false is returned and
		// an error is produced.
		CPPUNIT_ASSERT(!errorMatrix.addModifyRow(-1, validRow));
		CPPUNIT_ASSERT_EQUAL(size_t(1), errorMatrix.getErrors().size());
		CPPUNIT_ASSERT(originalRow == errorMatrix[modifiedRowIndex]);

		// CASE: test that when a row of invalid size is input that false is returned and
		// an error is produced.
		errorMatrix.clearErrors();
		const std::vector<int> invalidRow(width + 1);

		CPPUNIT_ASSERT(!errorMatrix.addModifyRow(modifiedRowIndex, invalidRow));
		CPPUNIT_ASSERT_EQUAL(size_t(1), errorMatrix.getErrors().size());
		CPPUNIT_ASSERT(originalRow == errorMatrix[modifiedRowIndex]);
	}


	// ---------------------------------------------------------------------------
	void testValidateNewRow()
	{
		const int width(10);
		Matrix<std::string> matrix(5, width);

		const std::vector<std::string> validRow(width);

		// CASE: test that when a row of equal length is input that true is returned.
		CPPUNIT_ASSERT(matrix.validateNewRow(validRow));
		CPPUNIT_ASSERT(matrix.getErrors().empty());

		// CASE: test the when a row of invalid length is input that false is returned
		// and an INVALID_NEW_ROW error is added to the errors.
		const std::vector<std::string> invalidRow(width + 1);

		CPPUNIT_ASSERT(!matrix.validateNewRow(invalidRow));

		CPPUNIT_ASSERT_EQUAL(size_t(1), matrix.getErrors().size());

		const std::vector<std::string> errorParameters{ std::to_string(invalidRow.size()), std::to_string(matrix.getWidth()) };

		const std::string expectedError =
			matrix.mErrorCodes.getErrorMsg(Matrix<std::string>::INVALID_NEW_ROW, errorParameters); 

		CPPUNIT_ASSERT_EQUAL(expectedError, matrix.getErrors()[0]);
	}


	// ---------------------------------------------------------------------------
	// Tests the getErrors and clearErrors functions and any errors 
	// that are produced when the Matrix is constructed.
	void testErrors()
	{
		// CASE: test that an invalid height and width entry
		// in the constructor produces an error.
		const int invalidHeight = -8;
		const int invalidWidth = -20;

		// Construct a matrix with the invalid values.
		Matrix<int> errorMatrix(invalidHeight, invalidWidth);

		// Verify that the height and width of the matrix are set to 1 since
		// the input values are invalid.
		CPPUNIT_ASSERT_EQUAL(1, errorMatrix.getHeight());
		CPPUNIT_ASSERT_EQUAL(1, errorMatrix.getWidth());

		// Verify the height and width errors are produced.
		CPPUNIT_ASSERT_EQUAL(size_t(2), errorMatrix.getErrors().size());

		// Verify the height error.
		const std::vector<std::string> heightErrorParams { "height", std::to_string(invalidHeight) };
		const std::string expectedHeightError = 
			errorMatrix.mErrorCodes.getErrorMsg(Matrix<int>::INVALID_SIZE, heightErrorParams);

		CPPUNIT_ASSERT_EQUAL(expectedHeightError, errorMatrix.getErrors()[0]);

		// Verify the width error.
		const std::vector<std::string> widthErrorParams { "width", std::to_string(invalidWidth) };
		const std::string expectedWidthError = 
			errorMatrix.mErrorCodes.getErrorMsg(Matrix<int>::INVALID_SIZE, widthErrorParams);

		CPPUNIT_ASSERT_EQUAL(expectedWidthError, errorMatrix.getErrors()[1]);

		// CASE: test that the clearErrors function clears the errors in the matrix.
		errorMatrix.clearErrors();
		CPPUNIT_ASSERT(errorMatrix.getErrors().empty());
	}

	// ---------------------------------------------------------------------------
	void testBracketOperator()
	{
		// Create a matrix and set one of the rows to have values.
		Matrix<int> nonConstMatrix(3, 3);
		const std::vector<int> expectedRow { 1, 2, 3 };

		const int modifiedRowIndex = nonConstMatrix.getHeight() - 1;
		nonConstMatrix.mMatrix[modifiedRowIndex] = expectedRow;

		// CASE: test the non-const bracket operator for retrieving values.
		CPPUNIT_ASSERT(expectedRow == nonConstMatrix[modifiedRowIndex]);
		CPPUNIT_ASSERT(nonConstMatrix.getErrors().empty());

		// Copy the modified matrix into a constant one to ensure
		// that the const overloaded bracket operator gets called.
		const Matrix<int> constMatrix(nonConstMatrix);

		// CASE: test the const bracket operator for retrieving values.
		CPPUNIT_ASSERT(expectedRow == constMatrix[modifiedRowIndex]);
		CPPUNIT_ASSERT(constMatrix.getErrors().empty());

		// CASE: verify that the double index brackets work.
		for (int i = 0; i < expectedRow.size(); ++i)
		{
			CPPUNIT_ASSERT_EQUAL(expectedRow[i], constMatrix[modifiedRowIndex][i]);
			CPPUNIT_ASSERT_EQUAL(expectedRow[i], nonConstMatrix[modifiedRowIndex][i]);
		}

		// CASE: verify the IndexOutOfBounds exception gets thrown for the const
		// and non-const bracket operator.

		const std::vector<int> invalidIndexes { -1, 99999 };
		
		for (const int index : invalidIndexes)
		{
			const std::string debugMsg("Index: " + std::to_string(index));

			CPPUNIT_ASSERT_THROW_MESSAGE(
				debugMsg, 
				nonConstMatrix[index],
				Matrix<int>::IndexOutOfBoundsException);

			CPPUNIT_ASSERT_THROW_MESSAGE(
				debugMsg, 
				constMatrix[index],
				Matrix<int>::IndexOutOfBoundsException);
		}
	}

	// ---------------------------------------------------------------------------
	void testValidateRowIndex()
	{
		// Create a matrix
		Matrix<int> matrix(3, 7);

		// CASE: Test that a valid index returns true.
		CPPUNIT_ASSERT(matrix.validateRowIndex(0));

		// CASE: Test that all invalid indexes returns false.
		// Make a container of possible invalid indexes.
		const std::vector<int> invalidIndexes { -1, matrix.getHeight() };
		
		for (const int index : invalidIndexes)
		{
			const std::string debugMsg("Index: " + std::to_string(index));
			CPPUNIT_ASSERT_MESSAGE(debugMsg, !matrix.validateRowIndex(index));
		}
	}

	// ---------------------------------------------------------------------------
	void testValidateColumnIndex()
	{
		// Create a matrix
		Matrix<int> matrix(3, 7);

		// CASE: Test that a valid index returns true.
		CPPUNIT_ASSERT(matrix.validateColumnIndex(0));

		// CASE: Test that all invalid indexes returns false.
		// Make a container of possible invalid indexes.
		const std::vector<int> invalidIndexes { -1, matrix.getWidth() + 1 };
		
		for (const int index : invalidIndexes)
		{
			const std::string debugMsg("Index: " + std::to_string(index));
			CPPUNIT_ASSERT_MESSAGE(debugMsg, !matrix.validateColumnIndex(index));
		}
	}




	// ---------------------------------------------------------------------------
	void testGetRow()
	{
		// Create a matrix and set one of the rows to contain non-zero values.
		const std::vector<int> rowWithValues { 1, 2, 3, 4, 5 };

		Matrix<int> matrix(6, rowWithValues.size());

		const int rowWithValuesIndex(3);
		matrix.mMatrix[rowWithValuesIndex] = rowWithValues;

		// Make a struct to hold the variable test values. 
		struct TestValues
		{
			int mRowIndex;

			bool mIsError;

			std::string mTestDetails;

			// Returns a string giving details on the object.
			std::string getDebugMsg() const
			{
				std::string debugMsg = mTestDetails + "\n";
			    debugMsg += "Row Index: " + std::to_string(mRowIndex) + "\n";
				debugMsg += "Is Error case?: " + std::to_string(mIsError);

				return debugMsg;
			}
		};

		std::vector<TestValues> testValueContainer;

		// CASE: test that an error occurs if the input row index is less than 0.
		const TestValues indexLessThanOneCase { -1, true, "Index less than zero" };
		testValueContainer.push_back(std::move(indexLessThanOneCase));
		
		// CASE: test that an error occurs if the input row index is 
		// greater than the height of the matrix.
		const TestValues indexGreaterThanHeightCase { matrix.getHeight() + 1, true, "Index greater than height" };
		testValueContainer.push_back(std::move(indexGreaterThanHeightCase));

		// CASE: test a case where the input row index is valid.
		const TestValues validIndexCase { rowWithValuesIndex, false, "Valid Index" };
		testValueContainer.push_back(std::move(validIndexCase));

		for(const TestValues& testValue : testValueContainer)
		{
			const std::string debugMsg(testValue.getDebugMsg());

			// Clear current errors.
			matrix.clearErrors();

			// Create a vector with one value.
			std::vector<int> actualRow(1);

			// Call the getRow function with the test row index value.
			actualRow = matrix.getRow(testValue.mRowIndex);

			if (testValue.mIsError)
			{
				// Verify an empty row was returned by the function
				// since this is an error case.
				CPPUNIT_ASSERT_MESSAGE(debugMsg, actualRow.empty());

				const std::string indexStr(std::to_string(testValue.mRowIndex));

				// Verify the correct error was returned.
				const std::string expectedError = matrix.mErrorCodes.getErrorMsg(
					Matrix<int>::INVALID_ROW_ACCESS, indexStr);

				CPPUNIT_ASSERT_MESSAGE(debugMsg, 1 == matrix.getErrors().size());
				CPPUNIT_ASSERT_EQUAL_MESSAGE(debugMsg, expectedError, matrix.getErrors()[0]);
			}
			else
			{
				// Verify there are no errors.
				CPPUNIT_ASSERT_MESSAGE(debugMsg, matrix.getErrors().empty());

				// Verify that the expected row is returned.
				const std::vector<int> expectedRow(rowWithValues);

				CPPUNIT_ASSERT_MESSAGE(debugMsg, expectedRow == actualRow);
			}
		}
	}

	// ---------------------------------------------------------------------------
	void testCopyConstructor()
	{
		Matrix<double> matrix(10, 10);
		Matrix<double> matrixCopy(matrix);

		CPPUNIT_ASSERT(matrix.mErrors == matrixCopy.mErrors);
		CPPUNIT_ASSERT(matrix.mErrorCodes == matrixCopy.mErrorCodes);
		CPPUNIT_ASSERT(matrix.mMatrix == matrixCopy.mMatrix);
	}

	// ---------------------------------------------------------------------------
	void testAt()
	{
		const int height(2);
		const int width(3);
		const int modifiedRowIndex(1);
		Matrix<int> matrix(height, width);

		const std::vector<int> modifiedRow{ 1, 2, 3 };

		// Add non-zero elements to the matrix.
		CPPUNIT_ASSERT(matrix.modifyRow(modifiedRowIndex, modifiedRow));

		// CASE: test that the function returns a valid element that can be modified
		// when the indexes are valid.

		// Get an element and verify it is what should be in the matrix.
		int& obtainedElem = matrix.at(modifiedRowIndex, 0);
		CPPUNIT_ASSERT_EQUAL(modifiedRow[0], matrix.at(modifiedRowIndex, 0));

		// Change the element value and verify that it changed the value in the 
		// matrix since it is a reference to the matrix element.
		obtainedElem = 3000;
		CPPUNIT_ASSERT_EQUAL(3000, matrix.mMatrix[modifiedRowIndex][0]);

		// CASE: verify the IndexOutOfBounds exception gets thrown if any of
		// the indexes are incorrect.

		// Verify the case where the height is invalid.
		CPPUNIT_ASSERT_THROW(matrix.at(-1, width - 1), Matrix<int>::IndexOutOfBoundsException);

		// Verify the case where the width is invalid.
		CPPUNIT_ASSERT_THROW(matrix.at(height - 1, -1), Matrix<int>::IndexOutOfBoundsException);

		// Verify the case where both height and width are invalid.
		CPPUNIT_ASSERT_THROW(matrix.at(-1, -1), Matrix<int>::IndexOutOfBoundsException);
	}

	// ---------------------------------------------------------------------------
	void testValidateEvenMatrices()
	{
		Matrix<double> matrixOne(2, 2);
		Matrix<double> matrixTwo(2, 2);
		
		// CASE: Verify true is returned if two matrices are even .
		CPPUNIT_ASSERT(matrixOne.validateEvenMatrices(matrixTwo));

		// CASE: Verify that matrices with uneven heights will return false.

		// Add another row to one of the matrices so the height is not equal.
		matrixTwo.appendRow(std::vector<double>(2));
		
		CPPUNIT_ASSERT(!matrixOne.validateEvenMatrices(matrixTwo));

		// CASE: Verify that matrices with uneven widths will return false.
		matrixOne.clearErrors();

		const Matrix<double> matrixThree(2, 3);

		CPPUNIT_ASSERT(!matrixOne.validateEvenMatrices(matrixThree));

		// CASE: Verify that matrices with uneven widths and heights will return false.
		CPPUNIT_ASSERT(!matrixTwo.validateEvenMatrices(matrixThree));
	}

	// ---------------------------------------------------------------------------
	void testAddSubtractRow()
	{
		const Matrix<int> matrix;

		std::vector<int> a, b;
		std::vector<int> results;

		// CASE: verify that an empty row would be returned if both rows are 
		// empty. NOTE: This should never occur with a matrix since the minimum
		// size is 1x1.
		CPPUNIT_ASSERT(results == matrix.addSubtractRow(true, a, b));
		CPPUNIT_ASSERT(results == matrix.addSubtractRow(false, a, b));
		CPPUNIT_ASSERT(results.empty());


		// Insert the same amount of random values into both vectors.
		const int rowSize = Random::getRangedRandomInt(1, 1200);

		a = std::move(getRandomIntContainer(rowSize));
		b = std::move(getRandomIntContainer(rowSize));

		CPPUNIT_ASSERT(a.size() == b.size());
		CPPUNIT_ASSERT(rowSize == b.size());


		// CASE: verify the correctness of row addition.

		// Add both rows.
		results = matrix.addSubtractRow(true, a, b);

		CPPUNIT_ASSERT(results.size() == rowSize);

		// Validate the results.
		for (int i = 0; i < rowSize; ++i)
		{
			CPPUNIT_ASSERT_EQUAL(a[i] + b[i], results[i]);
		}

		// CASE: verify the correctness of row subtraction.

		// subtract both rows.
		results = matrix.addSubtractRow(false, a, b);

		CPPUNIT_ASSERT(results.size() == rowSize);

		// Validate the results.
		for (int i = 0; i < rowSize; ++i)
		{
			CPPUNIT_ASSERT_EQUAL(a[i] - b[i], results[i]);
		}
	}

	// ---------------------------------------------------------------------------
	void testAddOperator()
	{
		const int height = Random::getRangedRandomInt(1, 50);
		const int width = Random::getRangedRandomInt(1, 50);

		Matrix<int> x(height, width);
		Matrix<int> y(height, width);

		// Create two matrices filled with random values.
		x = getMatrixWithRandomValues<int>(height, width);
		y = getMatrixWithRandomValues<int>(height, width);

		// Verify that all the dimensions are equal.
		bool equalDimensions = (x.getHeight() == y.getHeight());
		equalDimensions &= (x.getWidth() == y.getWidth());
		equalDimensions &= (x.getWidth() == width) && (y.getWidth() == width);
		equalDimensions &= (x.getHeight() == height) && (y.getHeight() == height);

		CPPUNIT_ASSERT(true == equalDimensions);

		// Add the two matrices and copy the results into another matrix.
		// Verify the dimensions are correct and that there are not errors.
		const Matrix<int> sum(x + y);
		CPPUNIT_ASSERT_EQUAL(sum.getHeight(), height);
		CPPUNIT_ASSERT_EQUAL(sum.getWidth(), width);
		CPPUNIT_ASSERT(sum.getErrors().empty());

		// Iterate though each row of the results matrix and compare it to the
		// added two rows. Note that the function addSubtractRow is tested in
		// another unit test.
		for (int i = 0; i < height; ++i)
		{
			CPPUNIT_ASSERT(x.addSubtractRow(true, x.mMatrix[i], y.mMatrix[i]) == sum.mMatrix[i]);
		}

		// Verify there are not errors in any of the matrices.
		CPPUNIT_ASSERT(sum.getErrors().empty());
		CPPUNIT_ASSERT(x.getErrors().empty());
		CPPUNIT_ASSERT(y.getErrors().empty());


		// CASE: Verify that an exception will get thrown if the two matrices that are being
		// added do not have the same dimensions.

		// Add another row to one of the matrices so that they are uneven with each other.
		x.appendRow(getRandomIntContainer(width));

		CPPUNIT_ASSERT_THROW(x + y, Matrix<int>::UnevenMatricesException);
	}

	// ---------------------------------------------------------------------------
	void testSubtractOperator()
	{
		const int height = Random::getRangedRandomInt(1, 50);
		const int width = Random::getRangedRandomInt(1, 50);

		Matrix<int> x(height, width);
		Matrix<int> y(height, width);

		// Create two matrices filled with random values.
		x = getMatrixWithRandomValues<int>(height, width);
		y = getMatrixWithRandomValues<int>(height, width);

		// Verify that all the dimensions are equal.
		bool equalDimensions = (x.getHeight() == y.getHeight());
		equalDimensions &= (x.getWidth() == y.getWidth());
		equalDimensions &= (x.getWidth() == width) && (y.getWidth() == width);
		equalDimensions &= (x.getHeight() == height) && (y.getHeight() == height);

		CPPUNIT_ASSERT(true == equalDimensions);

		// Subtract the two matrices and copy the results into another matrix.
		// Verify the dimensions are correct and that there are not errors.
		const Matrix<int> difference(x - y);
		CPPUNIT_ASSERT_EQUAL(difference.getHeight(), height);
		CPPUNIT_ASSERT_EQUAL(difference.getWidth(), width);
		CPPUNIT_ASSERT(difference.getErrors().empty());

		// Iterate though each row of the results matrix and compare it to the
		// subtracted two rows. Note that the function addSubtractRow is tested in
		// another unit test.
		for (int i = 0; i < height; ++i)
		{
			CPPUNIT_ASSERT(x.addSubtractRow(false, x.mMatrix[i], y.mMatrix[i]) == difference.mMatrix[i]);
		}

		// Verify there are not errors in any of the matrices.
		CPPUNIT_ASSERT(difference.getErrors().empty());
		CPPUNIT_ASSERT(x.getErrors().empty());
		CPPUNIT_ASSERT(y.getErrors().empty());


		// CASE: Verify that an exception will get thrown if the two matrices that are being
		// added do not have the same dimensions.

		// Add another row to one of the matrices so that they are uneven with each other.
		x.appendRow(getRandomIntContainer(width));

		CPPUNIT_ASSERT_THROW(x - y, Matrix<int>::UnevenMatricesException);
	}

	// ---------------------------------------------------------------------------
	void testEqualityOperators()
	{
		const int width = 3;
		const int height = 10;

		Matrix<int> x;

		// Create a matrix with random values.
		x = getMatrixWithRandomValues<int>(height, width);

		// Copy the matrix with all of the added values.
		const Matrix<int> y(x);

		// CASE: verify that the equals operator returns true if all 
		// Matrix object properties are equal and not unequal.
		CPPUNIT_ASSERT(x == y);
		CPPUNIT_ASSERT(false == (x != y));

		// CASE: verify that the operator returns false if the matrices have
		// different errors.
		x.mErrors.push_back("new error");
		CPPUNIT_ASSERT(false == (x == y));
		CPPUNIT_ASSERT(x != y);

		// Revert back the errors so the matrices are equal.
		x.mErrors.pop_back();
		CPPUNIT_ASSERT(x == y);

		// CASE: verify that the operator returns false if the matrices have
		// different matrix values.
		x.mMatrix.push_back(std::move(getRandomIntContainer(width)));
		CPPUNIT_ASSERT((x == y) == false);
		CPPUNIT_ASSERT(x != y);

		// Revert back the values so the matrices are equal.
		x.mMatrix.pop_back();
		CPPUNIT_ASSERT(x == y);

		// CASE: verify that the operator returns false if the matrices have
		// different error codes.
		ErrorManager<Matrix<int>::ErrorCodes> emptyErrorCodes;
		x.mErrorCodes = emptyErrorCodes;
		CPPUNIT_ASSERT((x == y) == false);
		CPPUNIT_ASSERT(x != y);
	}

	// ---------------------------------------------------------------------------
	void testSwap()
	{
		// Make two matrices and copy their original values so that they can be
		// used for comparison after the swap.
		Matrix<int> x;
		Matrix<int> y(3, 3);

		const std::vector<std::vector<int>> xMatrix = x.mMatrix;
		const std::vector<std::vector<int>> yMatrix = y.mMatrix;

		x.mErrors.push_back("error");

		const std::vector<std::string> xErrors = x.mErrors;
		const std::vector<std::string> yErrors = y.mErrors;

		ErrorManager<Matrix<int>::ErrorCodes> xErrorCodes;
		x.mErrorCodes = xErrorCodes;

		ErrorManager<Matrix<int>::ErrorCodes> yErrorCodes = y.mErrorCodes;

		// CASE: Verify that the swap function swaps objects.
		x.swap(y);

		CPPUNIT_ASSERT(xMatrix == y.mMatrix);
		CPPUNIT_ASSERT(yMatrix == x.mMatrix);

		CPPUNIT_ASSERT(xErrors == y.mErrors);
		CPPUNIT_ASSERT(yErrors == x.mErrors);

		CPPUNIT_ASSERT(xErrorCodes == y.mErrorCodes);
		CPPUNIT_ASSERT(yErrorCodes == x.mErrorCodes);
	}

	// ---------------------------------------------------------------------------
	void testAssignmentOperator()
	{
		// CASE: Verify that the assignment operator works properly.
		Matrix<double> x(4, 3);
		const Matrix<double> y = x;

		CPPUNIT_ASSERT(y == x);

		Matrix<double> z;
		z = y;

		CPPUNIT_ASSERT(y == z);
	}

	// ---------------------------------------------------------------------------
	void testScalarMultiplicationOperator()
	{
		// Create a random height and width.
		const int height = Random::getRangedRandomInt(1, 50);
		const int width = Random::getRangedRandomInt(1, 50);

		Matrix<int> x(height, width);

		// Populate a matrix with random values.
		std::vector<std::vector<int>> expectedValues;
		for (int i = 0; i < height; ++i)
		{
			expectedValues.push_back(std::move(getRandomIntContainer(width)));
			x.addModifyRow(i, expectedValues[i]);
		}

		const int scalar = 3;

		// Perform the scalar * matrix and matrix * scalar operations.
		Matrix<int> matrixTimesScalar = x * scalar; Matrix<int> scalarTimesMatrix = scalar * x;

		// Verify that the heights and widths of the results are correct.
		CPPUNIT_ASSERT_EQUAL(height, matrixTimesScalar.getHeight());
		CPPUNIT_ASSERT_EQUAL(height, scalarTimesMatrix.getHeight());

		CPPUNIT_ASSERT_EQUAL(x.getHeight(), matrixTimesScalar.getHeight());
		CPPUNIT_ASSERT_EQUAL(x.getWidth(), matrixTimesScalar.getWidth());
		CPPUNIT_ASSERT(matrixTimesScalar.getErrors().empty());

		CPPUNIT_ASSERT_EQUAL(x.getHeight(), scalarTimesMatrix.getHeight());
		CPPUNIT_ASSERT_EQUAL(x.getWidth(), scalarTimesMatrix.getWidth());
		CPPUNIT_ASSERT(matrixTimesScalar.getErrors().empty());

		// Verify that the values of the results are equal to the expected values.
		for (int i = 0; i < height; ++i)
		{
			for (int j = 0; j < width; ++j)
			{
				CPPUNIT_ASSERT_EQUAL(scalar * expectedValues[i][j], matrixTimesScalar[i][j]);
				CPPUNIT_ASSERT_EQUAL(scalar * expectedValues[i][j], scalarTimesMatrix[i][j]);
			}
		}
	}

	// ---------------------------------------------------------------------------
	void testMatrixMultiplicationOperator()
	{
		// CASE: Verify that an exception is thrown if the matrices are not able 
		// to be multiplied due to having incompatible widths and heights.
		Matrix<int> matrix(2, 3);
		matrix.addModifyRow(0, { 1, 2, 3 });
		matrix.addModifyRow(1, { 4, 5, 5 });

		Matrix<int> emptyMatrix;

		// This will throw an exception because the lhs is a 2 x 3 matrix and the rhs is a 1 x 1.
		CPPUNIT_ASSERT_THROW(matrix * emptyMatrix, Matrix<int>::NonMultipliableMatricesException);

		// CASE: Verify that the calculation works properly when the case matrix dimensions
		// are equal.
		const int randomSizeOne = Random::getRangedRandomInt(1, 50);
		const int randomSizeTwo = Random::getRangedRandomInt(1, 50);

		// Make two matrices where the height of one of them is equal to the width of the
		// other one so that the matrix product calculation is valid.
		Matrix<int> x(randomSizeOne, randomSizeTwo);
		Matrix<int> y(randomSizeTwo, randomSizeOne);

		// Populate each matrix with random values.
		x = getMatrixWithRandomValues<int>(x.getHeight(), x.getWidth());
		y = getMatrixWithRandomValues<int>(y.getHeight(), y.getWidth());

		// Calculate the expected results.
		Matrix<int> expectedResults(x.getHeight(), y.getWidth());
		for (int height = 0; height < x.getHeight(); ++height)
		{
			std::vector<int> temporaryRow;
			for (int i = 0; i < y.getWidth(); ++i)
			{
				temporaryRow.push_back(std::move(x.getDotProduct(x[height], y.getColumn(i))));
			}

			expectedResults.addModifyRow(height, temporaryRow);
		}

		// Validate the matrix product's dimensions and values.
		validateMatrixProduct(expectedResults, x, y, __LINE__);

		// CASE: Verify a hard-coded case where the lhs height is less than the
		// rhs height.
		Matrix<int> n(1, 3);
		n.addModifyRow(0, { 3, 4, 2 });

		Matrix<int> m(3, 4);
		m.addModifyRow(0, { 13, 9, 7, 15 });
		m.addModifyRow(1, { 8, 7, 4, 6 });
		m.addModifyRow(2, { 6, 4, 0, 3 });

		// Make a matrix with the expected results.
		Matrix<int> expectedMatrix(1, 4);
		expectedMatrix.addModifyRow(0, { 83, 63, 37, 75 });

		// Validate the matrix product's dimensions and values.
		validateMatrixProduct(expectedMatrix, n, m, __LINE__);

		// CASE: Verify hard-coded case where the lhs height is greater than the
		// rhs height.
		Matrix<int> a(4, 2);
		a.addModifyRow(0, { 1, 2 });
		a.addModifyRow(1, { 3, 4 });
		a.addModifyRow(2, { 5, 6 });
		a.addModifyRow(3, { 7, 8 });

		Matrix<int> b(2, 4);
		b.addModifyRow(0, { 9, 10, 11, 12 });
		b.addModifyRow(1, { 13, 14, 15, 16 });

		Matrix<int> expectedProduct(4, 4);
		expectedProduct.addModifyRow(0, { 35, 38, 41, 44 });
		expectedProduct.addModifyRow(1, { 79, 86, 93, 100 });
		expectedProduct.addModifyRow(2, { 123, 134, 145, 156 });
		expectedProduct.addModifyRow(3, { 167, 182, 197, 212 });

		// Validate the matrix product's dimensions and values.
		validateMatrixProduct(expectedProduct, a, b, __LINE__);
	}

	// ---------------------------------------------------------------------------
	void testGetDotProduct()
	{
		Matrix<int> matrix;

		const int width = Random::getRangedRandomInt(1, 50);

		std::vector<int> row = std::move(getRandomIntContainer(width));
		std::vector<int> column = std::move(getRandomIntContainer(width));

		CPPUNIT_ASSERT(row.size() == column.size());

		int expectedProduct = 0;

		for (int i = 0; i < row.size(); ++i)
		{
			expectedProduct += row[i] * column[i];
		}

		// CASE: Verify that the function is calculating the values properly.
		CPPUNIT_ASSERT_EQUAL(expectedProduct, matrix.getDotProduct(row, column));

		// CASE: Verify that when the rows and columns have mismatched sizes that 
		// the function will return 0;
		row.pop_back();
		CPPUNIT_ASSERT_EQUAL(0, matrix.getDotProduct(row, column));
	}

	// ---------------------------------------------------------------------------
	void testGetColumn()
	{
		// CASE: Verify that an empty matrix returns a container of size 1 if the
		// index is 0.
		Matrix<bool> emptyMatrix;

		std::vector<bool> emptyMatrixCol = emptyMatrix.getColumn(0);
		CPPUNIT_ASSERT(emptyMatrix.getErrors().empty());
		CPPUNIT_ASSERT_EQUAL(size_t(1), emptyMatrixCol.size());

		// CASE: Verify that the function gets the correct values.
		std::vector<std::vector<int>> matrixValues = {
			{ 1, 2, 3 },
			{ 4, 5, 6 },
			{ 7, 8, 9 } };

		Matrix<int> validMatrix;
		validMatrix.mMatrix = matrixValues;

		// Get the expected column values.
		std::vector<std::vector<int>> expectedColumnValues;

		int i = 0;
		while (i < matrixValues.size())
		{
			std::vector<int> column;

			for (const std::vector<int>& row : matrixValues)
			{
				column.push_back(row[i]);
			}

			expectedColumnValues.push_back(column);
			++i;
		}

		// Verify the columns retrieved are correct.
		for (int i = 0; i < validMatrix.getHeight(); ++i)
		{
			CPPUNIT_ASSERT(expectedColumnValues[i] == validMatrix.getColumn(i));
			CPPUNIT_ASSERT(validMatrix.getErrors().empty());
		}

		// CASE: Test that an error occurs if an invalid column index is input.
		const int invalidIndex = -10;

		const std::string expectedError= 
			validMatrix.mErrorCodes.getErrorMsg(Matrix<int>::INVALID_COLUMN_ACCESS, std::to_string(invalidIndex));

		CPPUNIT_ASSERT(std::vector<int>() == validMatrix.getColumn(invalidIndex));
		CPPUNIT_ASSERT(1 == validMatrix.getErrors().size());
		CPPUNIT_ASSERT_EQUAL(expectedError, validMatrix.mErrors[0]);
	}

	// ---------------------------------------------------------------------------
	void testRemoveColumn()
	{
		// Make a matrix.
		Matrix<int> matrix(3, 3);
		matrix.mMatrix = {
			{ 1, 2, 3 },
			{ 4, 5, 6 },
			{ 7, 8, 9 } };

		// CASE: Verify that the function removes the proper values

		// Remove the end column.
		CPPUNIT_ASSERT(matrix.removeColumn(2));

		// Verify that the dimensions are correct. 
		CPPUNIT_ASSERT_EQUAL(3, matrix.getHeight());
		CPPUNIT_ASSERT_EQUAL(2, matrix.getWidth());
		CPPUNIT_ASSERT(matrix.getErrors().empty());

		const std::vector<int> expectedColumn = { 2, 5, 8 };

		// Verify that the matrix values are correct.
		CPPUNIT_ASSERT(std::vector<int>({ 1, 4, 7 }) == matrix.getColumn(0));
		CPPUNIT_ASSERT(expectedColumn == matrix.getColumn(1));

		// Remove the first column and verify the matrix is correct.
		CPPUNIT_ASSERT(matrix.removeColumn(0));
		CPPUNIT_ASSERT_EQUAL(3, matrix.getHeight());
		CPPUNIT_ASSERT_EQUAL(1, matrix.getWidth());
		CPPUNIT_ASSERT(matrix.getErrors().empty());
		CPPUNIT_ASSERT(expectedColumn == matrix.getColumn(0));

		// CASE: Test that the column will not be removed if it is the only column
		// in the matrix.
		CPPUNIT_ASSERT(!matrix.removeColumn(0));
		CPPUNIT_ASSERT_EQUAL(3, matrix.getHeight());
		CPPUNIT_ASSERT_EQUAL(1, matrix.getWidth());
		CPPUNIT_ASSERT(matrix.getErrors().empty());
		CPPUNIT_ASSERT(expectedColumn == matrix.getColumn(0));

		// CASE: Test that if an invalid index is input that false will be returned,
		// an error will be logged and the matrix will not be changed.

		const int invalidColumn(2);
		CPPUNIT_ASSERT(!matrix.removeColumn(invalidColumn));
		CPPUNIT_ASSERT_EQUAL(3, matrix.getHeight());
		CPPUNIT_ASSERT_EQUAL(1, matrix.getWidth());
		CPPUNIT_ASSERT(expectedColumn == matrix.getColumn(0));

		CPPUNIT_ASSERT_EQUAL(size_t(1), matrix.getErrors().size());

		const std::string expectedError =
			matrix.mErrorCodes.getErrorMsg(Matrix<int>::INVALID_COLUMN_ACCESS, std::to_string(invalidColumn)); 

		CPPUNIT_ASSERT_EQUAL(expectedError, matrix.mErrors[0]);
	}

	// ---------------------------------------------------------------------------
	void testRemoveRow()
	{
		std::vector<int> rowZero = { 1, 2, 3 };
		std::vector<int> rowOne = { 4, 5, 6 };
		std::vector<int> rowTwo = { 7, 8, 9 };

		// Make a matrix.
		Matrix<int> matrix(3, 3);
		matrix.mMatrix = { rowZero, rowOne, rowTwo};

		// CASE: Verify that the function removes the proper values

		// Remove the bottom row.
		CPPUNIT_ASSERT(matrix.removeRow(2));

		// Verify that the dimensions are correct. 
		CPPUNIT_ASSERT_EQUAL(3, matrix.getWidth());
		CPPUNIT_ASSERT_EQUAL(2, matrix.getHeight());
		CPPUNIT_ASSERT(matrix.getErrors().empty());

		// Verify that the matrix values are correct.
		CPPUNIT_ASSERT(rowZero == matrix.getRow(0));
		CPPUNIT_ASSERT(rowOne == matrix.getRow(1));

		// Remove the first row and verify the matrix is correct.
		CPPUNIT_ASSERT(matrix.removeRow(0));
		CPPUNIT_ASSERT_EQUAL(1, matrix.getHeight());
		CPPUNIT_ASSERT_EQUAL(3, matrix.getWidth());
		CPPUNIT_ASSERT(matrix.getErrors().empty());
		CPPUNIT_ASSERT(rowOne== matrix.getRow(0));

		// CASE: Test that the row will not be removed if it is the only row 
		// in the matrix.
		CPPUNIT_ASSERT(!matrix.removeRow(0));
		CPPUNIT_ASSERT_EQUAL(1, matrix.getHeight());
		CPPUNIT_ASSERT_EQUAL(3, matrix.getWidth());
		CPPUNIT_ASSERT(matrix.getErrors().empty());
		CPPUNIT_ASSERT(rowOne == matrix.getRow(0));

		// CASE: Test that if an invalid index is input that false will be returned,
		// an error will be logged and the matrix will not be changed.
		const int invalidRow(2);

		CPPUNIT_ASSERT(!matrix.removeRow(invalidRow));
		CPPUNIT_ASSERT_EQUAL(1, matrix.getHeight());
		CPPUNIT_ASSERT_EQUAL(3, matrix.getWidth());
		CPPUNIT_ASSERT(rowOne == matrix.getRow(0));

		CPPUNIT_ASSERT_EQUAL(size_t(1), matrix.getErrors().size());

		const std::string expectedError =
			matrix.mErrorCodes.getErrorMsg(Matrix<int>::INVALID_ROW_ACCESS, std::to_string(invalidRow)); 

		CPPUNIT_ASSERT_EQUAL(expectedError, matrix.mErrors[0]);
	}

	// ---------------------------------------------------------------------------
	void testGetTwoByTwoDeterminant()
	{
		Matrix<int> matrix;

		// CASE: Verify that an exception is thrown if the matrix is not 2 by 2.
		CPPUNIT_ASSERT_THROW(matrix.getTwoByTwoDeterminant(Matrix<int>()), Matrix<int>::UnevenMatricesException);

		// CASE: Test that the function calculates correct values with hard-coded values.
		Matrix<int> hardCodedMatrix(2, 2);
		hardCodedMatrix.mMatrix = {
			{ 3, 8 },
			{ 4, 6 } };

		CPPUNIT_ASSERT_EQUAL(-14, matrix.getTwoByTwoDeterminant(hardCodedMatrix));

		// CASE: Test that the function calculates correct values with random values.
		for (int i = 0; i < 5; ++i)
		{
			const Matrix<int> randomMatrix = getMatrixWithRandomValues<int>(2, 2);

			// The expected value is ad - bc
			const int expectedValue = (randomMatrix[0][0] * randomMatrix[1][1]) - (randomMatrix[0][1] * randomMatrix[1][0]);

			CPPUNIT_ASSERT_EQUAL(expectedValue, matrix.getTwoByTwoDeterminant(randomMatrix));
		}
	}

	// ---------------------------------------------------------------------------
	void testGetColumnOrRowWithMostZeros()
	{
		// CASE: Test the case of an empty 1 x 1 matrix.
		Matrix<int> emptyMatrix;

		CPPUNIT_ASSERT(std::make_tuple(Matrix<int>::ROW, 0) == 
			emptyMatrix.getColumnOrRowWithMostZeros());

		// CASE: Test that the default row is returned if no rows/columns
		// have more zeros than another.
		Matrix<int> matrixOne(4, 3);
		matrixOne.mMatrix = {
			{ 1, 2, 3 },
			{ 4, 5, 6 },
			{ 7, 8, 8 },
			{ 9, 10, 11 } };

		CPPUNIT_ASSERT(std::make_tuple(Matrix<int>::ROW, 0) == 
			matrixOne.getColumnOrRowWithMostZeros());

		// CASE: Verify that the row with the most zeros is returned correctly.
		Matrix<int> matrixTwo(4, 3);
		matrixTwo.mMatrix = {
			{ 0, 2, 3 },
			{ 4, 0, 0 },
			{ 7, 8, 8 },
			{ 0, 0, 0 } };

		CPPUNIT_ASSERT(std::make_tuple(Matrix<int>::ROW, 3) == 
			matrixTwo.getColumnOrRowWithMostZeros());

		// CASE: Verify that the column with the most zeros is returned correctly.
		Matrix<int> matrixThree(4, 3);
		matrixThree.mMatrix = {
			{ 0, 2, 0 },
			{ 4, 0, 0 },
			{ 7, 8, 0 },
			{ 0, 0, 0 } };

		CPPUNIT_ASSERT(std::make_tuple(Matrix<int>::COLUMN, 2) == 
			matrixThree.getColumnOrRowWithMostZeros());


		// CASE: Verify that if a row and a column have the same amount of zeros
		// that the row position is returned.
		Matrix<double> matrixFour(4, 3);
		matrixFour.mMatrix = {
			{ 1, 2, 3 },
			{ 0, 0, 0 },
			{ 7, 8, 0 },
			{ 1, 0, 0 } };

		CPPUNIT_ASSERT(std::make_tuple(Matrix<int>::ROW, 1) == 
			matrixFour.getColumnOrRowWithMostZeros());
	}

	// ---------------------------------------------------------------------------
	void testGetDeterminantCalculationPattern()
	{
		Matrix<int> matrix;

		// CASE: Test that 1 is returned if an even index is input.
		CPPUNIT_ASSERT_EQUAL(1, matrix.getDeterminantCalculationPattern(0));

		// CASE: Test that -1 is returned if an odd index is input.
		CPPUNIT_ASSERT_EQUAL(-1, matrix.getDeterminantCalculationPattern(5));
	}

private:

	// ---------------------------------------------------------------------------
	// Generates a vector of random integer values of the input size. The size
	// must be greater than -1;
	std::vector<int> getRandomIntContainer(const int size) const
	{
        return Random::getRandomIntContainer(size, -50000, 50000);
	}

	// ---------------------------------------------------------------------------
	// Generates a matrix of random values of the input size. The sizes
	// must be greater than -1;
	template <class T>
	Matrix<T> getMatrixWithRandomValues(const int height, const int width)
	{
		CPPUNIT_ASSERT(height > -1);
		CPPUNIT_ASSERT(width > -1);

		Matrix<T> randomMatrix;
		randomMatrix.mMatrix.clear();

		for (int i = 0; i < height; ++i)
		{
			randomMatrix.mMatrix.push_back(std::move(getRandomIntContainer(width)));
		}

		return randomMatrix;
	}

	// ---------------------------------------------------------------------------
	// Multiplies two matrices and verifies the product of the matrices. The
	// lineCalled parameter is used to pass what line this function was called
	// by to the CPPUNIT_* messages.
	template <class T>
	void validateMatrixProduct(
		const Matrix<T>& expectedProduct,
		Matrix<T> lhs,
		Matrix<T> rhs,
		const int lineCalled) const
	{
		const std::string debugMsg("Called from line: " + std::to_string(lineCalled));

		// Multiply the lhs and rhs matrices.
		const Matrix<int> product = lhs * rhs;

		// Verify that the matrix dimensions are correct.
		CPPUNIT_ASSERT_EQUAL_MESSAGE(debugMsg, lhs.getHeight(), product.getHeight());
		CPPUNIT_ASSERT_EQUAL_MESSAGE(debugMsg, rhs.getWidth(), product.getWidth());
		CPPUNIT_ASSERT_MESSAGE(debugMsg, product.getErrors().empty());

		// Verify that the expected product values equal the actual ones.
		CPPUNIT_ASSERT_MESSAGE(debugMsg, expectedProduct == product);
	}
	

	// ------------------------------------------------------------------------
	CPPUNIT_TEST_SUITE(MatrixTest);
	CPPUNIT_TEST(testGetHeight);
	CPPUNIT_TEST(testGetWidth);
	CPPUNIT_TEST(testAppendRow);
	CPPUNIT_TEST(testGetRow);
	CPPUNIT_TEST(testErrors);
	CPPUNIT_TEST(testBracketOperator);
	CPPUNIT_TEST(testValidateRowIndex);
	CPPUNIT_TEST(testValidateColumnIndex);
	CPPUNIT_TEST(testValidateNewRow);
	CPPUNIT_TEST(testCopyConstructor);
	CPPUNIT_TEST(testModifyRow);
	CPPUNIT_TEST(testAddModifyRow);
	CPPUNIT_TEST(testAt);
	CPPUNIT_TEST(testValidateEvenMatrices);
	CPPUNIT_TEST(testAddSubtractRow);
	CPPUNIT_TEST(testAddOperator);
	CPPUNIT_TEST(testSubtractOperator);
	CPPUNIT_TEST(testEqualityOperators);
	CPPUNIT_TEST(testSwap);
	CPPUNIT_TEST(testAssignmentOperator);
	CPPUNIT_TEST(testScalarMultiplicationOperator);
	CPPUNIT_TEST(testMatrixMultiplicationOperator);
	CPPUNIT_TEST(testGetDotProduct);
	CPPUNIT_TEST(testGetColumn);
	CPPUNIT_TEST(testRemoveColumn);
	CPPUNIT_TEST(testRemoveRow);
	CPPUNIT_TEST(testGetTwoByTwoDeterminant);
	CPPUNIT_TEST(testGetColumnOrRowWithMostZeros);
	CPPUNIT_TEST(testGetDeterminantCalculationPattern);
	CPPUNIT_TEST_SUITE_END();
};

CPPUNIT_TEST_SUITE_REGISTRATION(MatrixTest);


int main(int argc, char* argv[])
{
	// Get the top level suite from the registry
	CppUnit::Test *suite = CppUnit::TestFactoryRegistry::getRegistry().makeTest();

	// Adds the test to the list of test to run
	CppUnit::TextUi::TestRunner runner;
	runner.addTest(suite);

	// Change the default outputter to a compiler error format outputter
	runner.setOutputter(new CppUnit::CompilerOutputter(&runner.result(),
		std::cerr));
	// Run the tests.
	bool wasSucessful = runner.run();

	// Return error code 1 if the one of test failed.
	return wasSucessful ? 0 : 1;
}
