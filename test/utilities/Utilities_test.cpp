#include <cstdio>
#include <fstream>

#include "Utilities.hpp"

#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/ui/text/TestRunner.h>

using namespace CppUnit;


static const std::string gTempFile = "temp.txt";
static const std::string gTempDir= "tempDir";
static const std::string gAlternateTempFile = "alternateTemp.txt";
static const std::string gAlternateTempDir= "alternateTempDir";

class UtilitiesTest : public TestFixture
{
public:

	// ---------------------------------------------------------------------------
	void setUp()
	{
	}

	// ---------------------------------------------------------------------------
	void tearDown()
	{
        // Remove the temporary data in case it exists.
        std::vector<std::string> tempData { gTempFile, gTempDir, gAlternateTempFile, gAlternateTempDir };

        for (auto data : tempData)
        {
            std::remove(data.c_str());
        }
	}

	// ---------------------------------------------------------------------------
	void testPathExists()
	{
        // CASE: Test that false is returned if a file does not exist.
        CPPUNIT_ASSERT(!Utilities::pathExists(gTempFile));


        // CASE: Test that true is returned if a file exists.
        std::string cmd = "touch " + gTempFile;
        CPPUNIT_ASSERT(std::system(cmd.c_str()) == 0);

        // Verify that the path exists.
        CPPUNIT_ASSERT(Utilities::pathExists(gTempFile));


        // CASE: Test that false is returned if a directory does not exist.
        CPPUNIT_ASSERT(!Utilities::pathExists(gTempDir));

        
        // CASE: Test that true is returned if a directory exists
        cmd = "mkdir " + gTempDir;
        CPPUNIT_ASSERT(std::system(cmd.c_str()) == 0);

        CPPUNIT_ASSERT(Utilities::pathExists(gTempDir));
	}

	// ---------------------------------------------------------------------------
    void testGetCurrentDateAndTime()
    {
        // Use the function being tested to get the current date and time.
        const std::string currentDateTime = Utilities::getCurrentDateAndTime();

        // Query the system to get the current date and time and write the 
        // output to a file.
        std::string cmd = "date '+%m/%d/%y %T' > " + gTempFile;
        CPPUNIT_ASSERT(std::system(cmd.c_str()) == 0);

        CPPUNIT_ASSERT(Utilities::pathExists(gTempFile));

        // Read the date from the file.
        std::string expectedDate;
        Utilities::readFileToString(gTempFile, expectedDate);

        // The newline character is added to the results since the system command
        // writes a newline the file
        CPPUNIT_ASSERT_EQUAL(expectedDate, currentDateTime + "\n");
    }

	// ---------------------------------------------------------------------------
    void testCreateFile()
    {
        // CASE: Verify that if the file already exists that false is returned.

        // Create a file.
        CPPUNIT_ASSERT(!Utilities::pathExists(gTempFile));

        std::string cmd = "touch " + gTempFile;
        CPPUNIT_ASSERT(std::system(cmd.c_str()) == 0);

        CPPUNIT_ASSERT(Utilities::pathExists(gTempFile));

        CPPUNIT_ASSERT(!Utilities::createFile(gTempFile));


        // CASE: Verify that if the file does not exist that true is returned
        // and a file exists.

        // Remove the file.
        CPPUNIT_ASSERT(std::remove(gTempFile.c_str()) == 0);

        CPPUNIT_ASSERT(Utilities::createFile(gTempFile));

        CPPUNIT_ASSERT(Utilities::pathExists(gTempFile));
    }

    // ---------------------------------------------------------------------------
    void testReadFileToString()
    {
        std::string actualFileStr;

        // CASE: Verify that false is returned if the file cannot be read.
        CPPUNIT_ASSERT(!Utilities::readFileToString("nonExistentFile.txt", actualFileStr));
        CPPUNIT_ASSERT(actualFileStr.empty());

        // CASE: Verify that when a file is read successfully the output string is the
        // same as what is in the file.
        const std::string expectedFileStr = "This string is in the file.";

        std::ofstream fileStream(gTempFile);

        fileStream << expectedFileStr;
        fileStream.close();

        CPPUNIT_ASSERT(Utilities::pathExists(gTempFile));

        CPPUNIT_ASSERT(Utilities::readFileToString(gTempFile, actualFileStr));
        CPPUNIT_ASSERT_EQUAL(expectedFileStr, actualFileStr);
    }

    // ---------------------------------------------------------------------------
    void testCopyFile()
    {
        // CASE: copy a file successfully.
        CPPUNIT_ASSERT(Utilities::createFile(gTempFile));
        CPPUNIT_ASSERT(Utilities::copyData(gTempFile, gAlternateTempFile));
        CPPUNIT_ASSERT(Utilities::pathExists(gAlternateTempFile));

        // CASE: copy a directory successfully.
        const std::string cmd = "mkdir " + gTempDir;
        CPPUNIT_ASSERT(std::system(cmd.c_str()) == 0);

        CPPUNIT_ASSERT(Utilities::copyData(gTempDir, gAlternateTempDir));
        CPPUNIT_ASSERT(Utilities::pathExists(gAlternateTempDir));

        // CASE: data unsuccessfully.
        CPPUNIT_ASSERT(!Utilities::copyData(gTempFile, ""));
    }

    // ------------------------------------------------------------------------
    void testFlattenStringVector()
    {
        const std::string delimeter = ", ";
        const std::vector<std::string> expectedValues = { "The", "quick", "brown", "fox" };

        // CASE: Test the expected string when the delimeter is not excluded.
        std::string expectedOutput;
        for(auto str : expectedValues)
        {
            expectedOutput += str + delimeter;
        }

        CPPUNIT_ASSERT_EQUAL(expectedOutput, Utilities::flattenStringVector(expectedValues, delimeter, false));


        // CASE: Test the expected string when the delimeter is excluded.

        // Remove the end delimeter from the expected output.
        expectedOutput.erase(std::end(expectedOutput) - delimeter.size(), std::end(expectedOutput));

        CPPUNIT_ASSERT_EQUAL(expectedOutput, Utilities::flattenStringVector(expectedValues, delimeter));
    }

    // ------------------------------------------------------------------------
    void testEncapsulateString()
    {
        const std::string originalStr = "hello";

        // CASE: Test encapsulating string with empty character and see that the
        // identical string is returned.

        CPPUNIT_ASSERT_EQUAL(originalStr, Utilities::encapsulateString(originalStr, '\0'));

        // CASE: Test encapsulating string with character and see that the
        // correct string is returned.
        const char paren = ')';
        const std::string expectedStr = paren + originalStr + paren;

        CPPUNIT_ASSERT_EQUAL(expectedStr, Utilities::encapsulateString(originalStr, paren));
    }

    // ------------------------------------------------------------------------
    void testDeencapsulateString()
    {
        // CASE: Test that the original string is returned if the passed in string
        // is less than two characters.
        const std::string smallStr = "x";
        CPPUNIT_ASSERT_EQUAL(smallStr, Utilities::deencapsulateString(smallStr, 'x'));

        // CASE: Test that the original string is returned if the passed in string is
        // greater than one character, but the passed in character is not at both ends
        // of the string.
        const std::string originalStr = "(hello world";
        CPPUNIT_ASSERT_EQUAL(originalStr, Utilities::deencapsulateString(originalStr, ')'));

        // CASE: Test that the original passed-in string is returned if the passed in character is null.
        const std::string nullStr = "nullStr\0\0";
        CPPUNIT_ASSERT_EQUAL(nullStr, Utilities::deencapsulateString(nullStr, '\0'));

        // CASE: Test that string is stripped of the passed-in character successfully.
        const std::string fullStr = "'my string'";
        CPPUNIT_ASSERT_EQUAL(std::string("my string"), Utilities::deencapsulateString(fullStr, '\''));
    }

private:


	// ------------------------------------------------------------------------
	CPPUNIT_TEST_SUITE(UtilitiesTest);
	CPPUNIT_TEST(testPathExists);
	CPPUNIT_TEST(testGetCurrentDateAndTime);
	CPPUNIT_TEST(testCreateFile);
    CPPUNIT_TEST(testReadFileToString);
    CPPUNIT_TEST(testCopyFile);
    CPPUNIT_TEST(testFlattenStringVector);
    CPPUNIT_TEST(testEncapsulateString);
    CPPUNIT_TEST(testDeencapsulateString);
    CPPUNIT_TEST_SUITE_END();
};

CPPUNIT_TEST_SUITE_REGISTRATION(UtilitiesTest);


int main(int argc, char* argv[])
{
	// Get the top level suite from the registry
	CppUnit::Test *suite = CppUnit::TestFactoryRegistry::getRegistry().makeTest(); 
	// Adds the test to the list of test to run
	CppUnit::TextUi::TestRunner runner;
	runner.addTest(suite);

	// Change the default outputter to a compiler error format outputter
	runner.setOutputter(new CppUnit::CompilerOutputter(&runner.result(),
		std::cerr));
	// Run the tests.
	bool wasSucessful = runner.run();

	// Return error code 1 if the one of test failed.
	return wasSucessful ? 0 : 1;
}
