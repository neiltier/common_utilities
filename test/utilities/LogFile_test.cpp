#include "LogFile.hpp"

#include <cstdio>

#include "Utilities.hpp"

#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/ui/text/TestRunner.h>

using namespace CppUnit;

static const std::string gTempFile = "LogFile.txt";

class LogFileTest : public TestFixture
{
public:

	// ---------------------------------------------------------------------------
	void setUp()
	{
	}

	// ---------------------------------------------------------------------------
	void tearDown()
	{
        LogFile::mUnitTesting = false;
        remove(gTempFile.c_str());
	}

	// ---------------------------------------------------------------------------
	void testConstructor()
	{
        // CASE: Test that an error is reported if the file can't be deleted
        // before opening and then test that if the file can't be opened an
        // error is shown..
        LogFile::mUnitTesting = true;

        // Create a file.
        CPPUNIT_ASSERT(Utilities::createFile(gTempFile));

        // Set the file to have no permissions so it can't be read.
        const std::string cmd = "chmod 000 " + gTempFile;
        CPPUNIT_ASSERT(std::system(cmd.c_str()) == 0);

        LogFile logFile(gTempFile);

        const std::string expectedRemoveFileError = logFile.mErrorCodes.getErrorMsg(LogFile::UNABLE_TO_REMOVE_EXISTING_FILE, gTempFile) + "\n";
        const std::string expectedOpenFileError = logFile.mErrorCodes.getErrorMsg(LogFile::UNABLE_TO_OPEN_FILE, gTempFile) + "\n";
        CPPUNIT_ASSERT_EQUAL(expectedRemoveFileError + expectedOpenFileError, logFile.getErrors());
   }

	// ---------------------------------------------------------------------------
	void testWrite()
	{
        // Write the file.
        LogFile logFile(gTempFile);

        CPPUNIT_ASSERT(logFile.getErrors().empty());

        const std::string expectedDateTime = Utilities::getCurrentDateAndTime(); 

        const std::string expectedStrOne = "This is a test string.";
        logFile.write(expectedStrOne);

        const std::string expectedStrTwo = "This is another test string.";
        logFile.write(expectedStrTwo);

        // Read the results.
        std::string fileContents;
        CPPUNIT_ASSERT(Utilities::readFileToString(gTempFile, fileContents));

        // Compare the file string to the expected string.
        std::string expectedLoggingLines = "\n" + LogFile::mFileSeperator + "\n";
        expectedLoggingLines += expectedDateTime + "\n";

        std::string expectedString = expectedLoggingLines + expectedStrOne + "\n";
        expectedString += expectedLoggingLines + expectedStrTwo + "\n";

        CPPUNIT_ASSERT_EQUAL(expectedString, fileContents);
    }

private:


	// ------------------------------------------------------------------------
	CPPUNIT_TEST_SUITE(LogFileTest);
	CPPUNIT_TEST(testConstructor);
	CPPUNIT_TEST(testWrite);
	CPPUNIT_TEST_SUITE_END();
};

CPPUNIT_TEST_SUITE_REGISTRATION(LogFileTest);


int main(int argc, char* argv[])
{
	// Get the top level suite from the registry
	CppUnit::Test *suite = CppUnit::TestFactoryRegistry::getRegistry().makeTest(); 
	// Adds the test to the list of test to run
	CppUnit::TextUi::TestRunner runner;
	runner.addTest(suite);

	// Change the default outputter to a compiler error format outputter
	runner.setOutputter(new CppUnit::CompilerOutputter(&runner.result(),
		std::cerr));
	// Run the tests.
	bool wasSucessful = runner.run();

	// Return error code 1 if the one of test failed.
	return wasSucessful ? 0 : 1;
}
