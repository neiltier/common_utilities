add_executable(errorManager_test ErrorManager_test.cpp)

target_link_libraries(errorManager_test error_management)

# Third party: cppunit
target_link_libraries(errorManager_test cppunit)

set(EXECUTABLE_OUTPUT_PATH "bin")

add_test(errorManager_test ${EXECUTABLE_OUTPUT_PATH}/errorManager_test)
