#include "ErrorManager.hpp"

#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/ui/text/TestRunner.h>

using namespace CppUnit;

class ErrorManagerTest : public TestFixture
{
public:

	// ---------------------------------------------------------------------------
	void setUp()
	{
		mErrorManager = new ErrorManager<ErrorType>();
	}

	// ---------------------------------------------------------------------------
	void tearDown()
	{
		delete mErrorManager;
	}

	// ---------------------------------------------------------------------------
	void testAddError()
	{
		const ErrorType errorCode = ERROR_TYPE_1;

		// CASE: Test adding a non-duplicate error.
		mErrorManager->addError(errorCode, "valid error");

		// CASE: Test that the duplicate assertion in this function works.
		CPPUNIT_ASSERT(mErrorManager->mErrors.find(errorCode) != mErrorManager->mErrors.end());
	}


	// ---------------------------------------------------------------------------
	void testEqualityOperators()
	{
		// CASE: Test that true is returned when two objects are equal.
		ErrorManager<ErrorType> errorMgr;
        CPPUNIT_ASSERT(*mErrorManager == errorMgr);


		// CASE: Test that false is returned when two objects have the same 
        // error code, but different messages.
		const ErrorType errorCode = ERROR_TYPE_1;

		mErrorManager->addError(errorCode, "message one");
		errorMgr.addError(errorCode, "message two");

        CPPUNIT_ASSERT(*mErrorManager != errorMgr);


		// CASE: Test that false is returned when two objects have a different
        // amount of error messages.
        errorMgr.addError(ERROR_TYPE_2, "error");
        CPPUNIT_ASSERT(*mErrorManager != errorMgr);


		// CASE: Test that false is returned when two objects have different 
        // error codes, but the same message.
        ErrorManager<ErrorType> errorManager;
        const std::string msg = "message";
        errorManager.addError(ERROR_TYPE_1, msg);

        mErrorManager->mErrors.clear();
		mErrorManager->addError(ERROR_TYPE_2, msg);

        CPPUNIT_ASSERT(*mErrorManager != errorManager);
	}

	// ---------------------------------------------------------------------------
    void testAssignmentOperator()
    {
        // CASE: Test that the assignment operator works properly.
		ErrorManager<ErrorType> errorMgr;
		errorMgr.addError(ERROR_TYPE_1, "msg");

        // Verify the objects are not equal.
        CPPUNIT_ASSERT(*mErrorManager != errorMgr);

        // Assign one of the objects to the other.
        errorMgr = *mErrorManager;

        // Verify the objects are equal.
        CPPUNIT_ASSERT(*mErrorManager == errorMgr);
    }

	// ---------------------------------------------------------------------------
    void testGetErrorMsgNoParameters()
    {
        const std::string expectedMsg("error message");
        const ErrorType errorCode = ERROR_TYPE_1;

        // CASE: test that an assertion failure will occur if the input error
        // code doesn't exist. This tests for all getErrorMessage(...) functions.
        CPPUNIT_ASSERT(mErrorManager->mErrors.find(errorCode) 
                == mErrorManager->mErrors.end());

        // Add an error.
        mErrorManager->addError(errorCode, expectedMsg);

        // Verify that the correct error is output based on the error message.
        CPPUNIT_ASSERT_EQUAL(expectedMsg, mErrorManager->getErrorMsg(errorCode));
    }

	// ---------------------------------------------------------------------------
    void testGetErrorMsgOneParameter()
    {
        const std::string errorMsg("error message with " + mErrorManager->STR + " parameter.");
        const ErrorType errorCode = ERROR_TYPE_1;

        // Add an error.
        mErrorManager->addError(errorCode, errorMsg);

        // Verify that the correct error is output based on the error message.
        const std::string expectedErrorMsg = "error message with one parameter.";
        CPPUNIT_ASSERT_EQUAL(expectedErrorMsg, mErrorManager->getErrorMsg(errorCode, "one"));
    }

	// ---------------------------------------------------------------------------
    void testGetErrorMsgManyParameters()
    {
        const std::string errorMsg(
                "The " + mErrorManager->STR + " brown " + mErrorManager->STR + " jumped.");

        const ErrorType errorCode = ERROR_TYPE_1;

        // Add an error.
        mErrorManager->addError(errorCode, errorMsg);

        const std::vector<std::string> params = {"quick", "fox"};

        // Verify that the correct error is output based on the error message.
        const std::string expectedErrorMsg = "The quick brown fox jumped.";
        CPPUNIT_ASSERT_EQUAL(expectedErrorMsg, mErrorManager->getErrorMsg(errorCode, params));
    }

private:

	enum ErrorType
	{
		ERROR_TYPE_1,
		ERROR_TYPE_2,
	};

	ErrorManager<ErrorType>* mErrorManager;


	// ------------------------------------------------------------------------
	CPPUNIT_TEST_SUITE(ErrorManagerTest);
	CPPUNIT_TEST(testAddError);
	CPPUNIT_TEST(testEqualityOperators);
	CPPUNIT_TEST(testAssignmentOperator);
	CPPUNIT_TEST(testGetErrorMsgNoParameters);
	CPPUNIT_TEST(testGetErrorMsgOneParameter);
	CPPUNIT_TEST(testGetErrorMsgManyParameters);
	CPPUNIT_TEST_SUITE_END();
};

CPPUNIT_TEST_SUITE_REGISTRATION(ErrorManagerTest);


int main(int argc, char* argv[])
{
	// Get the top level suite from the registry
	CppUnit::Test *suite = CppUnit::TestFactoryRegistry::getRegistry().makeTest(); 
	// Adds the test to the list of test to run
	CppUnit::TextUi::TestRunner runner;
	runner.addTest(suite);

	// Change the default outputter to a compiler error format outputter
	runner.setOutputter(new CppUnit::CompilerOutputter(&runner.result(),
		std::cerr));
	// Run the tests.
	bool wasSucessful = runner.run();

	// Return error code 1 if the one of test failed.
	return wasSucessful ? 0 : 1;
}
