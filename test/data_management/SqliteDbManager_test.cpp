#include "SqliteDbManager.hpp"
#include "Utilities.hpp"

#include <cstdio>
#include <sqlite3.h>

#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/ui/text/TestRunner.h>

using namespace CppUnit;
using DataManagement::SqliteDbManager;

static const std::string gDbFile = "TempDatabase.db";
static const std::string gTempFile= "tempFile.db";
static const std::string gLogFile = "LogFile.log";

// Database table used for tested.
static const std::string gExampleTable = "Employee";

class SqliteDbManagerTest: public TestFixture
{
public:

	// ---------------------------------------------------------------------------
	void setUp()
	{
        // Copy a sample database with dummy data in it to the temp database so
        // that it can be tested with.
        CPPUNIT_ASSERT(Utilities::copyData("testingDb.db", gDbFile));

        mLogFile = new LogFile(gLogFile);
        CPPUNIT_ASSERT(mLogFile->getErrors().empty());

        mDbManager = new SqliteDbManager(gDbFile, *mLogFile);
        CPPUNIT_ASSERT(mDbManager->isConnected());

        mExpectedData = makeDataCells("Ellen", "Hotchkins", "88");
    }

	// ---------------------------------------------------------------------------
	void tearDown()
	{
        delete mDbManager;
        mDbManager = nullptr;

        delete mLogFile;
        mLogFile = nullptr;

        remove(gDbFile.c_str());
        remove(gLogFile.c_str());
        remove(gTempFile.c_str());
    }

    // ---------------------------------------------------------------------------
    void testDestructor()
    {
        // CASE: Test that when the destructor is called that the database is disconected
        // and then a new database can be opened.
        CPPUNIT_ASSERT(mDbManager->isConnected());
        mDbManager->~SqliteDbManager();
        mDbManager = nullptr;

        SqliteDbManager dbManager(gDbFile, *mLogFile);
        CPPUNIT_ASSERT(dbManager.isConnected());
    }

    // ---------------------------------------------------------------------------
    void testNoFileError()
    {
        remove(gDbFile.c_str());
        SqliteDbManager dbManager(gDbFile, *mLogFile);

        CPPUNIT_ASSERT(!dbManager.isConnected());

        verifyError(dbManager.mErrorCodes, SqliteDbManager::NO_DB_FILE, { gDbFile }, __LINE__);
    }

    // ---------------------------------------------------------------------------
    void testNoConnectError()
    {
        // Create the file then set the permissions to none so it can't be opened.
        CPPUNIT_ASSERT(Utilities::createFile(gTempFile));

        const std::string cmd = "chmod 000 " + gTempFile;
        CPPUNIT_ASSERT(std::system(cmd.c_str()) == 0);

        SqliteDbManager dbManager(gTempFile, *mLogFile);

        CPPUNIT_ASSERT(!dbManager.isConnected());

        verifyError(dbManager.mErrorCodes, SqliteDbManager::UNABLE_TO_CONNECT, { sqlite3_errmsg(dbManager.mDb) }, __LINE__);
    }
    
    // ---------------------------------------------------------------------------
    void testExecuteStatement()
    {
        // CASE: Test that the statement was executed unsuccessfully.
        std::string stmt = "INSERT INTO Employee (first_name, last_name, age)";

	const std::string typo = "xyz";
	const std::string brokenStmt = stmt + typo;

        CPPUNIT_ASSERT(!mDbManager->executeStatement(brokenStmt));
        
        const std::string sqlError = "near \"" + typo + "\": syntax error";
        verifyError(mDbManager->mErrorCodes, SqliteDbManager::STATEMENT_ERROR, { brokenStmt, sqlError }, __LINE__);
        
        // CASE: Test that the statement was executed successfully.
        stmt += " VALUES ('John', 'Smith', 34)";
        CPPUNIT_ASSERT(mDbManager->executeStatement(stmt));
    }

    // ---------------------------------------------------------------------------
    void testInsertSingleDataItem()
    {
        // CASE: Test failing to insert data into the table.
        const std::string fakeTable = "FakeTable";
        CPPUNIT_ASSERT(!mDbManager->insertData(fakeTable, { mExpectedData }));

        // Verify the error.
        const std::string stmt = "INSERT INTO " + fakeTable + " (first_name, last_name, age) VALUES ('Ellen', 'Hotchkins', '88')";
        const std::string error = "no such table: " + fakeTable;
        verifyError(mDbManager->mErrorCodes, SqliteDbManager::STATEMENT_ERROR, { stmt, error }, __LINE__);


        // CASE: Test succesfully inserting one data row into the database.
        CPPUNIT_ASSERT(mDbManager->insertData(gExampleTable, { mExpectedData }));


        // Verify the data was inserted.
        char* errMsg = nullptr;

        DataManagement::InputDbDataCells_t actualDbData;

        const std::string sql = "SELECT * from " + gExampleTable;
        CPPUNIT_ASSERT(sqlite3_exec(mDbManager->mDb, sql.c_str(), getDataCallback, &actualDbData, &errMsg) == SQLITE_OK);

        sqlite3_free(errMsg);

        // Verify the expected and the actual data.
        verifyDbData(mExpectedData, actualDbData, __LINE__);
   }

    // ---------------------------------------------------------------------------
    void testInsertMultipleDataItems()
    {
        // CASE: Test succesfully inserting multiple data rows into the table.
        const DataManagement::InputDbDataCells_t secondRowCells = makeDataCells("Kevin", "Hoint", "1");

        CPPUNIT_ASSERT(mDbManager->insertData(gExampleTable, { mExpectedData, secondRowCells }));

        // Add the second set of cells to the expected data.
        mExpectedData.insert(std::end(mExpectedData), std::begin(secondRowCells), std::end(secondRowCells));


        // Verify the data was inserted.
        char* errMsg = nullptr;

        DataManagement::InputDbDataCells_t actualDbData;

        const std::string sql = "SELECT * from " + gExampleTable;
        CPPUNIT_ASSERT(sqlite3_exec(mDbManager->mDb, sql.c_str(), getDataCallback, &actualDbData, &errMsg) == SQLITE_OK);

        sqlite3_free(errMsg);

        // Verify the expected and the actual data.
        verifyDbData(mExpectedData, actualDbData, __LINE__);
    }

    // ---------------------------------------------------------------------------
    void testUpdateSingleDataItem()
    {
        const DataManagement::InputDbDataCells_t newDataCells = makeDataCells("Evelyn", "Hotchkins", "81");

        // Insert data in the DB.
        CPPUNIT_ASSERT(mDbManager->insertData(gExampleTable, { newDataCells }));


        // CASE: Test succesfully updating data in the table.
        CPPUNIT_ASSERT(mDbManager->updateDataItem(gExampleTable, newDataCells, mExpectedData));


        // Modify the original data so that there is a column value that isn't recognized.
        const DataManagement::InputDbDataCells_t invalidData =
        {
            DataManagement::InputDbDataCell("DNE", "Jake"),
            DataManagement::InputDbDataCell("last_name", "Hotchkins"),
            DataManagement::InputDbDataCell("age", "81")
        };


        // Verify the data was updated.
        char* errMsg = nullptr;

        DataManagement::InputDbDataCells_t actualDbData;

        const std::string sql = "SELECT * from " + gExampleTable;
        CPPUNIT_ASSERT(sqlite3_exec(mDbManager->mDb, sql.c_str(), getDataCallback, &actualDbData, &errMsg) == SQLITE_OK);

        sqlite3_free(errMsg);

        // Verify the expected and the actual data.
        verifyDbData(newDataCells, actualDbData, __LINE__);


        // CASE: Test unsuccesfully updating data in the table.
        CPPUNIT_ASSERT(!mDbManager->updateDataItem(gExampleTable, newDataCells, invalidData));

        // Verify the error.
        std::string stmt = "UPDATE Employee SET first_name = 'Evelyn', last_name = 'Hotchkins', age = '81'";
        stmt += " WHERE DNE = 'Jake' AND last_name = 'Hotchkins' AND age = '81'";

        const std::string error = "no such column: " + invalidData.front().getColumnName();
        verifyError(mDbManager->mErrorCodes, SqliteDbManager::STATEMENT_ERROR, { stmt, error }, __LINE__);
    }

    // ---------------------------------------------------------------------------
    void testUpdateMultipleDataItems()
    {
        const DataManagement::InputDbData_t originalData =
        {
            makeDataCells("John", "Smith", "33"),
            mExpectedData
        };

        // Insert data into the DB.
        CPPUNIT_ASSERT(mDbManager->insertData(gExampleTable, originalData));

        const DataManagement::InputDbDataCells_t updateRowOneCells = makeDataCells("John", "Johnson", "34");
        const DataManagement::InputDbDataCells_t updateRowTwoCells = makeDataCells("Jessica", "Parker", "73");

        const DataManagement::InputDbData_t updateData =
        {
            updateRowOneCells,
            updateRowTwoCells
        };

        CPPUNIT_ASSERT(mDbManager->updateData(gExampleTable, updateData, originalData));

        // Verify the data was updated.
        DataManagement::InputDbDataCells_t expectedData = std::move(updateRowOneCells);
        expectedData.insert(std::cend(expectedData), std::cbegin(updateRowTwoCells), std::cend(updateRowTwoCells));

        char* errMsg = nullptr;

        DataManagement::InputDbDataCells_t actualDbData;


        const std::string sql = "SELECT * from " + gExampleTable;
        CPPUNIT_ASSERT(sqlite3_exec(mDbManager->mDb, sql.c_str(), getDataCallback, &actualDbData, &errMsg) == SQLITE_OK);

        sqlite3_free(errMsg);

        // Verify the expected and the actual data.
        verifyDbData(expectedData, actualDbData, __LINE__);
    }

    // ---------------------------------------------------------------------------
    void testRetrieveData()
    {
        const std::tuple<std::string, int> columnOne("column_1", 2);
        const std::tuple<std::string, double> columnTwo("column_2", 3.888);
        const std::tuple<std::string, std::string> columnThree("column_3", "text");

        const DataManagement::InputDbDataCells_t dataCells =
        {
            DataManagement::InputDbDataCell(std::get<0>(columnOne), "2"),
            DataManagement::InputDbDataCell(std::get<0>(columnTwo), "3.888"),
            DataManagement::InputDbDataCell(std::get<0>(columnThree), "text"),
        };


        // CASE: Verify that an error is output if a syntax error exists.
        std::string table = "FakeTable";
        std::string query = "SELECT * FROM " + table;

        DataManagement::OutputDbData_t outputData;
        CPPUNIT_ASSERT(!mDbManager->retrieveData(query, dataCells.size(), outputData));

        const std::string error = "no such table: " + table;
        verifyError(mDbManager->mErrorCodes, SqliteDbManager::QUERY_ERROR, { query , error }, __LINE__);

        // Insert data into the DB.
        table = "TestRetrieve";
        query = "SELECT * FROM " + table;

        CPPUNIT_ASSERT(mDbManager->insertData(table, { dataCells }));


        // CASE: Verify that an error is output if the number of data cells is out of bounds with
        // the returned results.
        const int invalidRowSize = dataCells.size() + 1;

        CPPUNIT_ASSERT(!mDbManager->retrieveData(query, invalidRowSize, outputData));

        verifyError(mDbManager->mErrorCodes, SqliteDbManager::INVALID_ROW_SIZE, { query, std::to_string(invalidRowSize) }, __LINE__);


        // CASE: Verify that one row of data is successfully extracted from the DB.
        CPPUNIT_ASSERT(mDbManager->retrieveData(query, dataCells.size(), outputData));
        CPPUNIT_ASSERT_EQUAL(size_t(1), outputData.size());

        // Verify that the input data matches the output data.
        const DataManagement::OutputDbDataCells_t results = outputData.front();
        CPPUNIT_ASSERT_EQUAL(dataCells.size(), results.size());

        CPPUNIT_ASSERT_EQUAL(std::get<0>(columnOne), results[0].getColumnName());
        CPPUNIT_ASSERT_EQUAL(std::get<1>(columnOne), std::any_cast<int>(results[0].getColumnValue()));

        CPPUNIT_ASSERT_EQUAL(std::get<0>(columnTwo), results[1].getColumnName());
        CPPUNIT_ASSERT_EQUAL(std::get<1>(columnTwo), std::any_cast<double>(results[1].getColumnValue()));

        CPPUNIT_ASSERT_EQUAL(std::get<0>(columnThree), results[2].getColumnName());
        CPPUNIT_ASSERT_EQUAL(std::get<1>(columnThree), std::any_cast<std::string>(results[2].getColumnValue()));


        // CASE: Verify that multiple rows of data are successfully extracted from the DB.
        const std::tuple<std::string, int> colOne(std::get<0>(columnOne), 99999);
        const std::tuple<std::string, double> colTwo(std::get<0>(columnTwo), 8.90831);
        const std::tuple<std::string, std::string> colThree(std::get<0>(columnThree), "second row text");

        const DataManagement::InputDbDataCells_t newDataCells =
        {
            DataManagement::InputDbDataCell(std::get<0>(columnOne), "99999"),
            DataManagement::InputDbDataCell(std::get<0>(columnTwo), "8.90831"),
            DataManagement::InputDbDataCell(std::get<0>(columnThree), "second row text"),
        };

        // Add another data row to the DB.
        CPPUNIT_ASSERT(mDbManager->insertData(table, { newDataCells }));

        // Obtain the data and verify it matches the input data.
        CPPUNIT_ASSERT(mDbManager->retrieveData(query, dataCells.size(), outputData));
        CPPUNIT_ASSERT_EQUAL(size_t(2), outputData.size());

        const DataManagement::OutputDbDataCells_t firstRow = outputData.front();
        CPPUNIT_ASSERT_EQUAL(dataCells.size(), firstRow.size());

        CPPUNIT_ASSERT_EQUAL(std::get<0>(columnOne), firstRow[0].getColumnName());
        CPPUNIT_ASSERT_EQUAL(std::get<1>(columnOne), std::any_cast<int>(firstRow[0].getColumnValue()));

        CPPUNIT_ASSERT_EQUAL(std::get<0>(columnTwo), firstRow[1].getColumnName());
        CPPUNIT_ASSERT_EQUAL(std::get<1>(columnTwo), std::any_cast<double>(firstRow[1].getColumnValue()));

        CPPUNIT_ASSERT_EQUAL(std::get<0>(columnThree), firstRow[2].getColumnName());
        CPPUNIT_ASSERT_EQUAL(std::get<1>(columnThree), std::any_cast<std::string>(firstRow[2].getColumnValue()));

        const DataManagement::OutputDbDataCells_t secondRow = outputData[1];
        CPPUNIT_ASSERT_EQUAL(dataCells.size(), secondRow.size());

        CPPUNIT_ASSERT_EQUAL(std::get<0>(colOne), secondRow[0].getColumnName());
        CPPUNIT_ASSERT_EQUAL(std::get<1>(colOne), std::any_cast<int>(secondRow[0].getColumnValue()));

        CPPUNIT_ASSERT_EQUAL(std::get<0>(colTwo), secondRow[1].getColumnName());
        CPPUNIT_ASSERT_EQUAL(std::get<1>(colTwo), std::any_cast<double>(secondRow[1].getColumnValue()));

        CPPUNIT_ASSERT_EQUAL(std::get<0>(colThree), secondRow[2].getColumnName());
        CPPUNIT_ASSERT_EQUAL(std::get<1>(colThree), std::any_cast<std::string>(secondRow[2].getColumnValue()));


        // CASE: Test that NULL data is returned as an empty std::any type.
        const std::tuple<std::string, int> columnNumberOne(std::get<0>(columnOne), 11);

        const DataManagement::InputDbDataCells_t partialDataCells =
        {
            DataManagement::InputDbDataCell(std::get<0>(columnOne), "11"),
        };

        // Add the partial row to the DB so that all columns besides the first one are null.
        CPPUNIT_ASSERT(mDbManager->insertData(table, { partialDataCells }));

        // Obtain the data and verify it matches the input data.
        CPPUNIT_ASSERT(mDbManager->retrieveData(query, dataCells.size(), outputData));
        CPPUNIT_ASSERT_EQUAL(size_t(3), outputData.size());

        const DataManagement::OutputDbDataCells_t thirdRow = outputData[2];
        CPPUNIT_ASSERT_EQUAL(dataCells.size(), thirdRow.size());

        // Verify that the first column isn't null.
        CPPUNIT_ASSERT_EQUAL(std::get<0>(columnNumberOne), thirdRow[0].getColumnName());
        CPPUNIT_ASSERT_EQUAL(std::get<1>(columnNumberOne), std::any_cast<int>(thirdRow[0].getColumnValue()));

        // Verify that the other columns have empty std::any types.
        CPPUNIT_ASSERT_EQUAL(std::get<0>(colTwo), thirdRow[1].getColumnName());
        CPPUNIT_ASSERT(!thirdRow[1].getColumnValue().has_value());

        CPPUNIT_ASSERT_EQUAL(std::get<0>(colThree), thirdRow[2].getColumnName());
        CPPUNIT_ASSERT(!thirdRow[2].getColumnValue().has_value());
    }

private:

    // ---------------------------------------------------------------------------
    // Callback function used to obtain database data. This function is used when calling sqlite3_exec().
    static int getDataCallback(void* dataContainer, int numberOfColumns, char** values, char** columnNames)
    {
        CPPUNIT_ASSERT(dataContainer);

        DataManagement::InputDbDataCells_t* actualData = static_cast<DataManagement::InputDbDataCells_t*>(dataContainer);

        CPPUNIT_ASSERT(dataContainer);

        for(int i = 0; i < numberOfColumns; ++i)
        {
            actualData->push_back(DataManagement::InputDbDataCell(columnNames[i], values[i]));
        }

        return 0;
    }

    // ---------------------------------------------------------------------------
    // Compares the passed-in containers for equality.
    void verifyDbData(const DataManagement::InputDbDataCells_t& expectedData,
                      const DataManagement::InputDbDataCells_t& actualData,
                      const int line)
    {
        const std::string lineStr = "Called from line: " + std::to_string(line);

        CPPUNIT_ASSERT_EQUAL_MESSAGE(lineStr, expectedData.size(), actualData.size());

        for(int i = 0; i < expectedData.size(); ++i)
        {
            CPPUNIT_ASSERT_EQUAL_MESSAGE(lineStr, expectedData[i].getColumnName(), actualData[i].getColumnName());

            std::string expectedValue = expectedData[i].getColumnValue();

            // Add the quote to the value in the expected data because it is always returned this
            // way from the db.
            if (expectedValue.front() != '\'' && expectedValue.back() != '\'')
            {
                expectedValue = Utilities::encapsulateString(expectedValue, '\'');
            }

            CPPUNIT_ASSERT_EQUAL_MESSAGE(lineStr, expectedValue, actualData[i].getColumnValue());
        }
    }

    // ---------------------------------------------------------------------------
    // Verifies that the expected error message exists in the log file.
    void verifyError(
            const ErrorManager<SqliteDbManager::ErrorCodes>& errorManager,
            const SqliteDbManager::ErrorCodes errorCode,
            const std::vector<std::string>& messageParameters,
            const int line)
    {
        const std::string lineStr = "Called from line: " + std::to_string(line);

        std::string actualError;
        CPPUNIT_ASSERT_MESSAGE(lineStr, Utilities::readFileToString(gLogFile, actualError));

        const std::string expectedError = errorManager.getErrorMsg(errorCode, messageParameters);

        std::string messageStr = lineStr + "\n";
        messageStr += "EXPECTED ERROR: " + expectedError + "\n";
        messageStr += "ACTUAL ERROR: " + actualError + "\n";

        CPPUNIT_ASSERT_MESSAGE(messageStr, actualError.find(expectedError) != std::string::npos);
    }

    // ---------------------------------------------------------------------------
    // Returns data cell container that is compatible with the data in the gExampleTable.
    DataManagement::InputDbDataCells_t makeDataCells(const std::string& firstName,
                                          const std::string& lastName,
                                          const std::string& age)
    {
        const DataManagement::InputDbDataCells_t dataCells =
        {
            DataManagement::InputDbDataCell("first_name", firstName),
            DataManagement::InputDbDataCell("last_name", lastName),
            DataManagement::InputDbDataCell("age", age)
        };

        return dataCells;
    }

    // The class that is being tested.
    SqliteDbManager* mDbManager;

    // The logfile where errors are written.
    LogFile* mLogFile;

    // Sample DbDataCells used for testing.
    DataManagement::InputDbDataCells_t mExpectedData;


	// ------------------------------------------------------------------------
	CPPUNIT_TEST_SUITE(SqliteDbManagerTest);
    CPPUNIT_TEST(testNoFileError);
    CPPUNIT_TEST(testNoConnectError);
    CPPUNIT_TEST(testDestructor);
    CPPUNIT_TEST(testExecuteStatement);
    CPPUNIT_TEST(testInsertSingleDataItem);
    CPPUNIT_TEST(testInsertMultipleDataItems);
    CPPUNIT_TEST(testUpdateSingleDataItem);
    CPPUNIT_TEST(testUpdateMultipleDataItems);
    CPPUNIT_TEST(testRetrieveData);
    CPPUNIT_TEST_SUITE_END();
};

CPPUNIT_TEST_SUITE_REGISTRATION(SqliteDbManagerTest);


// ------------------------------------------------------------------------
// ------------------------------------------------------------------------
int main(int argc, char* argv[])
{
	// Get the top level suite from the registry
	CppUnit::Test *suite = CppUnit::TestFactoryRegistry::getRegistry().makeTest(); 
	// Adds the test to the list of test to run
	CppUnit::TextUi::TestRunner runner;
	runner.addTest(suite);

	// Change the default outputter to a compiler error format outputter
	runner.setOutputter(new CppUnit::CompilerOutputter(&runner.result(),
		std::cerr));
	// Run the tests.
	bool wasSucessful = runner.run();

	// Return error code 1 if the one of test failed.
	return wasSucessful ? 0 : 1;
}
