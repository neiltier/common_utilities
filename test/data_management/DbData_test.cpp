#include "InputDbDataCell.hpp"
#include "OutputDbDataCell.hpp"

#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/ui/text/TestRunner.h>

using namespace CppUnit;

class DbDataTest: public TestFixture
{
public:

	// ---------------------------------------------------------------------------
	void setUp()
    {
    }

	// ---------------------------------------------------------------------------
	void tearDown()
	{
    }

    // ---------------------------------------------------------------------------
    void testInputDbDataCell()
    {
        // Construct a set of cells.
        const DataManagement::InputDbDataCells_t expectedCells =
        {
            DataManagement::InputDbDataCell("first_name", "Chelsea"),
            DataManagement::InputDbDataCell("last_name", "Vandy"),
            DataManagement::InputDbDataCell("age", "11")
        };

        // Copy the cells to test the assignment operator.
        const DataManagement::InputDbDataCells_t duplicateCells = expectedCells;

        // Test the the values of the duplicate cells are equal to the values of
        // the original cells.
        CPPUNIT_ASSERT_EQUAL(expectedCells.size(), duplicateCells.size());

        for(int i = 0; i < duplicateCells.size(); ++i)
        {
            CPPUNIT_ASSERT_EQUAL(expectedCells[i].getColumnName(), duplicateCells[i].getColumnName());
            CPPUNIT_ASSERT_EQUAL(expectedCells[i].getColumnValue(), duplicateCells[i].getColumnValue());
        }
    }

    // ---------------------------------------------------------------------------
    void testOutputDbDataCell()
    {
        // Construct a set of cells.
        const DataManagement::OutputDbDataCells_t expectedCells =
        {
            DataManagement::OutputDbDataCell("column_1", "string"),
            DataManagement::OutputDbDataCell("column_2", 2),
            DataManagement::OutputDbDataCell("column_3", 3.343),
            DataManagement::OutputDbDataCell("column_4", true)
        };

        // Copy the cells to test the assignment operator.
        const DataManagement::OutputDbDataCells_t duplicateCells = expectedCells;

        // Test the the values of the duplicate cells are equal to the values of
        // the original cells.
        CPPUNIT_ASSERT_EQUAL(expectedCells.size(), duplicateCells.size());

        for(int i = 0; i < duplicateCells.size(); ++i)
        {
            CPPUNIT_ASSERT_EQUAL(expectedCells[i].getColumnName(), duplicateCells[i].getColumnName());

            // Compare the type since std::any does not have an == operator.
            CPPUNIT_ASSERT(expectedCells[i].getColumnValue().type() == duplicateCells[i].getColumnValue().type());
        }
    }

private:

	// ------------------------------------------------------------------------
    CPPUNIT_TEST_SUITE(DbDataTest);
    CPPUNIT_TEST(testInputDbDataCell);
    CPPUNIT_TEST(testOutputDbDataCell);
    CPPUNIT_TEST_SUITE_END();
};

CPPUNIT_TEST_SUITE_REGISTRATION(DbDataTest);


// ------------------------------------------------------------------------
// ------------------------------------------------------------------------
int main(int argc, char* argv[])
{
	// Get the top level suite from the registry
	CppUnit::Test *suite = CppUnit::TestFactoryRegistry::getRegistry().makeTest(); 
	// Adds the test to the list of test to run
	CppUnit::TextUi::TestRunner runner;
	runner.addTest(suite);

	// Change the default outputter to a compiler error format outputter
	runner.setOutputter(new CppUnit::CompilerOutputter(&runner.result(),
		std::cerr));
	// Run the tests.
	bool wasSucessful = runner.run();

	// Return error code 1 if the one of test failed.
	return wasSucessful ? 0 : 1;
}
