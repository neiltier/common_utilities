set(EXECUTABLE_OUTPUT_PATH "bin")

add_executable(sqlitedbmanager_test SqliteDbManager_test.cpp)
target_link_libraries(sqlitedbmanager_test data_management cppunit)
add_test(sqlitedbmanager_test ${EXECUTABLE_OUTPUT_PATH}/sqlitedbmanager_test)

add_executable(dbdata_test DbData_test.cpp)
target_link_libraries(dbdata_test data_management cppunit)
add_test(dbdata_test ${EXECUTABLE_OUTPUT_PATH}/dbdata_test)
