# README #

## Description
- C++ utilities for general use.

- **NOTE**: Most of this code was experimental or used as a proof of concept. I would suggest refactoring/retesting before using in a production setting.

## Dependencies
* cmake version 3.8 or greater.
* clang 5.0 or greater
* gcc 7 or greater.
* sqlite3 
* stdc++-8-dev library (8 or greater)
* libsqlite3-dev library
* libcppunit-dev library


## Configuration
- Build files are generated using Cmake. To generate, run the following terminal command from the repository's root:

```sh
cmake . -B build
```

## Building
- To build the project, first configure the make files using Cmake, then run the following terminal command:

```sh
make -j8
```

## Testing
- This project's unit test's utilize CppUnit as the testing framework as well as CTest to run the test executables. To run all project tests, from the ```build``` directory, run the terminal command:

```sh
make test
```